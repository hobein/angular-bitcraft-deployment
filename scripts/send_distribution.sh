#!/usr/bin/env bash
if [ "$#" -ne  "4" ]; then
    echo "Usage: ./send_distribution.sh address file name rootDirectory"
    exit 1;
fi

ADDRESS=$1
SOURCE_FILE_PATH=$2
FILE_NAME=$3
ROOT_DIRECTORY=$4

DISTRIBUTION_CACHE_DIRECTORY="$ROOT_DIRECTORY/distributionCache"
TARGET_FILE_PATH="$DISTRIBUTION_CACHE_DIRECTORY/$FILE_NAME"

ssh $ADDRESS "test -e $TARGET_FILE_PATH"
FILE_EXISTS_REMOTE=$?
if [ $FILE_EXISTS_REMOTE -ne 0 ]; then
    #send archive
    scp $SOURCE_FILE_PATH $ADDRESS:~/$TARGET_FILE_PATH
    echo "$FILE sent to $ADDRESS"
fi