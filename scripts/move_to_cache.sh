#!/usr/bin/env bash
if [ "$#" -ne  "3" ]; then
    echo "Usage: ./move_to_cache.sh filePath resultName distributionCachePath"
    exit 1
fi

FILE_PATH=$1
RESULT_NAME=$2
CACHE_DIRECTORY=$3

if [ ! -d $CACHE_DIRECTORY ]
then
    mkdir $CACHE_DIRECTORY
fi

mv $FILE_PATH $CACHE_DIRECTORY/$RESULT_NAME