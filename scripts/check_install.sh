#!/usr/bin/env bash

if [ "$#" -ne  "3" ]; then
    echo "Usage: ./check_install.sh localAddress directory rootDirectory"
    exit 1
fi

ADDRESS=$1
ROOT_DIRECTORY=$3

#check if the directory exists on the remote machine
DIRECTORY="$ROOT_DIRECTORY/$2"
ssh $ADDRESS "[ -d $DIRECTORY ]"
EXISTS=$?

if [ $EXISTS -ne 0 ]
then
    echo "$2 is not installed at $1"
    exit 1
fi

echo "$2 is installed at $1"