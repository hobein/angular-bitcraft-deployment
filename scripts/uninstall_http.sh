#!/usr/bin/env bash
if [ "$#" -ne  "2" ]; then
    echo "Usage: ./uninstall_http.sh localAddress rootDirectory"
    exit 1
fi

function stopForeverProcess() {
    PROCESS_ID=$1
    PROCESS_EXISTS=`ssh $ADDRESS "source .bash_profile; forever list | grep -c -i '$PROCESS_ID'"`
    if [ $PROCESS_EXISTS -eq 1 ]
    then
        #stop the running  process
        ssh $ADDRESS "source .bash_profile; forever stop $PROCESS_ID"
    fi
}

ADDRESS=$1
ROOT_DIRECTORY=$2

#check if the directory exists on the remote machine
HTTP_DIRECTORY="$ROOT_DIRECTORY/http"
ssh -o ConnectTimeout=5 $ADDRESS "[ -d $HTTP_DIRECTORY ]"
RESULT=$?
if [ $RESULT -eq 255 ]; then
    exit 255
fi

HTTP_EXISTS=$RESULT
if [ $HTTP_EXISTS -eq 0 ]
then
    echo "uninstalling http"

    stopForeverProcess http

    #delete the existing directory
    ssh $ADDRESS "rm -rf $HTTP_DIRECTORY"
fi