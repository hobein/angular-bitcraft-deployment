#!/usr/bin/env bash
if [ "$#" -ne  "2" ]; then
    echo "Usage: ./uninstall_sentinel.sh localAddress rootDirectory"
    exit 1
fi

function stopForeverProcess() {
    PROCESS_ID=$1
    PROCESS_EXISTS=`ssh $ADDRESS "source .bash_profile; forever list | grep -c -i '$PROCESS_ID'"`
    if [ $PROCESS_EXISTS -eq 1 ]
    then
        #stop the running  process
        ssh $ADDRESS "source .bash_profile; forever stop $PROCESS_ID"
    fi
}

ADDRESS=$1
ROOT_DIRECTORY=$2

#check if the directory exists on the remote machine
SENTINEL_DIRECTORY="$ROOT_DIRECTORY/sentinel"
ssh -o ConnectTimeout=5 $ADDRESS "[ -d $SENTINEL_DIRECTORY ]"
RESULT=$?
if [ $RESULT -eq 255 ]; then
    exit 255
fi

SENTINEL_EXISTS=$RESULT
if [ $SENTINEL_EXISTS -eq 0 ]
then
    echo "uninstalling sentinel"

    stopForeverProcess "sentinel-$ADDRESS"

    #delete the existing directory
    ssh $ADDRESS "rm -rf $SENTINEL_DIRECTORY"
fi