'use strict';

var config;

/**
 * @param config
 */
function setup(executingConfig) {
    config = executingConfig;
}

/**
 *
 * @param {Object} request
 * @param {function(number|undefined, Object)} callback
 */
function getCurrentConfig(request, callback) {
    callback(undefined, config);
}

module.exports = {
    setup: setup,
    plugins: {
        getCurrentConfig: {
            method: 'GET',
            anonymous: true,
            contentType: 'application/json',
            callback: getCurrentConfig

        }
    }
};
