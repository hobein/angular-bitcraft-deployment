'use strict';

// native
var fs = require('fs');
var util = require('util');

// 3rd party
var ObjectId = require('mongodb').ObjectId;
var async = require('async');
var tmp = require('tmp');

var uuid;
var mongo;
/** @type {{BAD_REQUEST, INTERNAL_ERROR}} */
var returnCodes;
var endpointService;

// local
/** @type {Log4jsLogger} */
var logger;

var HOSTS_DATA_COLLECTION;
var DISTRIBUTIONS_DATA_COLLECTION;
var NODES_DATA_COLLECTION;
var SENTINEL_ACCESS_PORT;
var EXECUTING_PATH;
var ARCHIVE_ROOT_DIRECTORY;
var DISTRIBUTION_DIRECTORY_PATH;

/** @type {Db} */
var defaultDb;

var exec = require('child_process').exec;

// ensure we delete the temporary files even on uncaught exception
tmp.setGracefulCleanup({unsafeCleanup: true});

/**
 * @param deploymentlogger
 * @param mongoDb
 * @param mongoService
 * @param laposteReturnCodes
 * @param libEndpointService
 * @param pyUuid
 * @param {{hostsDataCollection: string,
 *      nodesDataCollection: string,
 *      distributionsDataCollection: string}} collections
 * @param executingPath
 * @param archiveRootDirectory
 * @param distributionDirectoryPath
 */
function setup(deploymentlogger,
               mongoDb,
               mongoService,
               laposteReturnCodes,
               libEndpointService,
               pyUuid,
               collections,
               executingPath,
               archiveRootDirectory,
               distributionDirectoryPath) {
    logger = deploymentlogger;
    defaultDb = mongoDb;
    mongo = mongoService;
    returnCodes = laposteReturnCodes;
    endpointService = libEndpointService;
    uuid = pyUuid;
    HOSTS_DATA_COLLECTION = collections.hostsDataCollection;
    DISTRIBUTIONS_DATA_COLLECTION = collections.distributionsDataCollection;
    NODES_DATA_COLLECTION = collections.nodesDataCollection;
    EXECUTING_PATH = executingPath;
    ARCHIVE_ROOT_DIRECTORY = archiveRootDirectory;
    DISTRIBUTION_DIRECTORY_PATH = distributionDirectoryPath;
}

/**
 * @param {Error} err
 * @param {function(number|null, *, Object=)} callback
 */
function dbErrorHelper(err, callback) {
    logger.error('Failed retrieving data: ' + util.inspect(err));
    callback(returnCodes.INTERNAL_ERROR, 'DB error. See log.', {contentType: 'text/plain'});
}

/**
 * @param {string} collection
 * @param {Object} query
 * @param {Object|null} projection
 * @param {function(Error|undefined, Array=)} callback
 */
function simpleFindToArray(collection, query, projection, callback) {
    defaultDb.
    collection(collection).
    find(query, projection || {}).
    toArray(callback);
}

/**
 * @param {string} collection
 * @param {Object} query
 * @param {Object|null} projection
 * @param {function(Error|undefined, Object|null=)} callback
 */
function simpleFind(collection, query, projection, callback) {
    simpleFindToArray(collection, query, projection,
        function (err, docs) {
            if (err) {
                callback(err);
                return;
            }
            callback(undefined, docs.length > 0 ? docs[0] : null);
        });
}

/**
 * @param {string} nodeId
 * @param {function(err:string|undefined, hostInfo: HostInfo=)} callback
 */
function getHostInfoByNodeId(nodeId, callback) {
    async.waterfall(
        [
            /**
             * get the node info
             * @param {function(err: string|undefined, nodeInfo: NodeInfo)} next
             */
                function (next) {
                simpleFind(
                    NODES_DATA_COLLECTION,
                    {_id: new ObjectId(nodeId)},
                    null,
                    next
                );
            },
            /**
             * get the host info
             * @param {NodeInfo} nodeInfo
             * @param {function(err: string|undefined, hostInfo: HostInfo=)} next
             */
                function (nodeInfo, next) {
                if (!nodeInfo) {
                    next('node not found in db');
                    return;
                }

                simpleFind(
                    HOSTS_DATA_COLLECTION,
                    {_id: new ObjectId(nodeInfo.hostId)},
                    null,
                    next
                );
            }
        ],
        /**
         * @param {Error|{code: number, msg: string}|undefined|null} err
         * @param {HostInfo} hostInfo
         */
        function (err, hostInfo) {
            if (err) {
                callback(err);
                return;
            }
            callback(undefined, hostInfo);
        }
    );
}

/**
 * @param {string} nodeId
 * @param {string} api
 * @param {function(NodeInfo, HostInfo)} setDataFunction
 * @param {function(
 *      errCode: number|null,
 *      errMessage: string|null=,
 *      data: Object=)} callback
 */
function proxyPostAgent(nodeId, api, setDataFunction, callback) {
    /** @type {HostInfo} */
    var hostInfo;

    async.waterfall(
        [
            /** @param {function(string|undefined, HostInfo=)} next */
                function (next) {
                getHostInfoByNodeId(nodeId, next);
            },
            /**
             * @param {HostInfo} host
             * @param {function(Error|undefined, NodeInfo=)} next
             */
                function (host, next) {
                hostInfo = host;
                simpleFind(NODES_DATA_COLLECTION,
                    {_id: new ObjectId(nodeId)},
                    null,
                    next);
            },
            /**
             * @param {NodeInfo} nodeInfo
             * @param {function(string|undefined, statusCode: number=, data: Object)} next
             */
                function (nodeInfo, next) {
                endpointService.proxyPostRequest(
                    {
                        targetType: endpointService.ADMIN_PROXY_TARGET_TYPES.SENTINEL,
                        targetId: String(hostInfo._id)
                    },
                    api,
                    (setDataFunction && setDataFunction(nodeInfo, hostInfo)) || {agentUuid: nodeInfo.uuid},
                    /**
                     * @param {number} status
                     * @param {SentinelGenericResponse} response
                     */
                    function (status, response) {
                        next(response.error, status, response.data);
                    }
                );
            }
        ],
        /**
         * @param {Error|string|undefined} err
         * @param {number|undefined} errCode
         * @param {Object|undefined} data
         */
        function (err, errCode, data) {
            if (errCode && errCode !== returnCodes.OK) {
                callback(errCode, err.message || err, data);
                return;
            }
            if (err) {
                callback(returnCodes.INTERNAL_ERROR, err.message || err);
                return;
            }
            callback(null, null, data);
        }
    );
}

/**
 * @param {string} category
 * @param {string} name
 * @param {string} version
 * @param {function(Error|undefined, Object|null=)} callback
 */
function getDistributionVersion(category, name, version, callback) {
    simpleFind(
        DISTRIBUTIONS_DATA_COLLECTION,
        {category: category, name: name, version: version},
        null,
        callback
    );
}

/**
 * Load the main master data
 * @param {string} filePath
 * @param {function(Error|undefined, Buffer=)} callback
 */
function loadDistributionFile(filePath, callback) {
    if (!filePath) {
        callback(new Error('Missing file path'));
        return;
    }

    fs.readFile(filePath, function (err, buffer) {
        if (err) {
            callback(err);
            return;
        }
        callback(undefined, buffer);
    });
}

/**
 * @param {string} category
 * @param {string} name
 * @param {string} version
 * @param {Buffer} data
 * @param {function(Error=)} callback
 */
function writeDistributionData(category, name, version, data, callback) {
    /** @type {DistributionInfo} */
    var entry = {
        category: category,
        name: name,
        version: version,
        uploadTimestamp: Date.now(),
        data: mongo.Binary(data)
    };

    if (!data) {
        callback(new Error('Invalid distribution data'));
        return;
    }
    if (!data.length) {
        logger.warn('empty distribution data name=' + name + ', version=' + version);
        callback();
        return;
    }

    defaultDb.collection(DISTRIBUTIONS_DATA_COLLECTION).insert(entry, callback);
}

/**
 * @param {{
 *   data: {
 *       files: {
 *              file: uploadFileData
 *       }
 *       fields: {
 *              category: string,
 *              name: string,
 *              version: string,
 *        }
 *   }
 * }} request
 * @param {function(number|null, string=)} callback
 */
function uploadDistributionArchive(request, callback) {
    /** @type {string} */
    var name = request.data.fields.name;
    /** @type {string} */
    var category = request.data.fields.category;
    /** @type {string} */
    var version = request.data.fields.version;
    /** @type {uploadFileData} */
    var file = request.data.files.file;
    /** @type {Buffer} */
    var fileData;

    if (typeof name !== 'string' || typeof version !== 'string' || typeof category !== 'string') {
        callback(returnCodes.BAD_REQUEST, 'Invalid parameter name, version or category');
        return;
    }
    if (file === undefined) {
        callback(returnCodes.BAD_REQUEST, 'Missing file for ' + name);
        return;
    }

    async.series(
        [
            // 1. check if the version already exists
            function (next) {
                getDistributionVersion(category, name, version, function (err, doc) {
                    if (err) {
                        next(err);
                        return;
                    }
                    if (doc) {
                        next({code: returnCodes.BAD_REQUEST, msg: 'Entry already exists'});
                        return;
                    }
                    next();
                });
            },
            // 2. load the file
            function (next) {
                loadDistributionFile(
                    file.path,
                    function (err, data) {
                        if (err) {
                            logger.error('Failed to read distribution archive: ' + err);
                            next({
                                code: returnCodes.BAD_REQUEST,
                                msg: 'Could not load distribution archive'
                            });
                            return;
                        }
                        fileData = data;
                        next();
                    }
                );
            },
            // 3. write the data to the db
            function (next) {
                writeDistributionData(
                    category,
                    name,
                    version,
                    fileData,
                    function (err) {
                        if (err) {
                            next(err);
                            return;
                        }
                        next();
                    }
                );
            }
        ],
        /**
         * @param {Error|{code: number, msg: string}|undefined|null} err
         */
        function (err) {
            if (err) {
                if (err.code && err.msg) {
                    callback(err.code, err.msg);
                    return;
                }
                dbErrorHelper(err, callback);
                return;
            }
            callback(null);
        }
    );
}

/**
 * @param {{
 *   data: {
 *       files: {
 *              file: uploadFileData
 *       }
 *   }
 * }} request
 * @param {function(number|null, string=)} callback
 */
function uploadSentinelArchive(request, callback) {
    /** @type {uploadFileData} */
    var file = request.data.files.file;

    if (!file) {
        callback(returnCodes.BAD_REQUEST, 'Sentinel archive not found');
        return;
    }
    logger.debug('writing new sentinel to distribution cache: ' +
        request.data.files.file.name);

    exec('bash ' + EXECUTING_PATH + '/scripts/move_to_cache.sh ' +
        '"' + request.data.files.file.path + '" ' +
        '"' + request.data.files.file.name + '" ' +
        '"' + DISTRIBUTION_DIRECTORY_PATH + '"',
        function (err, stdout, stderr) {
            if (err) {
                logger.error('exec error: \n' + err);
                callback(returnCodes.INTERNAL_ERROR, err.toString());
                return;
            }
            if (stderr) {
                logger.debug('stderr: \n' + stderr);
            } else if (stdout) {
                logger.debug('stdout: \n' + stdout);
            }
            callback(null);
        });
}

/**
 * @param {HostInfo} hostInfo
 * @param {function(Error|undefined, Object|null=)} callback
 */
function getHost(hostInfo, callback) {
    simpleFind(
        HOSTS_DATA_COLLECTION,
        {category: hostInfo.category, localAddress: hostInfo.localAddress},
        null,
        callback
    );
}

/**
 * @param {string} hostId
 * @param {function(Error|undefined, Object|null=)} callback
 */
function getHostById(hostId, callback) {
    simpleFind(
        HOSTS_DATA_COLLECTION,
        {_id: new ObjectId(hostId)},
        null,
        callback
    );
}

/**
 * @param {HostInfo} hostInfo
 * @param {function(Error=)} callback
 */
function writeHostData(hostInfo, callback) {
    if (!hostInfo.category || !hostInfo.category.length) {
        callback(new Error('Category not set'));
        return;
    }
    if (!hostInfo.localAddress || !hostInfo.localAddress.length) {
        callback(new Error('Local address not set'));
        return;
    }

    defaultDb.collection(HOSTS_DATA_COLLECTION).insert(hostInfo, callback);
}

/**
 * @param {HostInfo} host
 * @param {string} name
 * @param {number} udpPort
 * @param {number} tcpPort
 * @param {string} adminHost
 * @param {number} adminPort
 * @param {JSON} customData
 * @param {function(Error=)} callback
 */
function writeNodeData(host, name, udpPort, tcpPort, adminHost, adminPort, customData, callback) {
    /** @type {NodeInfo} */
    var entry = {
        uuid: uuid.v4().toString(),
        hostId: host._id,
        name: name,
        udpPort: udpPort,
        tcpPort: tcpPort,
        adminAccess: {
            host: adminHost,
            port: adminPort
        },
        bootTime: null,
        customData: customData
    };

    defaultDb.collection(NODES_DATA_COLLECTION).insert(entry, callback);
}

var REMOVE_HOST_OPTIONS = {justOne: true};

/**
 * @param {{data: {hostId: string}}} request
 * @param {function(number|null, string=)} callback
 */
function uninstallHost(request, callback) {
    /** @type {HostInfo} */
    var hostInfo;

    if (!request || !request.data || !request.data.hostId) {
        callback(returnCodes.BAD_REQUEST);
        return;
    }

    async.waterfall(
        [
            /** @param {function(string|undefined, HostInfo=)} next */
                function (next) {
                getHostById(request.data.hostId, next);
            },
            /**
             * @param {HostInfo} host
             * @param {function(string=)} next
             */
                function (host, next) {
                hostInfo = host;
                /** @type {ObjectID} */
                var idToRemove = new ObjectId(request.data.hostId);

                defaultDb.collection(HOSTS_DATA_COLLECTION).remove({_id: idToRemove}, REMOVE_HOST_OPTIONS,
                    /** @param {Error} err */
                    function (err) {
                        if (err) {
                            next(err.message);
                            return;
                        }
                        next();
                    });
            },
            /**
             * @param {function(string=)} next
             */
                function (next) {
                logger.debug('uninstalling sentinel at ' + hostInfo.localAddress);
                exec('bash ' + EXECUTING_PATH + '/scripts/uninstall_sentinel.sh "' +
                    hostInfo.localAddress + '" "' + ARCHIVE_ROOT_DIRECTORY + '"',
                    function (err, stdout, stderr) {
                        if (err) {
                            next(err.toString());
                            return;
                        }
                        if (stderr) {
                            logger.debug('stderr: \n' + stderr);
                        } else if (stdout) {
                            logger.debug('stdout: \n' + stdout);
                        }
                        next();
                    });
            },
            /** @param {function(string=)} next */
                function (next) {
                if (hostInfo.category === 'http') {
                    logger.debug('uninstalling http at ' + hostInfo.localAddress);
                    exec('bash ' + EXECUTING_PATH + '/scripts/uninstall_http.sh "' +
                        hostInfo.localAddress + '" "' + ARCHIVE_ROOT_DIRECTORY + '"',
                        function (err, stdout, stderr) {
                            if (err) {
                                next(err.toString());
                                return;
                            }
                            if (stderr) {
                                logger.debug('stderr: \n' + stderr);
                            } else if (stdout) {
                                logger.debug('stdout: \n' + stdout);
                            }
                            next();
                        });
                } else {
                    next();
                }
            }
        ],
        /**
         * @param {string|undefined} err
         */
        function (err) {
            if (err) {
                logger.error('Failed to uninstall host.\n' + err);
                callback(returnCodes.INTERNAL_ERROR, err);
                return;
            }
            callback(returnCodes.OK);
        }
    );
}

/**
 * Add the host to the db and install sentinel
 * @param {{
 *   data: {
 *       files: {
 *              file: uploadFileData
 *       }
 *       fields: {
 *              id: string,
 *              category: string,
 *              sentinelPort: number,
 *              localAddress: string,
 *              dns: string
 *        }
 *   }
 * }} request
 * @param {function(number|null, string=)} callback
 */
function installHost(request, callback) {
    /** @type {HostInfo} */
    var hostInfo = {
        _id: request.data.fields.id,
        category: request.data.fields.category,
        sentinelPort: request.data.fields.sentinelPort,
        localAddress: request.data.fields.localAddress,
        dns: request.data.fields.dns
    };

    if (!hostInfo.category || !hostInfo.category.length ||
        !hostInfo.localAddress || !hostInfo.localAddress.length) {
        callback(returnCodes.BAD_REQUEST, 'Invalid parameter name or local address');
        return;
    }

    async.series(
        [
            // 1. checks if the host already exists
            /** @param {function(Error|{code: number, msg: string}=)} next */
                function (next) {
                getHost(hostInfo, function (err, doc) {
                    if (err) {
                        next(err);
                        return;
                    }
                    if (doc) {
                        next({code: returnCodes.BAD_REQUEST, msg: 'Entry already exists'});
                        return;
                    }
                    next();
                });
            },
            // 2. write the data to the db
            /** @param {function(Error=)} next */
                function (next) {
                writeHostData(
                    hostInfo,
                    function (err) {
                        if (err) {
                            next(err);
                            return;
                        }
                        next();
                    }
                );
            },
            // 3. move new sentinel file to distributionCache
            /** @param {function(Error|{code: number, msg: string}=)} next */
                function (next) {
                if (!request.data.files.file) {
                    next();
                    return;
                }
                logger.debug('writing new sentinel to distribution cache: ' +
                    request.data.files.file.name);

                exec('bash ' + EXECUTING_PATH + '/scripts/move_to_cache.sh ' +
                    '"' + request.data.files.file.path + '" ' +
                    '"' + request.data.files.file.name + '" ' +
                    '"' + DISTRIBUTION_DIRECTORY_PATH + '"',
                    function (err, stdout, stderr) {
                        if (err) {
                            logger.error('exec error: \n' + err);
                            next({code: returnCodes.INTERNAL_ERROR, msg: err.toString()});
                            return;
                        }
                        if (stderr) {
                            logger.debug('stderr: \n' + stderr);
                        } else if (stdout) {
                            logger.debug('stdout: \n' + stdout);
                        }
                        next();
                    });
            },
            // 4. deploy the sentinel
            /** @param {function(Error|{code: number, msg: string}=)} next */
                function (next) {
                // launch sentinel deployment script
                logger.debug('deploying sentinel to host "' + hostInfo.localAddress + '"');
                exec('bash ' + EXECUTING_PATH + '/scripts/install_sentinel.sh "' +
                    hostInfo.localAddress + '" "' + ARCHIVE_ROOT_DIRECTORY + '"',
                    /**
                     * @param {string} err
                     * @param {string} stdout
                     * @param {string} stderr
                     */
                    function (err, stdout, stderr) {
                        /** @type {ObjectID} */
                        var idToRemove;

                        if (err) {
                            logger.error('exec error: \n' + err);

                            logger.debug('failed to install sentinel. removing host from database...');
                            idToRemove = new ObjectId(hostInfo._id);
                            defaultDb.collection(HOSTS_DATA_COLLECTION).remove({_id: idToRemove}, REMOVE_HOST_OPTIONS,
                                function (err) {
                                    if (err) {
                                        logger.error('failed to remove host from db. ' + err.message);
                                        next({code: returnCodes.INTERNAL_ERROR,
                                            msg: 'Failed to install sentinel. See log for details.'});
                                        return;
                                    }
                                    next({code: returnCodes.INTERNAL_ERROR,
                                        msg: 'Failed to install sentinel. See log for details.'});
                                });
                            return;
                        }
                        if (stderr) {
                            logger.debug('stderr: \n' + stderr);
                        } else if (stdout) {
                            logger.debug('stdout: \n' + stdout);
                        }

                        logger.debug('sentinel running on host "' + hostInfo.localAddress + '"');
                        next();
                    });
            }
        ],
        /**
         * @param {Error|{code: number, msg: string}|undefined|null} err
         */
        function (err) {
            if (err) {
                if (err.code && err.msg) {
                    callback(err.code, err.msg);
                    return;
                }
                dbErrorHelper(err, callback);
                return;
            }
            callback(null);
        }
    );
}

/**
 * Reinstall sentinel
 * @param {{data: HostInfo}} request
 * @param {function(number|null, string=)} callback
 */
function reinstallSentinel(request, callback) {
    /** @type {HostInfo} */
    var hostInfo = request.data;

    if (!hostInfo.category || !hostInfo.category.length ||
        !hostInfo.localAddress || !hostInfo.localAddress.length) {
        callback(returnCodes.BAD_REQUEST, 'Invalid parameter name or local address');
        return;
    }

    // launch sentinel deployment script
    logger.debug('redeploying sentinel to host "' + hostInfo.localAddress + '"');
    exec('bash ' + EXECUTING_PATH + '/scripts/install_sentinel.sh "' +
        hostInfo.localAddress + '" "' + ARCHIVE_ROOT_DIRECTORY + '"',
        function (err, stdout, stderr) {
            if (err) {
                logger.error('exec error: \n' + err);
                callback(returnCodes.INTERNAL_ERROR, err.toString());
                return;
            }
            if (stderr) {
                logger.debug('stderr: \n' + stderr);
            } else if (stdout) {
                logger.debug('stdout: \n' + stdout);
            }

            callback(returnCodes.OK);
            logger.debug('sentinel running on host "' + hostInfo.localAddress + '"');
        });
}

var REMOVE_NODE_OPTIONS = {justOne: true};

/**
 * @param {{data: {nodeId: string}}} request
 * @param {function(errCode: number|null, Object=)} callback
 */
function uninstallAgent(request, callback) {
    if (!request || !request.data || !request.data.nodeId) {
        callback(returnCodes.BAD_REQUEST);
        return;
    }

    proxyPostAgent(request.data.nodeId,
        'uninstallAgent',
        null,
        /**
         * @param {number|null} errCode
         * @param {string|null} err
         * @param {Object|undefined} data
         */
        function (errCode, err, data) {
            /** @type {ObjectID} */
            var idToRemove = new ObjectId(request.data.nodeId);

            if (errCode) {
                logger.error('Failed to uninstall agent. ' + err);
                callback(errCode, err);
                return;
            }

            defaultDb.collection(NODES_DATA_COLLECTION).remove({_id: idToRemove}, REMOVE_NODE_OPTIONS,
                /** @param {Error} err */
                function (err) {
                    if (err) {
                        callback(returnCodes.INTERNAL_ERROR, {error: err.message});
                        return;
                    }
                    callback(null, data);
                });
        });
}

var REMOVE_DISTRIBUTIONS_OPTIONS = {justOne: true};

/**
 * @param {{data: {distributionId: string}}} request
 * @param {function(number|null, string=)} callback
 */
function removeDistribution(request, callback) {
    /** @type {ObjectID} */
    var idToRemove;

    if (!request || !request.data || !request.data.distributionId) {
        callback(returnCodes.BAD_REQUEST);
        return;
    }
    idToRemove = new ObjectId(request.data.distributionId);
    defaultDb.collection(DISTRIBUTIONS_DATA_COLLECTION).remove(
        {_id: idToRemove},
        REMOVE_DISTRIBUTIONS_OPTIONS,
        function (err) {
            if (err) {
                callback(returnCodes.INTERNAL_ERROR, err.message);
                return;
            }
            callback(null, 'Success');
        }
    );
}

/**
 * @param {{distributionId: string}} request
 * @param {function(number|null, string|Array=, Object=)} callback
 */
function downloadDistribution(request, callback) {
    /** @type {ObjectId} */
    var distributionId;

    if (!request || !request.distributionId) {
        callback(
            returnCodes.BAD_REQUEST,
            'Invalid distribution id',
            {contentType: 'text/plain'}
        );
        return;
    }
    distributionId = new ObjectId(request.distributionId);

    logger.info('downloading distribution ' + request.distributionId);
    defaultDb.collection(DISTRIBUTIONS_DATA_COLLECTION).findOne(
        {_id : distributionId},
        /**
         * @param {MongoError} err
         * @param {DistributionInfo} item
         */
        function (err, item) {
            if (err) {
                callback(returnCodes.INTERNAL_ERROR);
                return;
            }

            callback(
                null,
                item.data.buffer,
                {headers: {'Content-Disposition': 'attachment; filename=' + item.name}}
            );
        }
    );
}

/**
 * @param {{data: {
 *      id: string,
 *      name: string,
 *      udpPort: number,
 *      tcpPort: number,
 *      adminHost: string,
 *      adminPort: number,
 *      customData: Object}}} request
 * @param {function(number|null, string=)} callback
 */
function updateNode(request, callback) {
    /** @type {string} */
    var nodeId;
    /** @type {string} */
    var name;
    /** @type {number} */
    var udpPort;
    /** @type {number} */
    var tcpPort;
    /** @type {string} */
    var adminHost;
    /** @type {number} */
    var adminPort;
    /** @type {Object} */
    var customData;
    /** @type {ObjectID} */
    var idToUpdate;

    if (!request) {
        callback(returnCodes.INTERNAL_ERROR);
        return;
    }

    idToUpdate = new ObjectId(request.data.id);
    nodeId = request.data.id;
    name = request.data.name;
    udpPort = request.data.udpPort;
    tcpPort = request.data.tcpPort;
    adminHost = request.data.adminHost;
    adminPort = request.data.adminPort;
    customData = request.data.customData || {};

    defaultDb.collection(NODES_DATA_COLLECTION).updateOne(
        {_id: idToUpdate},
        {$set: {
            name: name,
            udpPort: udpPort,
            tcpPort: tcpPort,
            adminAccess: {
                host: adminHost,
                port: adminPort
            },
            customData: customData
        }},
        /** @type {MongoError} err */
        function (err) {
            if (err) {
                callback(returnCodes.INTERNAL_ERROR, err.message);
                return;
            }

            proxyPostAgent(nodeId,
                'updateNode',
                /**
                 * @param {NodeInfo} nodeInfo
                 * @param {HostInfo} hostInfo
                 */
                function (nodeInfo, hostInfo) {
                    return {
                        agentUuid: nodeInfo.uuid,
                        name: name,
                        udpPort: udpPort,
                        tcpPort: tcpPort,
                        adminAccess: {
                            host: adminHost,
                            port: adminPort
                        },
                        customData: customData
                    };
                },
                /**
                 * @param {number|null} errCode
                 * @param {string|null} err
                 * @param {Object|undefined} data
                 */
                function (errCode, err, data) {
                    if (errCode) {
                        logger.error('Failed to update node. ' + err);
                        callback(errCode, err);
                        return;
                    }
                    callback(undefined, data);
                });
        }
    );
}

/**
 * @param {{data: {id: string, version: string}}} request
 * @param {function(number|null, string=)} callback
 */
function updateDistribution(request, callback) {
    /** @type {string} */
    var distributionId;
    /** @type {string} */
    var version;
    /** @type {ObjectID} */
    var idToUpdate;

    if (!request) {
        callback(returnCodes.INTERNAL_ERROR);
        return;
    }

    distributionId = request.data.id;
    version = request.data.version;
    idToUpdate = new ObjectId(request.data.id);

    defaultDb.collection(DISTRIBUTIONS_DATA_COLLECTION).updateOne(
        {_id: idToUpdate},
        {$set: {version: version}},
        /** @type {MongoError} err */
        function (err) {
            if (err) {
                callback(returnCodes.INTERNAL_ERROR, err.message);
                return;
            }
            callback(null, 'Success');
        }
    );
}

/**
 * @param {string} distributionId
 * @param {string} hostId
 * @param {function(localAddress: string, distributionName: string, file: string)} buildDeployCommand
 * @param {function(err: string=)} callback
 */
function installDistribution(distributionId, hostId, buildDeployCommand, callback) {
    /** @type {string} */
    var file;
    /** @type {DistributionInfo} */
    var distributionInfo;

    logger.info('installing distribution ' + distributionId);

    async.waterfall(
        [
            /** @param {function(string|undefined, tmpFile: string=)} next */
                function (next) {
                tmp.file(undefined, next);
            },
            /**
             * @param {string} tmpFile
             * @param {number} fd
             * @param {function()} cleanupCallback
             * @param {function(
             *              err: Error|undefined,
             *              distribution: DistributionInfo,
             *              fd: number)} next
             */
                function (tmpFile, fd, cleanupCallback, next) {
                file = tmpFile;
                simpleFindToArray(DISTRIBUTIONS_DATA_COLLECTION,
                    {_id: new ObjectId(distributionId)},
                    null,
                    function (err, docs) {
                        next(err, docs.length > 0 ? docs[0] : undefined, fd);
                    });
            },
            /**
             * write distribution file to local drive
             * @param {DistributionInfo} distribution
             * @param {number} fd
             * @param {function(string=)} next
             */
                function (distribution, fd, next) {
                if (distribution === undefined) {
                    next('distribution not found');
                    return;
                }
                distributionInfo = distribution;
                fs.write(fd, distribution.data.buffer, 0, distribution.data.buffer.length, 0,
                    function (err) {
                        next(err);
                    });
            },
            /**
             * get host info
             * @param {function(Error|undefined, HostInfo=)} next
             */
                function (next) {
                getHostById(hostId, next);
            },
            /**
             * run the deployment script
             * @param {HostInfo} hostInfo
             * @param {function(string=)} next
             */
                function (hostInfo, next) {
                exec(buildDeployCommand(hostInfo.localAddress, distributionInfo.name, file),
                    function (err, stdout, stderr) {
                        if (err) {
                            logger.error('exec error: ' + err);
                            next(err.toString());
                            return;
                        }
                        if (stderr) {
                            logger.debug('stderr: \n' + stderr);
                        } else if (stdout) {
                            logger.debug('stdout: \n' + stdout);
                        }
                        next();
                    });
            }
        ],
        /**
         * @param {Error|string|undefined} err
         */
        function (err) {
            if (err) {
                logger.error('Failed to install distribution. ' + err.message || err);
                callback(err.message || err);
                return;
            }
            callback();
        }
    );
}

/**
 * @param {{data: {nodeId: string, distributionId: string}}} request
 * @param {function(errCode: number|null, Object=)} callback
 */
function installAgent(request, callback) {
    /** @type {NodeInfo} */
    var nodeInfo;
    /** @type {string} */
    var targetFileName;

    if (!request || !request.data || !request.data.nodeId) {
        callback(returnCodes.BAD_REQUEST);
        return;
    }

    //TODO use nodeInfo and hostInfo of proxyPostAdmin, don't get it from db twice!
    async.waterfall([
            /**
             * get the node info
             * @param {function(err: Error|undefined, nodeInfo: NodeInfo)} next
             */
                function (next) {
                simpleFind(
                    NODES_DATA_COLLECTION,
                    {_id: new ObjectId(request.data.nodeId)},
                    null,
                    next
                );
            },
            /**
             * @param {NodeInfo} node
             * @param {function(err: Error|string|undefined, hostInfo: HostInfo=)} next
             */
                function (node, next) {
                if (!node) {
                    next('node not found in db');
                    return;
                }
                nodeInfo = node;

                simpleFind(
                    HOSTS_DATA_COLLECTION,
                    {_id: new ObjectId(node.hostId)},
                    null,
                    next
                );
            },
            /**
             * @param {HostInfo} hostInfo
             * @param {function(err: string=)} next
             */
                function(hostInfo, next) {
                installDistribution(request.data.distributionId,
                    hostInfo._id,
                    /**
                     * @param {string} localAddress
                     * @param {string} distributionName
                     * @param {string} file
                     * @returns {string}
                     */
                    function (localAddress, distributionName, file) {
                        targetFileName = distributionName;
                        return 'bash ' + EXECUTING_PATH + '/scripts/send_distribution.sh "' + localAddress + '"' +
                            ' "' + file + '" "' + distributionName + '" "' + ARCHIVE_ROOT_DIRECTORY + '"';
                    },
                    /** @param {string|undefined} err */
                    function (err) {
                        if (err) {
                            next(err);
                            return;
                        }
                        next();
                    });
            },
            /**
             * @param {function({code: number, msg: string}|undefined, Object=)} next
             */
                function (next) {
                proxyPostAgent(request.data.nodeId,
                    'installAgent',
                    /**
                     * @param {NodeInfo} nodeInfo
                     * @param {HostInfo} hostInfo
                     */
                    function (nodeInfo, hostInfo) {
                        return {
                            agentUuid: nodeInfo.uuid,
                            distributionName: targetFileName,
                            localAddress: hostInfo.localAddress,
                            dns: hostInfo.dns,
                            udpPort: nodeInfo.udpPort,
                            tcpPort: nodeInfo.tcpPort,
                            adminAccess: nodeInfo.adminAccess,
                            agentName: nodeInfo.name,
                            customData: nodeInfo.customData
                        };
                    },
                    /**
                     * @param {number|null} errCode
                     * @param {string|null} err
                     * @param {Object|undefined} data
                     */
                    function (errCode, err, data) {
                        if (errCode) {
                            logger.error('Failed to install agent. ' + err);
                            next({code: errCode, msg: err});
                            return;
                        }
                        next(undefined, data);
                    });
            },
            /**
             * @param {Object} data
             * @param {function(string|undefined, Object=)} next
             */
                function(data, next) {
                // update distribution reference in db node document
                defaultDb.collection(NODES_DATA_COLLECTION).updateOne(
                    {_id: nodeInfo._id},
                    {$set: {
                        installTimestamp: Date.now(),
                        distributionId: request.data.distributionId
                    }},
                    /** @type {MongoError} err */
                    function (err) {
                        if (err) {
                            next({code: returnCodes.INTERNAL_ERROR, msg: err.message});
                            return;
                        }
                        next(undefined, data);
                    }
                );
            }
        ],
        /**
         * @param {Error|string|{code: number, msg: string}|undefined|null} err
         * @param {Object} data
         */
        function (err, data) {
            if (err) {
                if (err.code && err.msg) {
                    callback(err.code, {error: err.msg});
                } else {
                    callback(returnCodes.INTERNAL_ERROR, {error: err.message || err});
                }
                return;
            }
            callback(null, data);
        });
}

/**
 * @param {{data: {hostId: string, distributionId: string}}} request
 * @param {function(number|null, string=)} callback
 */
function installHttp(request, callback) {
    if (!request) {
        callback(returnCodes.INTERNAL_ERROR);
        return;
    }

    installDistribution(request.data.distributionId,
        request.data.hostId,
        /**
         * @param {string} localAddress
         * @param {string} distributionName
         * @param {string} file
         * @returns {string}
         */
        function (localAddress, distributionName, file) {
            return 'bash ' + EXECUTING_PATH + '/scripts/install_http.sh "' + localAddress + '"' +
                ' "' + file + '" "' + distributionName + '" "' + ARCHIVE_ROOT_DIRECTORY + '"';
        },
        /** @param {string|undefined} err */
        function (err) {
            if (err) {
                callback(returnCodes.INTERNAL_ERROR, err);
                return;
            }
            callback(returnCodes.OK);
        });
}

/**
 * @param {Object} query
 * @param {function({code:number}|null, Array.<HostInfo>=)} callback
 */
function listHosts(query, callback) {
    simpleFindToArray(
        HOSTS_DATA_COLLECTION,
        {},
        null,
        function (err, docs) {
            if (err) {
                callback(err);
                return;
            }
            callback(undefined, docs);
        }
    );
}

/**
 * @param {Object} query
 * @param {function({code:number}|null, Array.<HostInfo>=)} callback
 */
function listDistributions(query, callback) {
    simpleFindToArray(
        DISTRIBUTIONS_DATA_COLLECTION,
        {},
        {data: 0},
        function (err, docs) {
            if (err) {
                callback(err);
                return;
            }
            callback(undefined, docs);
        }
    );
}

/**
 * @param {{hostId: string}} request
 * @param {function(number|undefined, Object=)} callback
 */
function checkHttpInstall(request, callback) {
    if (!request || !request.hostId) {
        callback(returnCodes.BAD_REQUEST);
        return;
    }

    getHostById(request.hostId,
        /**
         * @param {Error} err
         * @param {HostInfo} hostInfo
         */
        function (err, hostInfo) {
            if (err) {
                callback(returnCodes.INTERNAL_ERROR, {error: err.message, installed: false});
                return;
            }
            exec('bash ' + EXECUTING_PATH + '/scripts/check_install.sh "' + hostInfo.localAddress +
                '" "http" "' + ARCHIVE_ROOT_DIRECTORY + '"',
                function (err, stdout, stderr) {
                    if (stdout) {
                        logger.debug('stdout: \n' + stdout);
                    }
                    //expecting error, means it's not installed
                    callback(returnCodes.OK, {installed: err ? false : true});
                });
        });
}

/**
 * @param {{nodeId: string}} request
 * @param {function(number|undefined, Object=)} callback
 */
function checkAgentInstall(request, callback) {
    /** @type {HostInfo} */
    var hostInfo;

    if (!request || !request.nodeId) {
        callback(returnCodes.BAD_REQUEST);
        return;
    }

    async.waterfall(
        [
            /** @param {function(string|undefined, HostInfo=)} next */
                function (next) {
                getHostInfoByNodeId(request.nodeId, next);
            },
            /**
             * @param {HostInfo} host
             * @param {function(err: string|undefined, NodeInfo=)} next
             */
                function (host, next) {
                hostInfo = host;
                simpleFind(
                    NODES_DATA_COLLECTION,
                    {_id: new ObjectId(request.nodeId)},
                    null,
                    next
                );
            },
            /**
             * @param {NodeInfo} node
             * @param {function(string|undefined, boolean)} next
             */
                function (node, next) {
                exec('bash ' + EXECUTING_PATH + '/scripts/check_install.sh "' + hostInfo.localAddress +
                    '" "agent-' + node.uuid + '" "' + ARCHIVE_ROOT_DIRECTORY + '"',
                    function (err, stdout, stderr) {
                        if (stdout) {
                            logger.debug('stdout: \n' + stdout);
                        }
                        //expecting error, means it's not installed
                        next(undefined, err ? false : true);
                    });
            }
        ],
        /**
         * @param {string|undefined} err
         * @param {boolean} installed
         */
        function (err, installed) {
            if (err) {
                callback(returnCodes.INTERNAL_ERROR, {error: err, installed: false});
                return;
            }
            callback(returnCodes.OK, {installed: installed});
        }
    );
}

/**
 * @param {{hostId: string}} request
 * @param {function(number|undefined, Object=)} callback
 */
function checkHttpState(request, callback) {
    if (!request || !request.hostId) {
        callback(returnCodes.BAD_REQUEST);
        return;
    }

    endpointService.proxyGetRequest(
        {
            targetType: endpointService.ADMIN_PROXY_TARGET_TYPES.SENTINEL,
            targetId: request.hostId
        },
        'checkHttp',
        undefined,
        callback
    );
}

/**
 * @param {{hostId: string}} request
 * @param {function(number|undefined, Object=)} callback
 */
function getNodeCount(request, callback) {
    if (!request || !request.hostId) {
        callback(returnCodes.BAD_REQUEST);
        return;
    }

    defaultDb.collection(NODES_DATA_COLLECTION).count({hostId: new ObjectId(request.hostId)},
        function (err, count) {
            if (err) {
                callback(returnCodes.INTERNAL_ERROR, {err: err.message});
                return;
            }
            callback(returnCodes.OK, {nodeCount: count});
        });
}

/**
 * @param {{data: {hostId: string}}} request
 * @param {function(number|undefined, string=)} callback
 */
function startHttp(request, callback) {
    if (!request || !request.data || !request.data.hostId) {
        callback(returnCodes.BAD_REQUEST);
        return;
    }

    endpointService.proxyPostRequest(
        {
            targetType: endpointService.ADMIN_PROXY_TARGET_TYPES.SENTINEL,
            targetId: request.data.hostId
        },
        'startHttp',
        {},
        /**
         * @param {number} status
         * @param {SentinelGenericResponse} response
         */
        function (status, response) {
            callback(undefined, 'Success');
        }
    );
}

/**
 * @param {{data: {hostId: string}}} request
 * @param {function(number|undefined, string=)} callback
 */
function stopHttp(request, callback) {
    if (!request || !request.data || !request.data.hostId) {
        callback(returnCodes.BAD_REQUEST);
        return;
    }

    endpointService.proxyPostRequest(
        {
            targetType: endpointService.ADMIN_PROXY_TARGET_TYPES.SENTINEL,
            targetId: request.data.hostId
        },
        'stopHttp',
        {},
        /**
         * @param {number} status
         * @param {SentinelGenericResponse} response
         */
        function (status, response) {
            callback(undefined, 'Success');
        }
    );
}

/**
 * @param {{data: {nodeId: string}}} request
 * @param {function(errCode: number|null, SentinelGenericResponse=)} callback
 */
function stopNode(request, callback) {
    if (!request || !request.data || !request.data.nodeId) {
        callback(returnCodes.BAD_REQUEST);
        return;
    }

    proxyPostAgent(request.data.nodeId,
        'stopAgent',
        null,
        /**
         * @param {number|null} errCode
         * @param {string|null} err
         * @param {Object|undefined} data
         */
        function (errCode, err, data) {
            if (errCode) {
                logger.error('Failed to stop node. See sentinel.log and agent.log for details. ' + err);
                callback(errCode, {error: err});
                return;
            }

            writeBootTime(request.data.nodeId, null,
                function(err2) {
                    if(err2) {
                        logger.warn('failed to write boot time. ' + err2);
                    }
                });
            callback(null, data);
        });
}

/**
 * @param {string} hostId
 * @param {string} api
 * @param {function()|null} setDataFunction - setting the hostId by default
 * @param {function(errCode: number|null, SentinelGenericResponse=)} callback
 */
function postRequestAllNodes(hostId, api, setDataFunction, callback) {
    //TODO
    callback('not implemented');

    // endpointService.proxyPostRequest(
    //     {
    //         targetType: endpointService.ADMIN_PROXY_TARGET_TYPES.SENTINEL,
    //         targetId: hostId
    //     },
    //     api,
    //     (setDataFunction && setDataFunction()) || {hostId: hostId},
    //     /**
    //      * @param {number} status
    //      * @param {SentinelGenericResponse} response
    //      */
    //     function (status, response) {
    //         next(response.error, status, response.data);
    //     }
    // );
}

/**
 * @param {{data: {hostId: string}}} request
 * @param {function(errCode: number|null, SentinelGenericResponse=)} callback
 */
function startAllNodes(request, callback) {
    if (!request || !request.data || !request.data.hostId) {
        callback(returnCodes.BAD_REQUEST);
        return;
    }

    postRequestAllNodes(request.data.hostId, 'startAllAgents', null, callback);
}

/**
 * @param {{data: {hostId: string, distributionId}}} request
 * @param {function(errCode: number|null, SentinelGenericResponse=)} callback
 */
function installAllNodes(request, callback) {
    if (!request || !request.data || !request.data.hostId || !request.data.distributionId) {
        callback(returnCodes.BAD_REQUEST);
        return;
    }

    postRequestAllNodes(
        request.data.hostId,
        'installAllAgents',
        function () {
            return {
                hostId: request.data.hostId,
                distributionId: request.data.distributionId
            };
        },
        callback
    );
}

/**
 * @param {{data: {hostId: string}}} request
 * @param {function(errCode: number|null, SentinelGenericResponse=)} callback
 */
function uninstallAllNodes(request, callback) {
    if (!request || !request.data || !request.data.hostId) {
        callback(returnCodes.BAD_REQUEST);
        return;
    }

    postRequestAllNodes(
        request.data.hostId,
        'uninstallAllAgents',
        null,
        callback
    );
}

/**
 * @param {{data: {hostId: string}}} request
 * @param {function(errCode: number|null, SentinelGenericResponse=)} callback
 */
function stopAllNodes(request, callback) {
    if (!request || !request.data || !request.data.hostId) {
        callback(returnCodes.BAD_REQUEST);
        return;
    }

    postRequestAllNodes(request.data.hostId, 'stopAllAgents', null, callback);
}

/**
 * @param {{nodeId: string}} request
 * @param {function(number|undefined, SentinelGenericResponse=)} callback
 */
function checkAgentState(request, callback) {
    if (!request || !request.nodeId) {
        callback(returnCodes.BAD_REQUEST);
        return;
    }

    getHostInfoByNodeId(request.nodeId,
        function (err, hostInfo) {
            if (err) {
                callback(returnCodes.INTERNAL_ERROR, {error: err});
                return;
            }
            simpleFind(
                NODES_DATA_COLLECTION,
                {_id: new ObjectId(request.nodeId)},
                null,
                /**
                 * @param {Error|undefined} err2
                 * @param {NodeInfo} nodeInfo
                 */
                function (err2, nodeInfo) {
                    if (err2) {
                        callback(returnCodes.INTERNAL_ERROR, {error: err2.message});
                        return;
                    }
                    endpointService.proxyGetRequest(
                        {
                            targetType: endpointService.ADMIN_PROXY_TARGET_TYPES.SENTINEL,
                            targetId: String(hostInfo._id)
                        },
                        'checkAgent',
                        {agentUuid: nodeInfo.uuid},
                        callback
                    );
                }
            );
        });
}

/**
 * @param {string} nodeId
 * @param {number|null} bootTime
 * @param {function(string=)} callback
 */
function writeBootTime(nodeId, bootTime, callback) {
    defaultDb.collection(NODES_DATA_COLLECTION)
        .updateOne(
            {_id: new ObjectId(nodeId)},
            {$set: {bootTime: bootTime}},
            /** @param {MongoError} err */
            function (err) {
                if (err) {
                    callback(err.message);
                    return;
                }
                callback();
            }
        );
}

/**
 * @param {{data: {nodeId: string}}} request
 * @param {function(errCode: number|null, Object=)} callback
 */
function startNode(request, callback) {
    if (!request || !request.data || !request.data.nodeId) {
        callback(returnCodes.BAD_REQUEST);
        return;
    }

    proxyPostAgent(request.data.nodeId,
        'startAgent',
        null,
        /**
         * @param {number|null} errCode
         * @param {string|null} err
         * @param {{running: boolean}|undefined} data
         */
        function (errCode, err, data) {
            if (errCode) {
                logger.error('Failed to start node. ' + err);
                callback(errCode, {error: err});
                return;
            }

            if(data && data.running) {
                writeBootTime(request.data.nodeId, Date.now(),
                    function (err2) {
                        if (err2) {
                            logger.warn('failed to write boot time. ' + err2);
                        }
                    });
            }
            callback(null, data);
        });
}

/**
 * @param {{hostId: string}} data
 * @param {function(number|undefined, Array.<NodeInfo>=)} callback
 */
function listNodes(data, callback) {
    /** @type {Array.<NodeInfo>} */
    var nodeList;

    if (!data || !data.hostId) {
        callback(returnCodes.BAD_REQUEST);
        return;
    }
    async.waterfall([
            /** @param {function(Error|undefined, Array.<NodeInfo>=)} next */
                function (next) {
                simpleFindToArray(
                    NODES_DATA_COLLECTION,
                    {hostId: new ObjectId(data.hostId)},
                    null,
                    next
                );
            },
            /**
             * @param {Array.<NodeInfo>} nodes
             * @param {function(Error|undefined, Array.<DistributionInfo>)} next
             */
                function (nodes, next) {
                nodeList = nodes;
                simpleFindToArray(
                    DISTRIBUTIONS_DATA_COLLECTION,
                    {},
                    {_id: 1, version: 1, uploadTimestamp: 1},
                    next
                );
            },
            /**
             * @param {Array.<DistributionInfo>} distributionList
             * @param {function(string=)} next
             */
                function (distributionList, next) {
                var i;
                /** {Object.<DistributionInfo>} */
                var map = {};
                /** @type {DistributionInfo} */
                var curDistribution;

                for (i = 0; i < distributionList.length; i += 1) {
                    map[distributionList[i]._id] = distributionList[i];
                }

                for (i = 0; i < nodeList.length; i += 1) {
                    curDistribution = map[nodeList[i].distributionId];
                    if (curDistribution) {
                        nodeList[i].distributionTimestamp = curDistribution.uploadTimestamp;
                        nodeList[i].distributionVersion = curDistribution.version;
                    } else {
                        nodeList[i].distributionVersion = 'unknown';
                    }
                }
                next();
            }
        ],
        /** @param {Error|string|undefined} err */
        function (err) {
            if (err) {
                logger.error('failed  to list the nodes. ' + err.message || err);
                callback(returnCodes.INTERNAL_ERROR);
                return;
            }
            callback(returnCodes.OK, nodeList);
        });
}

/**
 * @param {{data: {
 *      host: HostInfo,
 *      name: string,
 *      udpPort: number,
 *      tcpPort: number,
 *      adminHost: string,
 *      adminPort: number,
 *      customData: JSON}}} request
 * @param {function(number|null, string)} callback
 */
function addNode(request, callback) {
    /** @type {HostInfo} */
    var hostInfo = request.data.host;
    /** @type {string} */
    var nodeName = request.data.name;
    /** @type {number} */
    var udpPort = request.data.udpPort;
    /** @type {number} */
    var tcpPort = request.data.tcpPort;
    /** @type {string} */
    var adminHost = request.data.adminHost;
    /** @type {number} */
    var adminPort = request.data.adminPort;
    /** @type {JSON} */
    var customData = request.data.customData;

    if (!hostInfo.category || !hostInfo.category.length ||
        !hostInfo.localAddress || !hostInfo.localAddress.length) {
        callback(returnCodes.BAD_REQUEST, 'Invalid parameter name or local address');
        return;
    }

    async.waterfall(
        [
            // 1. get the host
            function (next) {
                getHost(hostInfo, function (err, doc) {
                    if (err) {
                        next(err);
                        return;
                    }
                    if (!doc) {
                        next({code: returnCodes.BAD_REQUEST, msg: 'Host not found'});
                        return;
                    }
                    next(undefined, doc);
                });
            },
            // 2. write the node to the db
            function (host, next) {
                writeNodeData(
                    host,
                    nodeName,
                    udpPort,
                    tcpPort,
                    adminHost,
                    adminPort,
                    customData,
                    function (err) {
                        if (err) {
                            next(err);
                            return;
                        }
                        next();
                    }
                );
            }
        ],
        /**
         * @param {Error|{code: number, msg: string}|undefined|null} err
         */
        function (err) {
            if (err) {
                if (err.code && err.msg) {
                    callback(err.code, err.msg);
                    return;
                }
                dbErrorHelper(err, callback);
                return;
            }
            callback(null, 'Success');
        }
    );
}

module.exports = {
    setup: setup,
    plugins: {
        uploadDistributionArchive: {
            method: 'POST',
            callback: uploadDistributionArchive,
            anonymous: true,
            inContentType: 'multipart/form-data',
            outContentType: 'text/plain'
        },
        uploadSentinelArchive: {
            method: 'POST',
            callback: uploadSentinelArchive,
            anonymous: true,
            inContentType: 'multipart/form-data',
            outContentType: 'text/plain'
        },
        installHost: {
            method: 'POST',
            callback: installHost,
            anonymous: true,
            inContentType: 'multipart/form-data',
            outContentType: 'text/plain'
        },
        listHosts: {
            method: 'GET',
            callback: listHosts,
            anonymous: true,
            contentType: 'application/json'
        },
        listDistributions: {
            method: 'GET',
            callback: listDistributions,
            anonymous: true,
            contentType: 'application/json'
        },
        listNodes: {
            method: 'GET',
            callback: listNodes,
            anonymous: true,
            contentType: 'application/json'
        },
        checkHttpInstall: {
            method: 'GET',
            callback: checkHttpInstall,
            anonymous: true,
            contentType: 'application/json'
        },
        checkAgentInstall: {
            method: 'GET',
            callback: checkAgentInstall,
            anonymous: true,
            contentType: 'application/json'
        },
        checkHttpState: {
            method: 'GET',
            callback: checkHttpState,
            anonymous: true,
            contentType: 'application/json'
        },
        checkAgentState: {
            method: 'GET',
            callback: checkAgentState,
            anonymous: true,
            contentType: 'application/json'
        },
        getNodeCount: {
            method: 'GET',
            callback: getNodeCount,
            anonymous: true,
            contentType: 'application/json'
        },
        addNode: {
            method: 'POST',
            callback: addNode,
            anonymous: true,
            inContentType: 'application/json',
            outContentType: 'text/plain'
        },
        uninstallAgent: {
            method: 'POST',
            callback: uninstallAgent,
            anonymous: true,
            contentType: 'application/json'
        },
        removeDistribution: {
            method: 'POST',
            callback: removeDistribution,
            anonymous: true,
            inContentType: 'application/json',
            outContentType: 'text/plain'
        },
        uninstallHost: {
            method: 'POST',
            callback: uninstallHost,
            anonymous: true,
            inContentType: 'application/json',
            outContentType: 'text/plain'
        },
        reinstallSentinel: {
            method: 'POST',
            callback: reinstallSentinel,
            anonymous: true,
            inContentType: 'application/json',
            outContentType: 'text/plain'
        },
        updateNode: {
            method: 'POST',
            callback: updateNode,
            anonymous: true,
            inContentType: 'application/json',
            outContentType: 'text/plain'
        },
        updateDistribution: {
            method: 'POST',
            callback: updateDistribution,
            anonymous: true,
            inContentType: 'application/json',
            outContentType: 'text/plain'
        },
        downloadDistribution: {
            method: 'GET',
            callback: downloadDistribution,
            anonymous: true,
            contentType: 'application/octet-stream'
            // GET / Query {distributionId: string}
            // Response: file download
        },
        installAgent: {
            method: 'POST',
            callback: installAgent,
            anonymous: true,
            contentType: 'application/json'
        },
        installHttp: {
            method: 'POST',
            callback: installHttp,
            anonymous: true,
            inContentType: 'application/json',
            outContentType: 'text/plain'
        },
        stopHttp: {
            method: 'POST',
            callback: stopHttp,
            anonymous: true,
            inContentType: 'application/json',
            outContentType: 'text/plain'
        },
        startHttp: {
            method: 'POST',
            callback: startHttp,
            anonymous: true,
            inContentType: 'application/json',
            outContentType: 'text/plain'
        },
        startNode: {
            method: 'POST',
            callback: startNode,
            anonymous: true,
            contentType: 'application/json'
        },
        stopNode: {
            method: 'POST',
            callback: stopNode,
            anonymous: true,
            contentType: 'application/json'
        },
        stopAllNodes: {
            method: 'POST',
            callback: stopAllNodes,
            anonymous: true,
            contentType: 'application/json'
        },
        startAllNodes: {
            method: 'POST',
            callback: startAllNodes,
            anonymous: true,
            contentType: 'application/json'
        },
        installAllNodes: {
            method: 'POST',
            callback: installAllNodes,
            anonymous: true,
            contentType: 'application/json'
        },
        uninstallAllNodes: {
            method: 'POST',
            callback: uninstallAllNodes,
            anonymous: true,
            contentType: 'application/json'
        }
    }
};

/**
 * @typedef {Object} protobufUploadFileDescription
 * @property {string} name the file name
 * @property {string} uploadTempPath the temporary file created by the server to store the file
 * @property {string} tempPath the temporary path of the file renamed into the temporary directory
 */

/**
 * @typedef {Object} HostInfo
 * @property {ObjectID|string} _id
 * @property {string} category
 * @property {string} localAddress
 * @property {string} dns
 * @property {number} sentinelPort
 */

/**
 * @typedef {Object} AdminAccess
 * @property {string} host
 * @property {number} port
 */

/**
 * @typedef {Object} NodeInfo
 * @property {ObjectID|string} _id
 * @property {ObjectID|string} hostId
 * @property {string} uuid
 * @property {string} name
 * @property {number} udpPort
 * @property {number} tcpPort
 * @property {AdminAccess} adminAccess
 * @property {number} bootTime
 * @property {string} distributionId
 * @property {string} distributionVersion
 * @property {number} distributionTimestamp
 * @property {number} installTimestamp
 * @property {Object} customData
 */

/**
 * @typedef {Object} DistributionInfo
 * @property {ObjectID|string} _id
 * @property {string} category
 * @property {string} name
 * @property {string} version
 * @property {number} uploadTimestamp
 * @property {Buffer} data
 */

/**
 * @typedef {Object} SentinelGenericResponse
 * @property {string} [error]
 * @property {Object} [data]
 */
