/*global angular*/
'use strict';

//noinspection JSUnusedGlobalSymbols
angular.module('bitcraft-distribution-management').
    component('bitcraftDistributionManagement', {
        templateUrl: 'js/distribution-management/distribution-management.template.html',
        controller: [
            '$log', 'dialogs', 'Notification', 'DeploymentManagement',
            function distributionManagementController(
                $log,
                dialogs,
                Notification,
                DeploymentManagement
            ) {
                var self = this;

                self.hostCategory = 'http';
                self.version = '';

                var notifyCallback = function (evt) {
                    var progressPercentage = parseInt(100.0 * evt.loaded / evt.total, 10);

                    // we only display progression feedback when the upload takes time,
                    // so we discard the 100% notification for instantaneous upload
                    if (progressPercentage !== 100) {
                        $log.info('PROGRESS: ' + progressPercentage + '% ');
                    }
                };

                function refreshDistributions() {
                    DeploymentManagement.listDistributions().then(function (data) {
                        self.distributions = data;
                    }, function (resp) {
                        Notification.error({
                            message: 'Failed to retrieve distributions (' + resp.statusText + ')'
                        });
                    });
                }

                /** @param {DistributionInfo} distributionInfo */
                self.onRemoveDistribution = function (distributionInfo) {
                    var dlg = dialogs.confirm(
                        'Confirmation required',
                        'Do you want to remove the distribution?',
                        {size: 'sm'}
                    );
                    dlg.result.then(function (btn) {
                        DeploymentManagement.removeDistribution(
                            distributionInfo
                        ).then(function (data) {
                            refreshDistributions();
                            Notification.success({
                                message: 'Distribution successfully removed'
                            });
                        }, function (resp) {
                            Notification.error({
                                message: 'Failed to remove distribution (' + resp.statusText + ')'
                            });
                        });
                    });
                };

                /**
                 * @param {string} distributionId
                 * @param {string} version
                 */
                self.onUpdateDistribution = function (distributionId, version) {
                    DeploymentManagement.updateDistribution(
                        distributionId,
                        version
                    ).then(function (data) {
                        Notification.success({
                            message: 'Distribution successfully updated'
                        });
                    }, function (resp) {
                        Notification.error({
                            message: 'Failed to update distribution (' + resp.statusText + ')'
                        });
                    });
                };

                /**
                 * @param {Array.<File>} filesData
                 */
                self.onUpload = function (filesData) {
                    if (filesData && filesData.length > 0) {
                        DeploymentManagement.uploadDistribution(
                            self.hostCategory,
                            self.version,
                            filesData[0]
                        ).then(function () {
                            $log.info('Upload success');
                            refreshDistributions();
                            Notification.success({message: 'Upload succeeded'});
                            self.version = '';
                        }, function (resp) {
                            $log.error('Upload failed: ' + resp.data, resp.statusText);
                            Notification.error(
                                {message: 'Upload failed (' + resp.statusText + ': ' + resp.data + ')'}
                            );
                        }, notifyCallback);
                    }
                };

                /**
                 * @param {Array.<File>} filesData
                 */
                self.onUploadSentinel = function (filesData) {
                    if (filesData && filesData.length > 0) {
                        DeploymentManagement.uploadSentinel(
                            filesData[0]
                        ).then(function () {
                            $log.info('Upload success');
                            Notification.success({message: 'Upload succeeded'});
                        }, function (resp) {
                            $log.error('Upload failed: ' + resp.data, resp.statusText);
                            Notification.error(
                                {message: 'Upload failed (' + resp.statusText + ': ' + resp.data + ')'}
                            );
                        }, notifyCallback);
                    }
                };

                /**
                 * Checks if the distributions version is valid. Returns a message in case of error,
                 * otherwise returns undefined.
                 * @param {string|undefined} version
                 * @returns {string|undefined}
                 */
                self.checkVersion = function (version) {
                    if (typeof version !== 'string' || version.length === 0) {
                        return 'Distribution version cannot be empty';
                    }
                };

                self.onDownloadDistribution = DeploymentManagement.downloadDistribution;

                refreshDistributions();
            }
        ]
    });
