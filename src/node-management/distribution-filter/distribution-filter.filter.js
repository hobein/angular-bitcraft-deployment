/*global angular*/
'use strict';

var util = require('util')

angular.
    module('bitcraft-distribution.filter', []).
    filter('bitcraftDistributionFilter', function () {
        return function (input, param) {
            /** @type {Array.<DistributionInfo>} */
            var distributionList = input;
            /** @type {string} */
            var filterCategory = param;
            /** @type {Array.<DistributionInfo>} */
            var result = [];

            angular.forEach(distributionList, function (host) {
                if (host.category === filterCategory) {
                    result.push(host);
                }
            });

            return result;
        };
    });