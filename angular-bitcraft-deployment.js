(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
// shim for using process in browser
var process = module.exports = {};

// cached from whatever global is present so that test runners that stub it
// don't break things.  But we need to wrap it in a try catch in case it is
// wrapped in strict mode code which doesn't define any globals.  It's inside a
// function because try/catches deoptimize in certain engines.

var cachedSetTimeout;
var cachedClearTimeout;

function defaultSetTimout() {
    throw new Error('setTimeout has not been defined');
}
function defaultClearTimeout () {
    throw new Error('clearTimeout has not been defined');
}
(function () {
    try {
        if (typeof setTimeout === 'function') {
            cachedSetTimeout = setTimeout;
        } else {
            cachedSetTimeout = defaultSetTimout;
        }
    } catch (e) {
        cachedSetTimeout = defaultSetTimout;
    }
    try {
        if (typeof clearTimeout === 'function') {
            cachedClearTimeout = clearTimeout;
        } else {
            cachedClearTimeout = defaultClearTimeout;
        }
    } catch (e) {
        cachedClearTimeout = defaultClearTimeout;
    }
} ())
function runTimeout(fun) {
    if (cachedSetTimeout === setTimeout) {
        //normal enviroments in sane situations
        return setTimeout(fun, 0);
    }
    // if setTimeout wasn't available but was latter defined
    if ((cachedSetTimeout === defaultSetTimout || !cachedSetTimeout) && setTimeout) {
        cachedSetTimeout = setTimeout;
        return setTimeout(fun, 0);
    }
    try {
        // when when somebody has screwed with setTimeout but no I.E. maddness
        return cachedSetTimeout(fun, 0);
    } catch(e){
        try {
            // When we are in I.E. but the script has been evaled so I.E. doesn't trust the global object when called normally
            return cachedSetTimeout.call(null, fun, 0);
        } catch(e){
            // same as above but when it's a version of I.E. that must have the global object for 'this', hopfully our context correct otherwise it will throw a global error
            return cachedSetTimeout.call(this, fun, 0);
        }
    }


}
function runClearTimeout(marker) {
    if (cachedClearTimeout === clearTimeout) {
        //normal enviroments in sane situations
        return clearTimeout(marker);
    }
    // if clearTimeout wasn't available but was latter defined
    if ((cachedClearTimeout === defaultClearTimeout || !cachedClearTimeout) && clearTimeout) {
        cachedClearTimeout = clearTimeout;
        return clearTimeout(marker);
    }
    try {
        // when when somebody has screwed with setTimeout but no I.E. maddness
        return cachedClearTimeout(marker);
    } catch (e){
        try {
            // When we are in I.E. but the script has been evaled so I.E. doesn't  trust the global object when called normally
            return cachedClearTimeout.call(null, marker);
        } catch (e){
            // same as above but when it's a version of I.E. that must have the global object for 'this', hopfully our context correct otherwise it will throw a global error.
            // Some versions of I.E. have different rules for clearTimeout vs setTimeout
            return cachedClearTimeout.call(this, marker);
        }
    }



}
var queue = [];
var draining = false;
var currentQueue;
var queueIndex = -1;

function cleanUpNextTick() {
    if (!draining || !currentQueue) {
        return;
    }
    draining = false;
    if (currentQueue.length) {
        queue = currentQueue.concat(queue);
    } else {
        queueIndex = -1;
    }
    if (queue.length) {
        drainQueue();
    }
}

function drainQueue() {
    if (draining) {
        return;
    }
    var timeout = runTimeout(cleanUpNextTick);
    draining = true;

    var len = queue.length;
    while(len) {
        currentQueue = queue;
        queue = [];
        while (++queueIndex < len) {
            if (currentQueue) {
                currentQueue[queueIndex].run();
            }
        }
        queueIndex = -1;
        len = queue.length;
    }
    currentQueue = null;
    draining = false;
    runClearTimeout(timeout);
}

process.nextTick = function (fun) {
    var args = new Array(arguments.length - 1);
    if (arguments.length > 1) {
        for (var i = 1; i < arguments.length; i++) {
            args[i - 1] = arguments[i];
        }
    }
    queue.push(new Item(fun, args));
    if (queue.length === 1 && !draining) {
        runTimeout(drainQueue);
    }
};

// v8 likes predictible objects
function Item(fun, array) {
    this.fun = fun;
    this.array = array;
}
Item.prototype.run = function () {
    this.fun.apply(null, this.array);
};
process.title = 'browser';
process.browser = true;
process.env = {};
process.argv = [];
process.version = ''; // empty string to avoid regexp issues
process.versions = {};

function noop() {}

process.on = noop;
process.addListener = noop;
process.once = noop;
process.off = noop;
process.removeListener = noop;
process.removeAllListeners = noop;
process.emit = noop;

process.binding = function (name) {
    throw new Error('process.binding is not supported');
};

process.cwd = function () { return '/' };
process.chdir = function (dir) {
    throw new Error('process.chdir is not supported');
};
process.umask = function() { return 0; };

},{}],2:[function(require,module,exports){
if (typeof Object.create === 'function') {
  // implementation from standard node.js 'util' module
  module.exports = function inherits(ctor, superCtor) {
    ctor.super_ = superCtor
    ctor.prototype = Object.create(superCtor.prototype, {
      constructor: {
        value: ctor,
        enumerable: false,
        writable: true,
        configurable: true
      }
    });
  };
} else {
  // old school shim for old browsers
  module.exports = function inherits(ctor, superCtor) {
    ctor.super_ = superCtor
    var TempCtor = function () {}
    TempCtor.prototype = superCtor.prototype
    ctor.prototype = new TempCtor()
    ctor.prototype.constructor = ctor
  }
}

},{}],3:[function(require,module,exports){
module.exports = function isBuffer(arg) {
  return arg && typeof arg === 'object'
    && typeof arg.copy === 'function'
    && typeof arg.fill === 'function'
    && typeof arg.readUInt8 === 'function';
}
},{}],4:[function(require,module,exports){
(function (process,global){
// Copyright Joyent, Inc. and other Node contributors.
//
// Permission is hereby granted, free of charge, to any person obtaining a
// copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the
// following conditions:
//
// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
// OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN
// NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
// USE OR OTHER DEALINGS IN THE SOFTWARE.

var formatRegExp = /%[sdj%]/g;
exports.format = function(f) {
  if (!isString(f)) {
    var objects = [];
    for (var i = 0; i < arguments.length; i++) {
      objects.push(inspect(arguments[i]));
    }
    return objects.join(' ');
  }

  var i = 1;
  var args = arguments;
  var len = args.length;
  var str = String(f).replace(formatRegExp, function(x) {
    if (x === '%%') return '%';
    if (i >= len) return x;
    switch (x) {
      case '%s': return String(args[i++]);
      case '%d': return Number(args[i++]);
      case '%j':
        try {
          return JSON.stringify(args[i++]);
        } catch (_) {
          return '[Circular]';
        }
      default:
        return x;
    }
  });
  for (var x = args[i]; i < len; x = args[++i]) {
    if (isNull(x) || !isObject(x)) {
      str += ' ' + x;
    } else {
      str += ' ' + inspect(x);
    }
  }
  return str;
};


// Mark that a method should not be used.
// Returns a modified function which warns once by default.
// If --no-deprecation is set, then it is a no-op.
exports.deprecate = function(fn, msg) {
  // Allow for deprecating things in the process of starting up.
  if (isUndefined(global.process)) {
    return function() {
      return exports.deprecate(fn, msg).apply(this, arguments);
    };
  }

  if (process.noDeprecation === true) {
    return fn;
  }

  var warned = false;
  function deprecated() {
    if (!warned) {
      if (process.throwDeprecation) {
        throw new Error(msg);
      } else if (process.traceDeprecation) {
        console.trace(msg);
      } else {
        console.error(msg);
      }
      warned = true;
    }
    return fn.apply(this, arguments);
  }

  return deprecated;
};


var debugs = {};
var debugEnviron;
exports.debuglog = function(set) {
  if (isUndefined(debugEnviron))
    debugEnviron = process.env.NODE_DEBUG || '';
  set = set.toUpperCase();
  if (!debugs[set]) {
    if (new RegExp('\\b' + set + '\\b', 'i').test(debugEnviron)) {
      var pid = process.pid;
      debugs[set] = function() {
        var msg = exports.format.apply(exports, arguments);
        console.error('%s %d: %s', set, pid, msg);
      };
    } else {
      debugs[set] = function() {};
    }
  }
  return debugs[set];
};


/**
 * Echos the value of a value. Trys to print the value out
 * in the best way possible given the different types.
 *
 * @param {Object} obj The object to print out.
 * @param {Object} opts Optional options object that alters the output.
 */
/* legacy: obj, showHidden, depth, colors*/
function inspect(obj, opts) {
  // default options
  var ctx = {
    seen: [],
    stylize: stylizeNoColor
  };
  // legacy...
  if (arguments.length >= 3) ctx.depth = arguments[2];
  if (arguments.length >= 4) ctx.colors = arguments[3];
  if (isBoolean(opts)) {
    // legacy...
    ctx.showHidden = opts;
  } else if (opts) {
    // got an "options" object
    exports._extend(ctx, opts);
  }
  // set default options
  if (isUndefined(ctx.showHidden)) ctx.showHidden = false;
  if (isUndefined(ctx.depth)) ctx.depth = 2;
  if (isUndefined(ctx.colors)) ctx.colors = false;
  if (isUndefined(ctx.customInspect)) ctx.customInspect = true;
  if (ctx.colors) ctx.stylize = stylizeWithColor;
  return formatValue(ctx, obj, ctx.depth);
}
exports.inspect = inspect;


// http://en.wikipedia.org/wiki/ANSI_escape_code#graphics
inspect.colors = {
  'bold' : [1, 22],
  'italic' : [3, 23],
  'underline' : [4, 24],
  'inverse' : [7, 27],
  'white' : [37, 39],
  'grey' : [90, 39],
  'black' : [30, 39],
  'blue' : [34, 39],
  'cyan' : [36, 39],
  'green' : [32, 39],
  'magenta' : [35, 39],
  'red' : [31, 39],
  'yellow' : [33, 39]
};

// Don't use 'blue' not visible on cmd.exe
inspect.styles = {
  'special': 'cyan',
  'number': 'yellow',
  'boolean': 'yellow',
  'undefined': 'grey',
  'null': 'bold',
  'string': 'green',
  'date': 'magenta',
  // "name": intentionally not styling
  'regexp': 'red'
};


function stylizeWithColor(str, styleType) {
  var style = inspect.styles[styleType];

  if (style) {
    return '\u001b[' + inspect.colors[style][0] + 'm' + str +
           '\u001b[' + inspect.colors[style][1] + 'm';
  } else {
    return str;
  }
}


function stylizeNoColor(str, styleType) {
  return str;
}


function arrayToHash(array) {
  var hash = {};

  array.forEach(function(val, idx) {
    hash[val] = true;
  });

  return hash;
}


function formatValue(ctx, value, recurseTimes) {
  // Provide a hook for user-specified inspect functions.
  // Check that value is an object with an inspect function on it
  if (ctx.customInspect &&
      value &&
      isFunction(value.inspect) &&
      // Filter out the util module, it's inspect function is special
      value.inspect !== exports.inspect &&
      // Also filter out any prototype objects using the circular check.
      !(value.constructor && value.constructor.prototype === value)) {
    var ret = value.inspect(recurseTimes, ctx);
    if (!isString(ret)) {
      ret = formatValue(ctx, ret, recurseTimes);
    }
    return ret;
  }

  // Primitive types cannot have properties
  var primitive = formatPrimitive(ctx, value);
  if (primitive) {
    return primitive;
  }

  // Look up the keys of the object.
  var keys = Object.keys(value);
  var visibleKeys = arrayToHash(keys);

  if (ctx.showHidden) {
    keys = Object.getOwnPropertyNames(value);
  }

  // IE doesn't make error fields non-enumerable
  // http://msdn.microsoft.com/en-us/library/ie/dww52sbt(v=vs.94).aspx
  if (isError(value)
      && (keys.indexOf('message') >= 0 || keys.indexOf('description') >= 0)) {
    return formatError(value);
  }

  // Some type of object without properties can be shortcutted.
  if (keys.length === 0) {
    if (isFunction(value)) {
      var name = value.name ? ': ' + value.name : '';
      return ctx.stylize('[Function' + name + ']', 'special');
    }
    if (isRegExp(value)) {
      return ctx.stylize(RegExp.prototype.toString.call(value), 'regexp');
    }
    if (isDate(value)) {
      return ctx.stylize(Date.prototype.toString.call(value), 'date');
    }
    if (isError(value)) {
      return formatError(value);
    }
  }

  var base = '', array = false, braces = ['{', '}'];

  // Make Array say that they are Array
  if (isArray(value)) {
    array = true;
    braces = ['[', ']'];
  }

  // Make functions say that they are functions
  if (isFunction(value)) {
    var n = value.name ? ': ' + value.name : '';
    base = ' [Function' + n + ']';
  }

  // Make RegExps say that they are RegExps
  if (isRegExp(value)) {
    base = ' ' + RegExp.prototype.toString.call(value);
  }

  // Make dates with properties first say the date
  if (isDate(value)) {
    base = ' ' + Date.prototype.toUTCString.call(value);
  }

  // Make error with message first say the error
  if (isError(value)) {
    base = ' ' + formatError(value);
  }

  if (keys.length === 0 && (!array || value.length == 0)) {
    return braces[0] + base + braces[1];
  }

  if (recurseTimes < 0) {
    if (isRegExp(value)) {
      return ctx.stylize(RegExp.prototype.toString.call(value), 'regexp');
    } else {
      return ctx.stylize('[Object]', 'special');
    }
  }

  ctx.seen.push(value);

  var output;
  if (array) {
    output = formatArray(ctx, value, recurseTimes, visibleKeys, keys);
  } else {
    output = keys.map(function(key) {
      return formatProperty(ctx, value, recurseTimes, visibleKeys, key, array);
    });
  }

  ctx.seen.pop();

  return reduceToSingleString(output, base, braces);
}


function formatPrimitive(ctx, value) {
  if (isUndefined(value))
    return ctx.stylize('undefined', 'undefined');
  if (isString(value)) {
    var simple = '\'' + JSON.stringify(value).replace(/^"|"$/g, '')
                                             .replace(/'/g, "\\'")
                                             .replace(/\\"/g, '"') + '\'';
    return ctx.stylize(simple, 'string');
  }
  if (isNumber(value))
    return ctx.stylize('' + value, 'number');
  if (isBoolean(value))
    return ctx.stylize('' + value, 'boolean');
  // For some reason typeof null is "object", so special case here.
  if (isNull(value))
    return ctx.stylize('null', 'null');
}


function formatError(value) {
  return '[' + Error.prototype.toString.call(value) + ']';
}


function formatArray(ctx, value, recurseTimes, visibleKeys, keys) {
  var output = [];
  for (var i = 0, l = value.length; i < l; ++i) {
    if (hasOwnProperty(value, String(i))) {
      output.push(formatProperty(ctx, value, recurseTimes, visibleKeys,
          String(i), true));
    } else {
      output.push('');
    }
  }
  keys.forEach(function(key) {
    if (!key.match(/^\d+$/)) {
      output.push(formatProperty(ctx, value, recurseTimes, visibleKeys,
          key, true));
    }
  });
  return output;
}


function formatProperty(ctx, value, recurseTimes, visibleKeys, key, array) {
  var name, str, desc;
  desc = Object.getOwnPropertyDescriptor(value, key) || { value: value[key] };
  if (desc.get) {
    if (desc.set) {
      str = ctx.stylize('[Getter/Setter]', 'special');
    } else {
      str = ctx.stylize('[Getter]', 'special');
    }
  } else {
    if (desc.set) {
      str = ctx.stylize('[Setter]', 'special');
    }
  }
  if (!hasOwnProperty(visibleKeys, key)) {
    name = '[' + key + ']';
  }
  if (!str) {
    if (ctx.seen.indexOf(desc.value) < 0) {
      if (isNull(recurseTimes)) {
        str = formatValue(ctx, desc.value, null);
      } else {
        str = formatValue(ctx, desc.value, recurseTimes - 1);
      }
      if (str.indexOf('\n') > -1) {
        if (array) {
          str = str.split('\n').map(function(line) {
            return '  ' + line;
          }).join('\n').substr(2);
        } else {
          str = '\n' + str.split('\n').map(function(line) {
            return '   ' + line;
          }).join('\n');
        }
      }
    } else {
      str = ctx.stylize('[Circular]', 'special');
    }
  }
  if (isUndefined(name)) {
    if (array && key.match(/^\d+$/)) {
      return str;
    }
    name = JSON.stringify('' + key);
    if (name.match(/^"([a-zA-Z_][a-zA-Z_0-9]*)"$/)) {
      name = name.substr(1, name.length - 2);
      name = ctx.stylize(name, 'name');
    } else {
      name = name.replace(/'/g, "\\'")
                 .replace(/\\"/g, '"')
                 .replace(/(^"|"$)/g, "'");
      name = ctx.stylize(name, 'string');
    }
  }

  return name + ': ' + str;
}


function reduceToSingleString(output, base, braces) {
  var numLinesEst = 0;
  var length = output.reduce(function(prev, cur) {
    numLinesEst++;
    if (cur.indexOf('\n') >= 0) numLinesEst++;
    return prev + cur.replace(/\u001b\[\d\d?m/g, '').length + 1;
  }, 0);

  if (length > 60) {
    return braces[0] +
           (base === '' ? '' : base + '\n ') +
           ' ' +
           output.join(',\n  ') +
           ' ' +
           braces[1];
  }

  return braces[0] + base + ' ' + output.join(', ') + ' ' + braces[1];
}


// NOTE: These type checking functions intentionally don't use `instanceof`
// because it is fragile and can be easily faked with `Object.create()`.
function isArray(ar) {
  return Array.isArray(ar);
}
exports.isArray = isArray;

function isBoolean(arg) {
  return typeof arg === 'boolean';
}
exports.isBoolean = isBoolean;

function isNull(arg) {
  return arg === null;
}
exports.isNull = isNull;

function isNullOrUndefined(arg) {
  return arg == null;
}
exports.isNullOrUndefined = isNullOrUndefined;

function isNumber(arg) {
  return typeof arg === 'number';
}
exports.isNumber = isNumber;

function isString(arg) {
  return typeof arg === 'string';
}
exports.isString = isString;

function isSymbol(arg) {
  return typeof arg === 'symbol';
}
exports.isSymbol = isSymbol;

function isUndefined(arg) {
  return arg === void 0;
}
exports.isUndefined = isUndefined;

function isRegExp(re) {
  return isObject(re) && objectToString(re) === '[object RegExp]';
}
exports.isRegExp = isRegExp;

function isObject(arg) {
  return typeof arg === 'object' && arg !== null;
}
exports.isObject = isObject;

function isDate(d) {
  return isObject(d) && objectToString(d) === '[object Date]';
}
exports.isDate = isDate;

function isError(e) {
  return isObject(e) &&
      (objectToString(e) === '[object Error]' || e instanceof Error);
}
exports.isError = isError;

function isFunction(arg) {
  return typeof arg === 'function';
}
exports.isFunction = isFunction;

function isPrimitive(arg) {
  return arg === null ||
         typeof arg === 'boolean' ||
         typeof arg === 'number' ||
         typeof arg === 'string' ||
         typeof arg === 'symbol' ||  // ES6 symbol
         typeof arg === 'undefined';
}
exports.isPrimitive = isPrimitive;

exports.isBuffer = require('./support/isBuffer');

function objectToString(o) {
  return Object.prototype.toString.call(o);
}


function pad(n) {
  return n < 10 ? '0' + n.toString(10) : n.toString(10);
}


var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep',
              'Oct', 'Nov', 'Dec'];

// 26 Feb 16:19:34
function timestamp() {
  var d = new Date();
  var time = [pad(d.getHours()),
              pad(d.getMinutes()),
              pad(d.getSeconds())].join(':');
  return [d.getDate(), months[d.getMonth()], time].join(' ');
}


// log is just a thin wrapper to console.log that prepends a timestamp
exports.log = function() {
  console.log('%s - %s', timestamp(), exports.format.apply(exports, arguments));
};


/**
 * Inherit the prototype methods from one constructor into another.
 *
 * The Function.prototype.inherits from lang.js rewritten as a standalone
 * function (not on Function.prototype). NOTE: If this file is to be loaded
 * during bootstrapping this function needs to be rewritten using some native
 * functions as prototype setup using normal JavaScript does not work as
 * expected during bootstrapping (see mirror.js in r114903).
 *
 * @param {function} ctor Constructor function which needs to inherit the
 *     prototype.
 * @param {function} superCtor Constructor function to inherit prototype from.
 */
exports.inherits = require('inherits');

exports._extend = function(origin, add) {
  // Don't do anything if add isn't an object
  if (!add || !isObject(add)) return origin;

  var keys = Object.keys(add);
  var i = keys.length;
  while (i--) {
    origin[keys[i]] = add[keys[i]];
  }
  return origin;
};

function hasOwnProperty(obj, prop) {
  return Object.prototype.hasOwnProperty.call(obj, prop);
}

}).call(this,require('_process'),typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})
},{"./support/isBuffer":3,"_process":1,"inherits":2}],5:[function(require,module,exports){
(function (process,global){
/*!
 * async
 * https://github.com/caolan/async
 *
 * Copyright 2010-2014 Caolan McMahon
 * Released under the MIT license
 */
(function () {

    var async = {};
    function noop() {}
    function identity(v) {
        return v;
    }
    function toBool(v) {
        return !!v;
    }
    function notId(v) {
        return !v;
    }

    // global on the server, window in the browser
    var previous_async;

    // Establish the root object, `window` (`self`) in the browser, `global`
    // on the server, or `this` in some virtual machines. We use `self`
    // instead of `window` for `WebWorker` support.
    var root = typeof self === 'object' && self.self === self && self ||
            typeof global === 'object' && global.global === global && global ||
            this;

    if (root != null) {
        previous_async = root.async;
    }

    async.noConflict = function () {
        root.async = previous_async;
        return async;
    };

    function only_once(fn) {
        return function() {
            if (fn === null) throw new Error("Callback was already called.");
            fn.apply(this, arguments);
            fn = null;
        };
    }

    function _once(fn) {
        return function() {
            if (fn === null) return;
            fn.apply(this, arguments);
            fn = null;
        };
    }

    //// cross-browser compatiblity functions ////

    var _toString = Object.prototype.toString;

    var _isArray = Array.isArray || function (obj) {
        return _toString.call(obj) === '[object Array]';
    };

    // Ported from underscore.js isObject
    var _isObject = function(obj) {
        var type = typeof obj;
        return type === 'function' || type === 'object' && !!obj;
    };

    function _isArrayLike(arr) {
        return _isArray(arr) || (
            // has a positive integer length property
            typeof arr.length === "number" &&
            arr.length >= 0 &&
            arr.length % 1 === 0
        );
    }

    function _arrayEach(arr, iterator) {
        var index = -1,
            length = arr.length;

        while (++index < length) {
            iterator(arr[index], index, arr);
        }
    }

    function _map(arr, iterator) {
        var index = -1,
            length = arr.length,
            result = Array(length);

        while (++index < length) {
            result[index] = iterator(arr[index], index, arr);
        }
        return result;
    }

    function _range(count) {
        return _map(Array(count), function (v, i) { return i; });
    }

    function _reduce(arr, iterator, memo) {
        _arrayEach(arr, function (x, i, a) {
            memo = iterator(memo, x, i, a);
        });
        return memo;
    }

    function _forEachOf(object, iterator) {
        _arrayEach(_keys(object), function (key) {
            iterator(object[key], key);
        });
    }

    function _indexOf(arr, item) {
        for (var i = 0; i < arr.length; i++) {
            if (arr[i] === item) return i;
        }
        return -1;
    }

    var _keys = Object.keys || function (obj) {
        var keys = [];
        for (var k in obj) {
            if (obj.hasOwnProperty(k)) {
                keys.push(k);
            }
        }
        return keys;
    };

    function _keyIterator(coll) {
        var i = -1;
        var len;
        var keys;
        if (_isArrayLike(coll)) {
            len = coll.length;
            return function next() {
                i++;
                return i < len ? i : null;
            };
        } else {
            keys = _keys(coll);
            len = keys.length;
            return function next() {
                i++;
                return i < len ? keys[i] : null;
            };
        }
    }

    // Similar to ES6's rest param (http://ariya.ofilabs.com/2013/03/es6-and-rest-parameter.html)
    // This accumulates the arguments passed into an array, after a given index.
    // From underscore.js (https://github.com/jashkenas/underscore/pull/2140).
    function _restParam(func, startIndex) {
        startIndex = startIndex == null ? func.length - 1 : +startIndex;
        return function() {
            var length = Math.max(arguments.length - startIndex, 0);
            var rest = Array(length);
            for (var index = 0; index < length; index++) {
                rest[index] = arguments[index + startIndex];
            }
            switch (startIndex) {
                case 0: return func.call(this, rest);
                case 1: return func.call(this, arguments[0], rest);
            }
            // Currently unused but handle cases outside of the switch statement:
            // var args = Array(startIndex + 1);
            // for (index = 0; index < startIndex; index++) {
            //     args[index] = arguments[index];
            // }
            // args[startIndex] = rest;
            // return func.apply(this, args);
        };
    }

    function _withoutIndex(iterator) {
        return function (value, index, callback) {
            return iterator(value, callback);
        };
    }

    //// exported async module functions ////

    //// nextTick implementation with browser-compatible fallback ////

    // capture the global reference to guard against fakeTimer mocks
    var _setImmediate = typeof setImmediate === 'function' && setImmediate;

    var _delay = _setImmediate ? function(fn) {
        // not a direct alias for IE10 compatibility
        _setImmediate(fn);
    } : function(fn) {
        setTimeout(fn, 0);
    };

    if (typeof process === 'object' && typeof process.nextTick === 'function') {
        async.nextTick = process.nextTick;
    } else {
        async.nextTick = _delay;
    }
    async.setImmediate = _setImmediate ? _delay : async.nextTick;


    async.forEach =
    async.each = function (arr, iterator, callback) {
        return async.eachOf(arr, _withoutIndex(iterator), callback);
    };

    async.forEachSeries =
    async.eachSeries = function (arr, iterator, callback) {
        return async.eachOfSeries(arr, _withoutIndex(iterator), callback);
    };


    async.forEachLimit =
    async.eachLimit = function (arr, limit, iterator, callback) {
        return _eachOfLimit(limit)(arr, _withoutIndex(iterator), callback);
    };

    async.forEachOf =
    async.eachOf = function (object, iterator, callback) {
        callback = _once(callback || noop);
        object = object || [];

        var iter = _keyIterator(object);
        var key, completed = 0;

        while ((key = iter()) != null) {
            completed += 1;
            iterator(object[key], key, only_once(done));
        }

        if (completed === 0) callback(null);

        function done(err) {
            completed--;
            if (err) {
                callback(err);
            }
            // Check key is null in case iterator isn't exhausted
            // and done resolved synchronously.
            else if (key === null && completed <= 0) {
                callback(null);
            }
        }
    };

    async.forEachOfSeries =
    async.eachOfSeries = function (obj, iterator, callback) {
        callback = _once(callback || noop);
        obj = obj || [];
        var nextKey = _keyIterator(obj);
        var key = nextKey();
        function iterate() {
            var sync = true;
            if (key === null) {
                return callback(null);
            }
            iterator(obj[key], key, only_once(function (err) {
                if (err) {
                    callback(err);
                }
                else {
                    key = nextKey();
                    if (key === null) {
                        return callback(null);
                    } else {
                        if (sync) {
                            async.setImmediate(iterate);
                        } else {
                            iterate();
                        }
                    }
                }
            }));
            sync = false;
        }
        iterate();
    };



    async.forEachOfLimit =
    async.eachOfLimit = function (obj, limit, iterator, callback) {
        _eachOfLimit(limit)(obj, iterator, callback);
    };

    function _eachOfLimit(limit) {

        return function (obj, iterator, callback) {
            callback = _once(callback || noop);
            obj = obj || [];
            var nextKey = _keyIterator(obj);
            if (limit <= 0) {
                return callback(null);
            }
            var done = false;
            var running = 0;
            var errored = false;

            (function replenish () {
                if (done && running <= 0) {
                    return callback(null);
                }

                while (running < limit && !errored) {
                    var key = nextKey();
                    if (key === null) {
                        done = true;
                        if (running <= 0) {
                            callback(null);
                        }
                        return;
                    }
                    running += 1;
                    iterator(obj[key], key, only_once(function (err) {
                        running -= 1;
                        if (err) {
                            callback(err);
                            errored = true;
                        }
                        else {
                            replenish();
                        }
                    }));
                }
            })();
        };
    }


    function doParallel(fn) {
        return function (obj, iterator, callback) {
            return fn(async.eachOf, obj, iterator, callback);
        };
    }
    function doParallelLimit(fn) {
        return function (obj, limit, iterator, callback) {
            return fn(_eachOfLimit(limit), obj, iterator, callback);
        };
    }
    function doSeries(fn) {
        return function (obj, iterator, callback) {
            return fn(async.eachOfSeries, obj, iterator, callback);
        };
    }

    function _asyncMap(eachfn, arr, iterator, callback) {
        callback = _once(callback || noop);
        arr = arr || [];
        var results = _isArrayLike(arr) ? [] : {};
        eachfn(arr, function (value, index, callback) {
            iterator(value, function (err, v) {
                results[index] = v;
                callback(err);
            });
        }, function (err) {
            callback(err, results);
        });
    }

    async.map = doParallel(_asyncMap);
    async.mapSeries = doSeries(_asyncMap);
    async.mapLimit = doParallelLimit(_asyncMap);

    // reduce only has a series version, as doing reduce in parallel won't
    // work in many situations.
    async.inject =
    async.foldl =
    async.reduce = function (arr, memo, iterator, callback) {
        async.eachOfSeries(arr, function (x, i, callback) {
            iterator(memo, x, function (err, v) {
                memo = v;
                callback(err);
            });
        }, function (err) {
            callback(err, memo);
        });
    };

    async.foldr =
    async.reduceRight = function (arr, memo, iterator, callback) {
        var reversed = _map(arr, identity).reverse();
        async.reduce(reversed, memo, iterator, callback);
    };

    async.transform = function (arr, memo, iterator, callback) {
        if (arguments.length === 3) {
            callback = iterator;
            iterator = memo;
            memo = _isArray(arr) ? [] : {};
        }

        async.eachOf(arr, function(v, k, cb) {
            iterator(memo, v, k, cb);
        }, function(err) {
            callback(err, memo);
        });
    };

    function _filter(eachfn, arr, iterator, callback) {
        var results = [];
        eachfn(arr, function (x, index, callback) {
            iterator(x, function (v) {
                if (v) {
                    results.push({index: index, value: x});
                }
                callback();
            });
        }, function () {
            callback(_map(results.sort(function (a, b) {
                return a.index - b.index;
            }), function (x) {
                return x.value;
            }));
        });
    }

    async.select =
    async.filter = doParallel(_filter);

    async.selectLimit =
    async.filterLimit = doParallelLimit(_filter);

    async.selectSeries =
    async.filterSeries = doSeries(_filter);

    function _reject(eachfn, arr, iterator, callback) {
        _filter(eachfn, arr, function(value, cb) {
            iterator(value, function(v) {
                cb(!v);
            });
        }, callback);
    }
    async.reject = doParallel(_reject);
    async.rejectLimit = doParallelLimit(_reject);
    async.rejectSeries = doSeries(_reject);

    function _createTester(eachfn, check, getResult) {
        return function(arr, limit, iterator, cb) {
            function done() {
                if (cb) cb(getResult(false, void 0));
            }
            function iteratee(x, _, callback) {
                if (!cb) return callback();
                iterator(x, function (v) {
                    if (cb && check(v)) {
                        cb(getResult(true, x));
                        cb = iterator = false;
                    }
                    callback();
                });
            }
            if (arguments.length > 3) {
                eachfn(arr, limit, iteratee, done);
            } else {
                cb = iterator;
                iterator = limit;
                eachfn(arr, iteratee, done);
            }
        };
    }

    async.any =
    async.some = _createTester(async.eachOf, toBool, identity);

    async.someLimit = _createTester(async.eachOfLimit, toBool, identity);

    async.all =
    async.every = _createTester(async.eachOf, notId, notId);

    async.everyLimit = _createTester(async.eachOfLimit, notId, notId);

    function _findGetResult(v, x) {
        return x;
    }
    async.detect = _createTester(async.eachOf, identity, _findGetResult);
    async.detectSeries = _createTester(async.eachOfSeries, identity, _findGetResult);
    async.detectLimit = _createTester(async.eachOfLimit, identity, _findGetResult);

    async.sortBy = function (arr, iterator, callback) {
        async.map(arr, function (x, callback) {
            iterator(x, function (err, criteria) {
                if (err) {
                    callback(err);
                }
                else {
                    callback(null, {value: x, criteria: criteria});
                }
            });
        }, function (err, results) {
            if (err) {
                return callback(err);
            }
            else {
                callback(null, _map(results.sort(comparator), function (x) {
                    return x.value;
                }));
            }

        });

        function comparator(left, right) {
            var a = left.criteria, b = right.criteria;
            return a < b ? -1 : a > b ? 1 : 0;
        }
    };

    async.auto = function (tasks, concurrency, callback) {
        if (typeof arguments[1] === 'function') {
            // concurrency is optional, shift the args.
            callback = concurrency;
            concurrency = null;
        }
        callback = _once(callback || noop);
        var keys = _keys(tasks);
        var remainingTasks = keys.length;
        if (!remainingTasks) {
            return callback(null);
        }
        if (!concurrency) {
            concurrency = remainingTasks;
        }

        var results = {};
        var runningTasks = 0;

        var hasError = false;

        var listeners = [];
        function addListener(fn) {
            listeners.unshift(fn);
        }
        function removeListener(fn) {
            var idx = _indexOf(listeners, fn);
            if (idx >= 0) listeners.splice(idx, 1);
        }
        function taskComplete() {
            remainingTasks--;
            _arrayEach(listeners.slice(0), function (fn) {
                fn();
            });
        }

        addListener(function () {
            if (!remainingTasks) {
                callback(null, results);
            }
        });

        _arrayEach(keys, function (k) {
            if (hasError) return;
            var task = _isArray(tasks[k]) ? tasks[k]: [tasks[k]];
            var taskCallback = _restParam(function(err, args) {
                runningTasks--;
                if (args.length <= 1) {
                    args = args[0];
                }
                if (err) {
                    var safeResults = {};
                    _forEachOf(results, function(val, rkey) {
                        safeResults[rkey] = val;
                    });
                    safeResults[k] = args;
                    hasError = true;

                    callback(err, safeResults);
                }
                else {
                    results[k] = args;
                    async.setImmediate(taskComplete);
                }
            });
            var requires = task.slice(0, task.length - 1);
            // prevent dead-locks
            var len = requires.length;
            var dep;
            while (len--) {
                if (!(dep = tasks[requires[len]])) {
                    throw new Error('Has nonexistent dependency in ' + requires.join(', '));
                }
                if (_isArray(dep) && _indexOf(dep, k) >= 0) {
                    throw new Error('Has cyclic dependencies');
                }
            }
            function ready() {
                return runningTasks < concurrency && _reduce(requires, function (a, x) {
                    return (a && results.hasOwnProperty(x));
                }, true) && !results.hasOwnProperty(k);
            }
            if (ready()) {
                runningTasks++;
                task[task.length - 1](taskCallback, results);
            }
            else {
                addListener(listener);
            }
            function listener() {
                if (ready()) {
                    runningTasks++;
                    removeListener(listener);
                    task[task.length - 1](taskCallback, results);
                }
            }
        });
    };



    async.retry = function(times, task, callback) {
        var DEFAULT_TIMES = 5;
        var DEFAULT_INTERVAL = 0;

        var attempts = [];

        var opts = {
            times: DEFAULT_TIMES,
            interval: DEFAULT_INTERVAL
        };

        function parseTimes(acc, t){
            if(typeof t === 'number'){
                acc.times = parseInt(t, 10) || DEFAULT_TIMES;
            } else if(typeof t === 'object'){
                acc.times = parseInt(t.times, 10) || DEFAULT_TIMES;
                acc.interval = parseInt(t.interval, 10) || DEFAULT_INTERVAL;
            } else {
                throw new Error('Unsupported argument type for \'times\': ' + typeof t);
            }
        }

        var length = arguments.length;
        if (length < 1 || length > 3) {
            throw new Error('Invalid arguments - must be either (task), (task, callback), (times, task) or (times, task, callback)');
        } else if (length <= 2 && typeof times === 'function') {
            callback = task;
            task = times;
        }
        if (typeof times !== 'function') {
            parseTimes(opts, times);
        }
        opts.callback = callback;
        opts.task = task;

        function wrappedTask(wrappedCallback, wrappedResults) {
            function retryAttempt(task, finalAttempt) {
                return function(seriesCallback) {
                    task(function(err, result){
                        seriesCallback(!err || finalAttempt, {err: err, result: result});
                    }, wrappedResults);
                };
            }

            function retryInterval(interval){
                return function(seriesCallback){
                    setTimeout(function(){
                        seriesCallback(null);
                    }, interval);
                };
            }

            while (opts.times) {

                var finalAttempt = !(opts.times-=1);
                attempts.push(retryAttempt(opts.task, finalAttempt));
                if(!finalAttempt && opts.interval > 0){
                    attempts.push(retryInterval(opts.interval));
                }
            }

            async.series(attempts, function(done, data){
                data = data[data.length - 1];
                (wrappedCallback || opts.callback)(data.err, data.result);
            });
        }

        // If a callback is passed, run this as a controll flow
        return opts.callback ? wrappedTask() : wrappedTask;
    };

    async.waterfall = function (tasks, callback) {
        callback = _once(callback || noop);
        if (!_isArray(tasks)) {
            var err = new Error('First argument to waterfall must be an array of functions');
            return callback(err);
        }
        if (!tasks.length) {
            return callback();
        }
        function wrapIterator(iterator) {
            return _restParam(function (err, args) {
                if (err) {
                    callback.apply(null, [err].concat(args));
                }
                else {
                    var next = iterator.next();
                    if (next) {
                        args.push(wrapIterator(next));
                    }
                    else {
                        args.push(callback);
                    }
                    ensureAsync(iterator).apply(null, args);
                }
            });
        }
        wrapIterator(async.iterator(tasks))();
    };

    function _parallel(eachfn, tasks, callback) {
        callback = callback || noop;
        var results = _isArrayLike(tasks) ? [] : {};

        eachfn(tasks, function (task, key, callback) {
            task(_restParam(function (err, args) {
                if (args.length <= 1) {
                    args = args[0];
                }
                results[key] = args;
                callback(err);
            }));
        }, function (err) {
            callback(err, results);
        });
    }

    async.parallel = function (tasks, callback) {
        _parallel(async.eachOf, tasks, callback);
    };

    async.parallelLimit = function(tasks, limit, callback) {
        _parallel(_eachOfLimit(limit), tasks, callback);
    };

    async.series = function(tasks, callback) {
        _parallel(async.eachOfSeries, tasks, callback);
    };

    async.iterator = function (tasks) {
        function makeCallback(index) {
            function fn() {
                if (tasks.length) {
                    tasks[index].apply(null, arguments);
                }
                return fn.next();
            }
            fn.next = function () {
                return (index < tasks.length - 1) ? makeCallback(index + 1): null;
            };
            return fn;
        }
        return makeCallback(0);
    };

    async.apply = _restParam(function (fn, args) {
        return _restParam(function (callArgs) {
            return fn.apply(
                null, args.concat(callArgs)
            );
        });
    });

    function _concat(eachfn, arr, fn, callback) {
        var result = [];
        eachfn(arr, function (x, index, cb) {
            fn(x, function (err, y) {
                result = result.concat(y || []);
                cb(err);
            });
        }, function (err) {
            callback(err, result);
        });
    }
    async.concat = doParallel(_concat);
    async.concatSeries = doSeries(_concat);

    async.whilst = function (test, iterator, callback) {
        callback = callback || noop;
        if (test()) {
            var next = _restParam(function(err, args) {
                if (err) {
                    callback(err);
                } else if (test.apply(this, args)) {
                    iterator(next);
                } else {
                    callback.apply(null, [null].concat(args));
                }
            });
            iterator(next);
        } else {
            callback(null);
        }
    };

    async.doWhilst = function (iterator, test, callback) {
        var calls = 0;
        return async.whilst(function() {
            return ++calls <= 1 || test.apply(this, arguments);
        }, iterator, callback);
    };

    async.until = function (test, iterator, callback) {
        return async.whilst(function() {
            return !test.apply(this, arguments);
        }, iterator, callback);
    };

    async.doUntil = function (iterator, test, callback) {
        return async.doWhilst(iterator, function() {
            return !test.apply(this, arguments);
        }, callback);
    };

    async.during = function (test, iterator, callback) {
        callback = callback || noop;

        var next = _restParam(function(err, args) {
            if (err) {
                callback(err);
            } else {
                args.push(check);
                test.apply(this, args);
            }
        });

        var check = function(err, truth) {
            if (err) {
                callback(err);
            } else if (truth) {
                iterator(next);
            } else {
                callback(null);
            }
        };

        test(check);
    };

    async.doDuring = function (iterator, test, callback) {
        var calls = 0;
        async.during(function(next) {
            if (calls++ < 1) {
                next(null, true);
            } else {
                test.apply(this, arguments);
            }
        }, iterator, callback);
    };

    function _queue(worker, concurrency, payload) {
        if (concurrency == null) {
            concurrency = 1;
        }
        else if(concurrency === 0) {
            throw new Error('Concurrency must not be zero');
        }
        function _insert(q, data, pos, callback) {
            if (callback != null && typeof callback !== "function") {
                throw new Error("task callback must be a function");
            }
            q.started = true;
            if (!_isArray(data)) {
                data = [data];
            }
            if(data.length === 0 && q.idle()) {
                // call drain immediately if there are no tasks
                return async.setImmediate(function() {
                    q.drain();
                });
            }
            _arrayEach(data, function(task) {
                var item = {
                    data: task,
                    callback: callback || noop
                };

                if (pos) {
                    q.tasks.unshift(item);
                } else {
                    q.tasks.push(item);
                }

                if (q.tasks.length === q.concurrency) {
                    q.saturated();
                }
            });
            async.setImmediate(q.process);
        }
        function _next(q, tasks) {
            return function(){
                workers -= 1;

                var removed = false;
                var args = arguments;
                _arrayEach(tasks, function (task) {
                    _arrayEach(workersList, function (worker, index) {
                        if (worker === task && !removed) {
                            workersList.splice(index, 1);
                            removed = true;
                        }
                    });

                    task.callback.apply(task, args);
                });
                if (q.tasks.length + workers === 0) {
                    q.drain();
                }
                q.process();
            };
        }

        var workers = 0;
        var workersList = [];
        var q = {
            tasks: [],
            concurrency: concurrency,
            payload: payload,
            saturated: noop,
            empty: noop,
            drain: noop,
            started: false,
            paused: false,
            push: function (data, callback) {
                _insert(q, data, false, callback);
            },
            kill: function () {
                q.drain = noop;
                q.tasks = [];
            },
            unshift: function (data, callback) {
                _insert(q, data, true, callback);
            },
            process: function () {
                while(!q.paused && workers < q.concurrency && q.tasks.length){

                    var tasks = q.payload ?
                        q.tasks.splice(0, q.payload) :
                        q.tasks.splice(0, q.tasks.length);

                    var data = _map(tasks, function (task) {
                        return task.data;
                    });

                    if (q.tasks.length === 0) {
                        q.empty();
                    }
                    workers += 1;
                    workersList.push(tasks[0]);
                    var cb = only_once(_next(q, tasks));
                    worker(data, cb);
                }
            },
            length: function () {
                return q.tasks.length;
            },
            running: function () {
                return workers;
            },
            workersList: function () {
                return workersList;
            },
            idle: function() {
                return q.tasks.length + workers === 0;
            },
            pause: function () {
                q.paused = true;
            },
            resume: function () {
                if (q.paused === false) { return; }
                q.paused = false;
                var resumeCount = Math.min(q.concurrency, q.tasks.length);
                // Need to call q.process once per concurrent
                // worker to preserve full concurrency after pause
                for (var w = 1; w <= resumeCount; w++) {
                    async.setImmediate(q.process);
                }
            }
        };
        return q;
    }

    async.queue = function (worker, concurrency) {
        var q = _queue(function (items, cb) {
            worker(items[0], cb);
        }, concurrency, 1);

        return q;
    };

    async.priorityQueue = function (worker, concurrency) {

        function _compareTasks(a, b){
            return a.priority - b.priority;
        }

        function _binarySearch(sequence, item, compare) {
            var beg = -1,
                end = sequence.length - 1;
            while (beg < end) {
                var mid = beg + ((end - beg + 1) >>> 1);
                if (compare(item, sequence[mid]) >= 0) {
                    beg = mid;
                } else {
                    end = mid - 1;
                }
            }
            return beg;
        }

        function _insert(q, data, priority, callback) {
            if (callback != null && typeof callback !== "function") {
                throw new Error("task callback must be a function");
            }
            q.started = true;
            if (!_isArray(data)) {
                data = [data];
            }
            if(data.length === 0) {
                // call drain immediately if there are no tasks
                return async.setImmediate(function() {
                    q.drain();
                });
            }
            _arrayEach(data, function(task) {
                var item = {
                    data: task,
                    priority: priority,
                    callback: typeof callback === 'function' ? callback : noop
                };

                q.tasks.splice(_binarySearch(q.tasks, item, _compareTasks) + 1, 0, item);

                if (q.tasks.length === q.concurrency) {
                    q.saturated();
                }
                async.setImmediate(q.process);
            });
        }

        // Start with a normal queue
        var q = async.queue(worker, concurrency);

        // Override push to accept second parameter representing priority
        q.push = function (data, priority, callback) {
            _insert(q, data, priority, callback);
        };

        // Remove unshift function
        delete q.unshift;

        return q;
    };

    async.cargo = function (worker, payload) {
        return _queue(worker, 1, payload);
    };

    function _console_fn(name) {
        return _restParam(function (fn, args) {
            fn.apply(null, args.concat([_restParam(function (err, args) {
                if (typeof console === 'object') {
                    if (err) {
                        if (console.error) {
                            console.error(err);
                        }
                    }
                    else if (console[name]) {
                        _arrayEach(args, function (x) {
                            console[name](x);
                        });
                    }
                }
            })]));
        });
    }
    async.log = _console_fn('log');
    async.dir = _console_fn('dir');
    /*async.info = _console_fn('info');
    async.warn = _console_fn('warn');
    async.error = _console_fn('error');*/

    async.memoize = function (fn, hasher) {
        var memo = {};
        var queues = {};
        var has = Object.prototype.hasOwnProperty;
        hasher = hasher || identity;
        var memoized = _restParam(function memoized(args) {
            var callback = args.pop();
            var key = hasher.apply(null, args);
            if (has.call(memo, key)) {   
                async.setImmediate(function () {
                    callback.apply(null, memo[key]);
                });
            }
            else if (has.call(queues, key)) {
                queues[key].push(callback);
            }
            else {
                queues[key] = [callback];
                fn.apply(null, args.concat([_restParam(function (args) {
                    memo[key] = args;
                    var q = queues[key];
                    delete queues[key];
                    for (var i = 0, l = q.length; i < l; i++) {
                        q[i].apply(null, args);
                    }
                })]));
            }
        });
        memoized.memo = memo;
        memoized.unmemoized = fn;
        return memoized;
    };

    async.unmemoize = function (fn) {
        return function () {
            return (fn.unmemoized || fn).apply(null, arguments);
        };
    };

    function _times(mapper) {
        return function (count, iterator, callback) {
            mapper(_range(count), iterator, callback);
        };
    }

    async.times = _times(async.map);
    async.timesSeries = _times(async.mapSeries);
    async.timesLimit = function (count, limit, iterator, callback) {
        return async.mapLimit(_range(count), limit, iterator, callback);
    };

    async.seq = function (/* functions... */) {
        var fns = arguments;
        return _restParam(function (args) {
            var that = this;

            var callback = args[args.length - 1];
            if (typeof callback == 'function') {
                args.pop();
            } else {
                callback = noop;
            }

            async.reduce(fns, args, function (newargs, fn, cb) {
                fn.apply(that, newargs.concat([_restParam(function (err, nextargs) {
                    cb(err, nextargs);
                })]));
            },
            function (err, results) {
                callback.apply(that, [err].concat(results));
            });
        });
    };

    async.compose = function (/* functions... */) {
        return async.seq.apply(null, Array.prototype.reverse.call(arguments));
    };


    function _applyEach(eachfn) {
        return _restParam(function(fns, args) {
            var go = _restParam(function(args) {
                var that = this;
                var callback = args.pop();
                return eachfn(fns, function (fn, _, cb) {
                    fn.apply(that, args.concat([cb]));
                },
                callback);
            });
            if (args.length) {
                return go.apply(this, args);
            }
            else {
                return go;
            }
        });
    }

    async.applyEach = _applyEach(async.eachOf);
    async.applyEachSeries = _applyEach(async.eachOfSeries);


    async.forever = function (fn, callback) {
        var done = only_once(callback || noop);
        var task = ensureAsync(fn);
        function next(err) {
            if (err) {
                return done(err);
            }
            task(next);
        }
        next();
    };

    function ensureAsync(fn) {
        return _restParam(function (args) {
            var callback = args.pop();
            args.push(function () {
                var innerArgs = arguments;
                if (sync) {
                    async.setImmediate(function () {
                        callback.apply(null, innerArgs);
                    });
                } else {
                    callback.apply(null, innerArgs);
                }
            });
            var sync = true;
            fn.apply(this, args);
            sync = false;
        });
    }

    async.ensureAsync = ensureAsync;

    async.constant = _restParam(function(values) {
        var args = [null].concat(values);
        return function (callback) {
            return callback.apply(this, args);
        };
    });

    async.wrapSync =
    async.asyncify = function asyncify(func) {
        return _restParam(function (args) {
            var callback = args.pop();
            var result;
            try {
                result = func.apply(this, args);
            } catch (e) {
                return callback(e);
            }
            // if result is Promise object
            if (_isObject(result) && typeof result.then === "function") {
                result.then(function(value) {
                    callback(null, value);
                })["catch"](function(err) {
                    callback(err.message ? err : new Error(err));
                });
            } else {
                callback(null, result);
            }
        });
    };

    // Node.js
    if (typeof module === 'object' && module.exports) {
        module.exports = async;
    }
    // AMD / RequireJS
    else if (typeof define === 'function' && define.amd) {
        define([], function () {
            return async;
        });
    }
    // included directly via <script> tag
    else {
        root.async = async;
    }

}());

}).call(this,require('_process'),typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})
},{"_process":1}],6:[function(require,module,exports){
/*global angular */
'use strict';

var template = '' +
'<table st-table="distributionList" st-safe-src="$ctrl.distributionList" class="table table-striped">' +
'    <thead>' +
'    <tr>' +
'        <th st-sort="distribution" st-sort-default="reverse" translate>DISTRIBUTION_MANAGEMENT.DATE</th>' +
'        <th translate>DISTRIBUTION_MANAGEMENT.CATEGORY</th>' +
'        <th translate>DISTRIBUTION_MANAGEMENT.VERSION</th>' +
'        <th translate>DISTRIBUTION_MANAGEMENT.ACTIONS</th>' +
'    </tr>' +
'    </thead>' +
'    <tbody>' +
'    <tr ng-repeat="item in distributionList"' +
'        ng-class="{info: item.active}"' +
'    >' +
'        <td>{{item.uploadTimestamp | date : \'short\'}}</td>' +
'        <td>{{item.category}}</td>' +
'        <td>' +
'            <span editable-text="item.version" e-name="version" e-form="updateForm"' +
'                  onbeforesave="$ctrl.onCheckVersion({version: $data})" e-required>' +
'                {{ item.version || \'empty\' }}' +
'            </span>' +
'        </td>' +
'        <td style="white-space: nowrap">' +
'            <!-- update node form -->' +
'            <form editable-form name="updateForm" onbeforesave="$ctrl.updateDistribution($data, item)"' +
'                  ng-show="updateForm.$visible" class="form-buttons form-inline">' +
'                <button type="submit" ng-disabled="updateForm.$waiting"' +
'                        class="btn btn-primary" translate>' +
'                    GENERAL.SAVE' +
'                </button>' +
'                <button type="button" ng-disabled="updateForm.$waiting"' +
'                        ng-click="updateForm.$cancel()" class="btn btn-default" translate>' +
'                    GENERAL.CANCEL' +
'                </button>' +
'            </form>' +
'            <div class="buttons" ng-show="!updateForm.$visible">' +
'                <button type="button"' +
'                        class="btn btn-default btn-xs"' +
'                        ng-click="updateForm.$show()"' +
'                        translate>GENERAL.EDIT</button>' +
'                <button type="button"' +
'                        class="btn btn-primary btn-xs"' +
'                        ng-click="$ctrl.onDownload({distributionInfo: item})"' +
'                        translate>GENERAL.DOWNLOAD</button>' +
'                <button type="button"' +
'                        class="btn btn-danger btn-xs"' +
'                        ng-click="$ctrl.onRemove({distributionInfo: item})"' +
'                        translate>GENERAL.REMOVE</button>' +
'            </div>' +
'        </td>' +
'    </tr>' +
'    </tbody>' +
'    <tfoot>' +
'    <tr>' +
'        <td colspan="5" class="text-center">' +
'            <div st-pagination="" st-items-by-page="10" st-displayed-pages="7"></div>' +
'        </td>' +
'    </tr>' +
'    </tfoot>' +
'</table>' +
'';


angular.
    module('bitcraft-distribution.list').
    component('bitcraftDistributionList', {
        template: template,
        controller: function DistributionListController() {
            var self = this;

            self.$onChanges = function (changesObj) {
                if (changesObj.distributionList) {
                    self.distributionList = angular.copy(changesObj.distributionList.currentValue);
                }
            };

            /**
             * @param {{version: string}} data
             * @param {DistributionInfo} distribution
             */
            self.updateDistribution = function (data, distribution) {
                self.onUpdateDistribution({
                    distributionId: distribution._id,
                    version: data.version
                });
            };

        },
        bindings: {
            distributionList: '<',
            onRemove: '&',
            onDownload: '&',
            onCheckVersion: '&',
            onUpdateDistribution: '&'
        }
    });

},{}],7:[function(require,module,exports){
/*global angular*/
'use strict';

angular.module('bitcraft-distribution.list', []);

},{}],8:[function(require,module,exports){
'use strict';
require('./distribution-list.module');
require('./distribution-list.component');

},{"./distribution-list.component":6,"./distribution-list.module":7}],9:[function(require,module,exports){
/*global angular*/
'use strict';

var template = '' +
'<h3 translate>DISTRIBUTION_MANAGEMENT.SENTINEL_TITLE</h3>' +
'<div class="row" style="margin-top: 30px">' +
'    <div class="col-md-12">' +
'        <files-upload' +
'                format-name="sentinel archive"' +
'                on-upload="$ctrl.onUploadSentinel(data)"' +
'                allow-cancel="false"' +
'        ></files-upload>' +
'    </div>' +
'</div>' +
'<h3 translate>DISTRIBUTION_MANAGEMENT.DISTRIBUTION_TITLE</h3>' +
'<div class="row">' +
'    <div class="col-md-5">' +
'        <div class="row" style="margin-top: 30px">' +
'            <div class="col-md-3">' +
'                <label class="control-label" for="host-category" translate>DISTRIBUTION_MANAGEMENT.HOST_CATEGORY</label>' +
'            </div>' +
'            <div class="col-md-4">' +
'                <select' +
'                        ng-model="$ctrl.hostCategory"' +
'                        class="form-control"' +
'                        id="host-category"' +
'                >' +
'                    <option value="http" selected>Http</option>' +
'                    <option value="agent">Agent</option>' +
'                    <option value="router">Router</option>' +
'                </select>' +
'            </div>' +
'        </div>' +
'        <div class="row" style="margin-top: 30px">' +
'            <div class="col-md-3">' +
'                <label class="control-label" for="upload-version"' +
'                       translate>DISTRIBUTION_MANAGEMENT.VERSION</label>' +
'            </div>' +
'            <div class="col-md-4">' +
'                <input type="text" class="form-control" id="upload-version"' +
'                       ng-model="$ctrl.version" required/>' +
'            </div>' +
'        </div>' +
'        <div class="row" style="margin-top: 30px">' +
'            <div class="col-md-12">' +
'                <files-upload' +
'                        format-name="archive"' +
'                        on-upload="$ctrl.onUpload(data)"' +
'                        allow-cancel="false"' +
'                ></files-upload>' +
'            </div>' +
'        </div>' +
'    </div>' +
'    <div class="col-md-5">' +
'        <div class="row" style="margin-top:10px; margin-bottom:10px;">' +
'            <div class="col-md-12">' +
'                <h4 translate>DISTRIBUTION_MANAGEMENT.DISTRIBUTION_LIST</h4>' +
'            </div>' +
'        </div>' +
'        <bitcraft-distribution-list distribution-list="$ctrl.distributions"' +
'                           on-remove="$ctrl.onRemoveDistribution(distributionInfo)"' +
'                           on-download="$ctrl.onDownloadDistribution(distributionInfo)"' +
'                           on-check-version="$ctrl.checkVersion(version)"' +
'                           on-update-distribution="$ctrl.onUpdateDistribution(distributionId, version)"' +
'        ></bitcraft-distribution-list>' +
'    </div>' +
'</div>' +'';


//noinspection JSUnusedGlobalSymbols
angular.module('bitcraft-distribution-management').
    component('bitcraftDistributionManagement', {
        template: template,
        controller: [
            '$log', 'dialogs', 'Notification', 'DeploymentManagement',
            function distributionManagementController(
                $log,
                dialogs,
                Notification,
                DeploymentManagement
            ) {
                var self = this;

                self.hostCategory = 'http';
                self.version = '';

                var notifyCallback = function (evt) {
                    var progressPercentage = parseInt(100.0 * evt.loaded / evt.total, 10);

                    // we only display progression feedback when the upload takes time,
                    // so we discard the 100% notification for instantaneous upload
                    if (progressPercentage !== 100) {
                        $log.info('PROGRESS: ' + progressPercentage + '% ');
                    }
                };

                function refreshDistributions() {
                    DeploymentManagement.listDistributions().then(function (data) {
                        self.distributions = data;
                    }, function (resp) {
                        Notification.error({
                            message: 'Failed to retrieve distributions (' + resp.statusText + ')'
                        });
                    });
                }

                /** @param {DistributionInfo} distributionInfo */
                self.onRemoveDistribution = function (distributionInfo) {
                    var dlg = dialogs.confirm(
                        'Confirmation required',
                        'Do you want to remove the distribution?',
                        {size: 'sm'}
                    );
                    dlg.result.then(function (btn) {
                        DeploymentManagement.removeDistribution(
                            distributionInfo
                        ).then(function (data) {
                            refreshDistributions();
                            Notification.success({
                                message: 'Distribution successfully removed'
                            });
                        }, function (resp) {
                            Notification.error({
                                message: 'Failed to remove distribution (' + resp.statusText + ')'
                            });
                        });
                    });
                };

                /**
                 * @param {string} distributionId
                 * @param {string} version
                 */
                self.onUpdateDistribution = function (distributionId, version) {
                    DeploymentManagement.updateDistribution(
                        distributionId,
                        version
                    ).then(function (data) {
                        Notification.success({
                            message: 'Distribution successfully updated'
                        });
                    }, function (resp) {
                        Notification.error({
                            message: 'Failed to update distribution (' + resp.statusText + ')'
                        });
                    });
                };

                /**
                 * @param {Array.<File>} filesData
                 */
                self.onUpload = function (filesData) {
                    if (filesData && filesData.length > 0) {
                        DeploymentManagement.uploadDistribution(
                            self.hostCategory,
                            self.version,
                            filesData[0]
                        ).then(function () {
                            $log.info('Upload success');
                            refreshDistributions();
                            Notification.success({message: 'Upload succeeded'});
                            self.version = '';
                        }, function (resp) {
                            $log.error('Upload failed: ' + resp.data, resp.statusText);
                            Notification.error(
                                {message: 'Upload failed (' + resp.statusText + ': ' + resp.data + ')'}
                            );
                        }, notifyCallback);
                    }
                };

                /**
                 * @param {Array.<File>} filesData
                 */
                self.onUploadSentinel = function (filesData) {
                    if (filesData && filesData.length > 0) {
                        DeploymentManagement.uploadSentinel(
                            filesData[0]
                        ).then(function () {
                            $log.info('Upload success');
                            Notification.success({message: 'Upload succeeded'});
                        }, function (resp) {
                            $log.error('Upload failed: ' + resp.data, resp.statusText);
                            Notification.error(
                                {message: 'Upload failed (' + resp.statusText + ': ' + resp.data + ')'}
                            );
                        }, notifyCallback);
                    }
                };

                /**
                 * Checks if the distributions version is valid. Returns a message in case of error,
                 * otherwise returns undefined.
                 * @param {string|undefined} version
                 * @returns {string|undefined}
                 */
                self.checkVersion = function (version) {
                    if (typeof version !== 'string' || version.length === 0) {
                        return 'Distribution version cannot be empty';
                    }
                };

                self.onDownloadDistribution = DeploymentManagement.downloadDistribution;

                refreshDistributions();
            }
        ]
    });

},{}],10:[function(require,module,exports){
/*global angular*/
'use strict';

angular.module('bitcraft-distribution-management', ['bitcraft-distribution.list']);

},{}],11:[function(require,module,exports){
'use strict';
require('./distribution-management.module');
require('./distribution-management.component');
require('./distribution-list');

},{"./distribution-list":8,"./distribution-management.component":9,"./distribution-management.module":10}],12:[function(require,module,exports){
/*global angular*/
'use strict';

var template = '' +
'<div class="row" style="margin-top: 30px">' +
'    <div class="col-md-1">' +
'        <label class="control-label" for="host-category" translate>' +
'            HOST_MANAGEMENT.HOST_CATEGORY' +
'        </label>' +
'    </div>' +
'    <div class="col-md-2">' +
'        <select' +
'                ng-model="$ctrl.hostCategory"' +
'                class="form-control"' +
'                id="host-category"' +
'                ng-change="$ctrl.updateDnsPrefill()"' +
'        >' +
'            <option value="http" selected>Http</option>' +
'            <option value="agent">Agent</option>' +
'            <option value="router">Router</option>' +
'        </select>' +
'    </div>' +
'</div>' +
'<div class="row" style="margin-top: 30px">' +
'    <div class="col-md-1">' +
'        <label class="control-label" for="local-address" translate title="{{\'HOST_MANAGEMENT.TOOLTIP_LOCAL_ADDRESS\'|translate}}">' +
'            HOST_MANAGEMENT.LOCAL_ADDRESS' +
'        </label>' +
'    </div>' +
'    <div class="col-md-2">' +
'        <input' +
'                ng-model="$ctrl.localAddress"' +
'                type="text"' +
'                class="form-control"' +
'                id="local-address"' +
'        />' +
'    </div>' +
'</div>' +
'<div class="row" style="margin-top: 30px" ng-show="$ctrl.hostCategory!==\'http\'">' +
'    <div class="col-md-1">' +
'        <label class="control-label" for="dns" translate ng-show="$ctrl.hostCategory!==\'agent\'" title="{{\'HOST_MANAGEMENT.TOOLTIP_REMOTE_ADDRESS\'|translate}}">' +
'            HOST_MANAGEMENT.DNS_NAME</label>' +
'        <label class="control-label" for="dns" translate ng-show="$ctrl.hostCategory===\'agent\'" title="{{\'HOST_MANAGEMENT.TOOLTIP_REMOTE_ADDRESS\'|translate}}">' +
'            HOST_MANAGEMENT.REMOTE_ADDRESS_NAME</label>' +
'    </div>' +
'    <div class="col-md-2">' +
'        <input' +
'                ng-model="$ctrl.dns"' +
'                type="text"' +
'                class="form-control"' +
'                id="dns"' +
'        />' +
'    </div>' +
'</div>' +
'<div class="row" style="margin-top: 30px">' +
'    <div class="col-md-1">' +
'        <label class="control-label" for="sentinel-port" translate>' +
'            HOST_MANAGEMENT.SENTINEL_PORT' +
'        </label>' +
'    </div>' +
'    <div class="col-md-2">' +
'        <input' +
'                ng-model="$ctrl.sentinelPort"' +
'                type="text"' +
'                class="form-control"' +
'                id="sentinel-port"' +
'        />' +
'    </div>' +
'</div>' +
'<div class="row" style="margin-top: 30px">' +
'    <div class="col-md-2">' +
'        <div class="form-group">' +
'            <label><span translate>HOST_MANAGEMENT.UPLOAD_SENTINEL</span>' +
'                <input type="checkbox" ng-model="$ctrl.uploadNewSentinel" />' +
'            </label>' +
'        </div>' +
'    </div>' +
'    <div class="col-md-1">' +
'        <a ng-if="!$ctrl.uploadNewSentinel" class="btn btn-primary" ng-click="$ctrl.installHost()" translate>' +
'            HOST_MANAGEMENT.INSTALL' +
'        </a>' +
'        <a ng-if="$ctrl.uploadNewSentinel" class="btn btn-primary" ng-click="$ctrl.installHost()" translate>' +
'            HOST_MANAGEMENT.UPLOAD_SENTINEL_AND_INSTALL' +
'        </a>' +
'        <span class="btn btn-default"' +
'              ng-show="false"' +
'              id="sentinel-file-select"' +
'              ngf-select="$ctrl.sentinelFileSelected($file)"' +
'              ngf-pattern=".tar.gz"' +
'              accept=".tar.gz"' +
'              ngf-multiple="false"/>' +
'    </div>' +
'</div>' +
'';


//noinspection JSUnusedGlobalSymbols
angular.module('bitcraft-host-management').
    component('bitcraftHostManagement', {
        template: template,
        controller: [
            '$log', 'dialogs', 'Notification', 'DeploymentManagement', 'ServerConfig',
            function hostManagementController(
                $log,
                dialogs,
                Notification,
                DeploymentManagement,
                ServerConfig
            ) {
                var self = this;

                /** @type {SystemConfig} */
                self.config = {};
                self.hostCategory = 'http';
                self.localAddress = '';
                self.sentinelPort = '';
                self.dns = '';
                self.uploadNewSentinel = false;
                self.sentinelFile = null;

                self.installHost = function () {
                    if (self.uploadNewSentinel) {
                        angular.element('#sentinel-file-select').trigger('click');
                    } else {
                        self.addHost();
                    }
                };

                /**
                 * @param {File} file
                 */
                self.sentinelFileSelected = function (file) {
                    if (file) {
                        self.addHost(file);
                    }
                };

                self.updateDnsPrefill = function () {
                    if (self.hostCategory === 'http') {
                        self.dns = self.config.hostManagement.httpDNS;
                    } else if (self.hostCategory === 'agent') {
                        self.dns = self.config.hostManagement.agentDNS;
                    } else if (self.hostCategory === 'router') {
                        self.dns = self.config.hostManagement.routerDNS;
                    }
                };

                ServerConfig.getCurrentConfig().then(function (data) {
                    self.config = data;
                    self.updateDnsPrefill();
                    self.sentinelPort = self.config.hostManagement.sentinelPort
                }, function (resp) {
                    $log.debug('failed to retrieve config from server. (' + resp.data + ')');
                });

                /**
                 * @param {File} [sentinelFile]
                 */
                self.addHost = function (sentinelFile) {
                    /** @type {HostInfo} */
                    var newHost = {
                        _id: undefined,
                        category: self.hostCategory,
                        localAddress: self.localAddress,
                        dns: self.dns,
                        sentinelPort: parseInt(self.sentinelPort, 10)
                    };

                    Notification.info({
                        message: 'Deploying sentinel...'
                    });
                    DeploymentManagement.installHost(newHost, sentinelFile).then(function () {
                        Notification.success({
                            message: 'Successfully added new host with address "' + self.localAddress + '"'
                        });

                        // reset the text boxes
                        self.localAddress = '';
                        self.updateDnsPrefill();
                        self.sentinelPort = self.config.hostManagement.sentinelPort;
                    }, function (resp) {
                        Notification.error({
                            message: 'Failed to add new host with address ' +
                            self.localAddress + ' (' + resp.statusText + ': ' + resp.data + ')'
                        });
                    });
                };

            }
        ]
    });

/**
 * @typedef {Object} HostInfo
 * @property {ObjectID|string} _id
 * @property {string} category
 * @property {string} localAddress
 * @property {string} dns
 * @property {number} sentinelPort
 */

/**
 * @typedef {Object} HostManagementConfig
 * @property {string} httpDNS
 * @property {string} agentDNS
 * @property {string} routerDNS
 * @property {number} sentinelPort
 */

/**
 * @typedef {Object} NodeManagementConfig
 * @property {number} tcpPort
 * @property {number} udpPort
 * @property {string} adminHost
 * @property {number} adminPort
 * @property {JSON} customData
 */

/**
 * @typedef {Object} SystemConfig
 * @property {HostManagementConfig} hostManagement
 * @property {NodeManagementConfig} nodeManagement
 */

},{}],13:[function(require,module,exports){
/*global angular*/
'use strict';

angular.module('bitcraft-host-management', []);

},{}],14:[function(require,module,exports){
'use strict';
require('./host-management.module');
require('./host-management.component');

},{"./host-management.component":12,"./host-management.module":13}],15:[function(require,module,exports){
'use strict';
require('./services');
require('./distribution-management');
require('./host-management');
require('./node-management');

},{"./distribution-management":11,"./host-management":14,"./node-management":25,"./services":33}],16:[function(require,module,exports){
/*global angular*/
'use strict';

var util = require('util')

angular.
    module('bitcraft-distribution.filter', []).
    filter('bitcraftDistributionFilter', function () {
        return function (input, param) {
            /** @type {Array.<DistributionInfo>} */
            var distributionList = input;
            /** @type {string} */
            var filterCategory = param;
            /** @type {Array.<DistributionInfo>} */
            var result = [];

            angular.forEach(distributionList, function (host) {
                if (host.category === filterCategory) {
                    result.push(host);
                }
            });

            return result;
        };
    });
},{"util":4}],17:[function(require,module,exports){
/*global angular*/
'use strict';

angular.module('bitcraft-distribution.filter', []);

},{}],18:[function(require,module,exports){
'use strict';
require('./distribution-filter.module');
require('./distribution-filter.filter');

},{"./distribution-filter.filter":16,"./distribution-filter.module":17}],19:[function(require,module,exports){
/*global angular*/
'use strict';

angular.
    module('bitcraft-host.filter', []).
    filter('bitcraftHostFilter', function () {
        return function (input, param) {
            /** @type {Array.<HostInfo>} */
            var hostList = input;
            /** @type {string} */
            var filterCategory = param;
            /** @type {Array.<HostInfo>} */
            var result = [];

            angular.forEach(hostList, function (host) {
                if (host.category === filterCategory) {
                    result.push(host);
                }
            });

            return result;
        };
    });
},{}],20:[function(require,module,exports){
/*global angular*/
'use strict';

angular.module('bitcraft-host.filter', []);

},{}],21:[function(require,module,exports){
'use strict';
require('./host-filter.module');
require('./host-filter.filter');

},{"./host-filter.filter":19,"./host-filter.module":20}],22:[function(require,module,exports){
/*global angular */
'use strict';

var template = '' +
'<style>' +
'    .HOST_RUNNING {' +
'        color: #5cb85c;' +
'    }' +
'    .HOST_DOWN {' +
'        color: #c9302c;' +
'    }' +
'    .HOST_NOT_INSTALLED {' +
'        color: #c9302c;' +
'    }' +
'</style>' +
'<table st-table="hostList" st-safe-src="$ctrl.hostList" class="table table-striped">' +
'    <thead>' +
'    <!--router headers-->' +
'    <tr ng-if="$ctrl.hostCategory===\'router\'">' +
'        <th st-sort="host" st-sort-default="reverse" translate>NODE_MANAGEMENT.STATUS</th>' +
'        <th translate>NODE_MANAGEMENT.LOCAL_ADDRESS</th>' +
'        <th translate>NODE_MANAGEMENT.DNS</th>' +
'        <th translate>HOST_MANAGEMENT.SENTINEL_PORT</th>' +
'        <th translate>NODE_MANAGEMENT.ACTIONS</th>' +
'    </tr>' +
'    <!--http headers-->' +
'    <tr ng-if="$ctrl.hostCategory===\'http\'">' +
'        <th st-sort="host" st-sort-default="reverse" translate>NODE_MANAGEMENT.STATUS</th>' +
'        <th translate>NODE_MANAGEMENT.LOCAL_ADDRESS</th>' +
'        <th translate>HOST_MANAGEMENT.SENTINEL_PORT</th>' +
'        <th translate>NODE_MANAGEMENT.ACTIONS</th>' +
'    </tr>' +
'    <!--agent headers-->' +
'    <tr ng-if="$ctrl.hostCategory===\'agent\'">' +
'        <th st-sort="host" st-sort-default="reverse" translate>NODE_MANAGEMENT.LOCAL_ADDRESS</th>' +
'        <th translate>NODE_MANAGEMENT.REMOTE_ADDRESS_NAME</th>' +
'        <th translate>HOST_MANAGEMENT.SENTINEL_PORT</th>' +
'        <th translate>NODE_MANAGEMENT.NODE_COUNT</th>' +
'        <th translate>NODE_MANAGEMENT.ACTIONS</th>' +
'    </tr>' +
'    </thead>' +
'    <tbody>' +
'    <tr ng-repeat="item in hostList | bitcraftHostFilter:$ctrl.hostCategory"' +
'        ng-class="{info: item.active}"' +
'        ng-click="$ctrl.onHostClicked({hostInfo: item})"' +
'    >' +
'        <td ng-show="$ctrl.hostCategory!==\'agent\'">' +
'            <span class="{{item.state.replace(\'.\', \'_\')}}" translate>{{item.state}}</span>' +
'        </td>' +
'        <td>{{item.localAddress}}</td>' +
'        <td ng-if="$ctrl.hostCategory!==\'http\'">{{item.dns}}</td>' +
'        <td>{{item.sentinelPort}}</td>' +
'        <td ng-show="$ctrl.hostCategory===\'agent\'">{{item.nodeCount}}</td>' +
'        <!--TODO' +
'            show machine info: agent count, agent health/errors' +
'        -->' +
'        <td style="white-space: nowrap">' +
'            <!-- install distribution form -->' +
'            <form editable-form name="installForm"' +
'                  onbeforesave="$ctrl.installHttp($ctrl.selectedDistribution, item, $ctrl.hostInfo)"' +
'                  ng-show="installForm.$visible" class="form-buttons form-inline">' +
'                <select' +
'                        ng-model="$ctrl.selectedDistribution"' +
'                        class="form-control"' +
'                        id="distribution-list"' +
'                        ng-options="dist as (dist.name + \' - \' + dist.version)' +
'                            for dist in $ctrl.distributionList' +
'                            track by dist._id"' +
'                >' +
'                </select>' +
'                <button type="submit" ng-disabled="installForm.$waiting"' +
'                        class="btn btn-primary" translate>' +
'                    DIALOGS_OK' +
'                </button>' +
'                <button type="button" ng-disabled="installForm.$waiting"' +
'                        ng-click="installForm.$cancel()" class="btn btn-default" translate>' +
'                    GENERAL.CANCEL' +
'                </button>' +
'            </form>' +
'            <div class="buttons" ng-show="!installForm.$visible">' +
'                <button ng-show="$ctrl.hostCategory!==\'agent\'"' +
'                        type="button"' +
'                        class="btn btn-primary btn-xs"' +
'                        ng-click="installForm.$show(); $event.stopPropagation();"' +
'                        ng-disabled="!$ctrl.canInstall(item) || $ctrl.isOperationInProgress"' +
'                        translate>NODE_MANAGEMENT.INSTALL</button>' +
'                <button ng-show="$ctrl.hostCategory!==\'agent\'"' +
'                        class="btn btn-success btn-xs"' +
'                        ng-disabled="!$ctrl.canStart(item) || $ctrl.isOperationInProgress"' +
'                        ng-click="$ctrl.startHttp(item); $event.stopPropagation();"' +
'                        translate>GENERAL.START</button>' +
'                <button ng-show="$ctrl.hostCategory!==\'agent\'"' +
'                        class="btn btn-danger btn-xs"' +
'                        ng-disabled="!$ctrl.canStop(item) || $ctrl.isOperationInProgress"' +
'                        ng-click="$ctrl.stopHttp(item); $event.stopPropagation();"' +
'                        translate>GENERAL.STOP</button>' +
'                <button type="button"' +
'                        class="btn btn-default btn-xs"' +
'                        ng-disabled="$ctrl.isOperationInProgress"' +
'                        ng-click="$ctrl.onUninstallHost({hostInfo: item}); $event.stopPropagation();"' +
'                        translate>NODE_MANAGEMENT.UNINSTALL</button>' +
'                <button type="button"' +
'                        class="btn btn-primary btn-xs"' +
'                        ng-disabled="$ctrl.isOperationInProgress"' +
'                        ng-click="$ctrl.reinstallSentinel(item); $event.stopPropagation();"' +
'                        translate>NODE_MANAGEMENT.REINSTALL_SENTINEL</button>' +
'            </div>' +
'        </td>' +
'    </tr>' +
'    </tbody>' +
'    <tfoot>' +
'    <tr>' +
'        <td colspan="5" class="text-center">' +
'            <div st-pagination="" st-items-by-page="10" st-displayed-pages="7"></div>' +
'        </td>' +
'    </tr>' +
'    </tfoot>' +
'</table>' +
'';


// 3rd party
var async = require('async');

/** @enum {string} */
var STATES = {
    NOT_INSTALLED: 'HOST.NOT_INSTALLED',
    RUNNING: 'HOST.RUNNING',
    DOWN: 'HOST.DOWN',
    PENDING: 'HOST.PENDING'
};

angular.module('bitcraft-host.list').
    component('bitcraftHostList', {
        template: template,
        controller: [
            'Notification',
            function HostListController(Notification) {
                var i;
                var self = this;

                self.selectedDistribution = {};
                self.distributionList = [];

                function init() {
                    async.forEach(self.hostList,
                        function (item, next) {
                            self.checkHttpInstall(item, function (err) {
                                self.checkHttpState(item);
                                self.updateNodeCount(item);
                                next(err);
                            });
                        },
                        /** @param {string} err */
                        function (err) {
                            if (err) {
                                Notification.error({
                                    message: 'Failed to initialize hosts (' + err + ')'
                                });
                            }
                        });
                }

                /**
                 * @param {string} id
                 * @returns {HostInfo|undefined}
                 */
                function getHostById(id) {
                    for (i = 0; i < self.hostList.length; i += 1) {
                        if (self.hostList[i]._id === id) {
                            return self.hostList[i];
                        }
                    }
                }

                self.$onChanges = function (changesObj) {
                    if (changesObj.hostList) {
                        self.hostList = angular.copy(changesObj.hostList.currentValue);
                        if (!changesObj.hostList.isFirstChange() || self.hostList !== undefined) {
                            init();
                        }
                    }
                    if (changesObj.distributionList) {
                        self.distributionList = angular.copy(
                            changesObj.distributionList.currentValue
                        );
                        if (self.distributionList && self.distributionList.length > 0) {
                            self.selectedDistribution = self.distributionList[0];
                        }
                    }
                };

                /**
                 * @param {HostInfo} item
                 * @param {function(string=)} callback
                 */
                self.checkHttpInstall = function (item, callback) {
                    if (item.category === 'http') {
                        item.httpInstalled = false;
                        item.state = STATES.NOT_INSTALLED;
                        self.onCheckHttpInstall({hostInfo: item}).then(
                            function (data) {
                                item.httpInstalled = data.installed;
                                callback();
                            },
                            function (resp) {
                                Notification.error({
                                    message: 'Failed to check http state (' +
                                        resp.statusText + ': ' + resp.data + ')'
                                });
                                callback(resp.data);
                            }
                        );
                    }
                };

                /** @param {HostInfo} item */
                self.checkHttpState = function (item) {
                    if (item.httpInstalled) {
                        item.state = STATES.PENDING;
                        self.onCheckHttpState({hostInfo: item}).then(
                            function (data) {
                                item.state = data.running ? STATES.RUNNING : STATES.DOWN;
                            },
                            function (resp) {
                                Notification.error({
                                    message: 'Failed to check state (' +
                                        resp.statusText + ': ' + resp.data + ')'
                                });
                            }
                        );
                    }
                };

                /** @param {HostInfo} item */
                self.updateNodeCount = function (item) {
                    if (item.category === 'agent') {
                        self.onGetNodeCount({hostInfo: item}).then(
                            function (data) {
                                item.nodeCount = data.nodeCount;
                            },
                            function (resp) {
                                Notification.error({
                                    message: 'Failed to update node count (' +
                                        resp.statusText + ': ' + resp.data + ')'
                                });
                            }
                        );
                    }
                };

                /**
                 * @param {DistributionInfo} data
                 * @param {HostInfo} host
                 */
                self.installHttp = function (data, host) {
                    self.isOperationInProgress = true;
                    self.onInstallHttp({
                        distributionId: data._id,
                        hostId: host._id,
                        callback: function () {
                            self.isOperationInProgress = false;
                            self.checkHttpInstall(host, function () {
                                host.state = STATES.DOWN;
                            });
                        }
                    });
                };

                /**
                 * Returns true if the host can be installed
                 * @param {HostInfo} data
                 * @returns {boolean}
                 */
                self.canInstall = function (data) {
                    return true;
                };

                /**
                 * Returns true if the host can be started
                 * @param {HostInfo} data
                 * @returns {boolean}
                 */
                self.canStart = function (data) {
                    var host = getHostById(data._id);
                    if (!host) {
                        return false;
                    }

                    return host.httpInstalled && host.state === STATES.DOWN;
                };

                /**
                 * Returns true if the host can be stopped
                 * @param {HostInfo} data
                 * @returns {boolean}
                 */
                self.canStop = function (data) {
                    var host = getHostById(data._id);
                    if (!host) {
                        return false;
                    }

                    return host.httpInstalled && host.state === STATES.RUNNING;
                };

                /** @param {HostInfo} data */
                self.reinstallSentinel = function (host) {
                    self.isOperationInProgress = true;
                    self.onReinstallSentinel({
                        hostInfo: host,
                        callback: function () {
                            self.isOperationInProgress = false;
                        }
                    });
                };

                /** @param {HostInfo} hostInfo */
                self.startHttp = function (hostInfo) {
                    self.onStartHttp({
                        hostInfo: hostInfo,
                        callback: function () {
                            self.checkHttpState(hostInfo);
                        }
                    });
                };

                /** @param {HostInfo} hostInfo */
                self.stopHttp = function (hostInfo) {
                    self.onStopHttp({
                        hostInfo: hostInfo,
                        callback: function () {
                            self.checkHttpState(hostInfo);
                        }
                    });
                };

            }],
        bindings: {
            hostList: '<',
            distributionList: '<',
            hostCategory: '<',
            isOperationInProgress: '=',
            onAddNode: '&',
            onHostClicked: '&',
            onCheckHttpState: '&',
            onStartHttp: '&',
            onStopHttp: '&',
            onGetNodeCount: '&',
            onUninstallHost: '&',
            onReinstallSentinel: '&',
            onInstallHttp: '&',
            onCheckHttpInstall: '&'
        }
    });

},{"async":5}],23:[function(require,module,exports){
/*global angular*/
'use strict';

angular.module('bitcraft-host.list', ['bitcraft-host.filter']);

},{}],24:[function(require,module,exports){
'use strict';
require('./host-list.module');
require('./host-list.component');

},{"./host-list.component":22,"./host-list.module":23}],25:[function(require,module,exports){
'use strict';
require('./node-management.module');
require('./node-management.component');
require('./distribution-filter');
require('./host-list');
require('./host-filter');
require('./node-list');

},{"./distribution-filter":18,"./host-filter":21,"./host-list":24,"./node-list":26,"./node-management.component":29,"./node-management.module":30}],26:[function(require,module,exports){
'use strict';
require('./node-list.module');
require('./node-list.component');

},{"./node-list.component":27,"./node-list.module":28}],27:[function(require,module,exports){
/*global angular */
'use strict';

var template = '' +
'<style>' +
'    .AGENTS_RUNNING {' +
'        color: #5cb85c;' +
'    }' +
'    .AGENTS_DOWN {' +
'        color: #c9302c;' +
'    }' +
'    .AGENTS_NOT_INSTALLED {' +
'        color: #c9302c;' +
'    }' +
'</style>' +
'<table st-table="nodeList" st-safe-src="$ctrl.nodeList" class="table table-striped">' +
'    <thead>' +
'    <tr>' +
'        <th st-sort="host" st-sort-default="reverse" translate>NODE_MANAGEMENT.STATUS</th>' +
'        <th translate>NODE_MANAGEMENT.NAME</th>' +
'        <th translate>NODE_MANAGEMENT.UUID</th>' +
'        <th translate>NODE_MANAGEMENT.TCP_PORT</th>' +
'        <th translate>NODE_MANAGEMENT.UDP_PORT</th>' +
'        <th translate>NODE_MANAGEMENT.ADMIN_ACCESS_HOST</th>' +
'        <th translate>NODE_MANAGEMENT.ADMIN_ACCESS_PORT</th>' +
'        <th translate>NODE_MANAGEMENT.CUSTOM_DATA</th>' +
'        <th translate>NODE_MANAGEMENT.BOOT_TIME</th>' +
'        <th translate>NODE_MANAGEMENT.DISTRIBUTION_VERSION</th>' +
'        <th translate>NODE_MANAGEMENT.DISTRIBUTION_DATE</th>' +
'        <th translate>NODE_MANAGEMENT.INSTALL_DATE</th>' +
'        <th translate>NODE_MANAGEMENT.ACTIONS</th>' +
'    </tr>' +
'    </thead>' +
'    <tbody>' +
'    <tr ng-repeat="item in nodeList" ng-class="{info: item.active}">' +
'        <td>' +
'            <span class="{{item.state.replace(\'.\', \'_\')}}" translate>{{item.state}}</span>' +
'        </td>' +
'        <td>' +
'            <span editable-text="item.name" e-name="name" e-form="updateForm"' +
'                  onbeforesave="$ctrl.onCheckName({name: $data})" e-required>' +
'                {{ item.name || \'empty\' }}' +
'            </span>' +
'        </td>' +
'        <td>{{item.uuid}}</td>' +
'        <td>' +
'            <span editable-text="item.tcpPort" e-name="tcpPort" e-form="updateForm">' +
'                {{ item.tcpPort || \'empty\' }}' +
'            </span>' +
'        </td>' +
'        <td>' +
'            <span editable-text="item.udpPort" e-name="udpPort" e-form="updateForm">' +
'                {{ item.udpPort || \'empty\' }}' +
'            </span>' +
'        </td>' +
'        <td>' +
'            <span editable-text="item.adminAccess.host" e-name="adminHost" e-form="updateForm">' +
'                {{ item.adminAccess.host || \'empty\' }}' +
'            </span>' +
'        </td>' +
'        <td>' +
'            <span editable-text="item.adminAccess.port" e-name="adminPort" e-form="updateForm">' +
'                {{ item.adminAccess.port || \'empty\' }}' +
'            </span>' +
'        </td>' +
'        <td>' +
'            <div ui-ace="$ctrl.aceSettings"' +
'                 id="input-customData"' +
'                 ng-model="item.customDataAsString"' +
'                 e-name="customData"' +
'                 e-form="updateForm"' +
'                 editable-textarea="item.customDataAsString"' +
'                 readonly="true"' +
'                 style="height: 150px; width: 200px; background: whitesmoke;"' +
'            />' +
'        </td>' +
'        <td>{{item.bootTime | date : \'short\'}}</td>' +
'        <td>{{item.distributionVersion}}</td>' +
'        <td>{{item.distributionTimestamp | date : \'short\'}}</td>' +
'        <td>{{item.installTimestamp | date : \'short\'}}</td>' +
'        <td style="white-space: nowrap">' +
'            <!-- update node form -->' +
'            <form editable-form name="updateForm" onbeforesave="$ctrl.updateNode($data, item)"' +
'                  ng-show="updateForm.$visible" class="form-buttons form-inline">' +
'                <button type="submit" ng-disabled="updateForm.$waiting"' +
'                        class="btn btn-primary" translate>' +
'                    GENERAL.SAVE' +
'                </button>' +
'                <button type="button" ng-disabled="updateForm.$waiting"' +
'                        ng-click="updateForm.$cancel()" class="btn btn-default" translate>' +
'                    GENERAL.CANCEL' +
'                </button>' +
'            </form>' +
'            <!-- install distribution form -->' +
'            <form editable-form name="installForm"' +
'                  onbeforesave="$ctrl.installAgent($ctrl.selectedDistribution, item)"' +
'                  ng-show="installForm.$visible" class="form-buttons form-inline">' +
'                <select' +
'                        ng-model="$ctrl.selectedDistribution"' +
'                        class="form-control"' +
'                        id="distribution-list"' +
'                        ng-options="dist as (dist.name + \' - \' + dist.version)' +
'                            for dist in $ctrl.distributionList' +
'                            track by dist._id"' +
'                >' +
'                </select>' +
'                <button type="submit" ng-disabled="installForm.$waiting"' +
'                        class="btn btn-primary" translate>' +
'                    DIALOGS_OK' +
'                </button>' +
'                <button type="button" ng-disabled="installForm.$waiting"' +
'                        ng-click="installForm.$cancel()" class="btn btn-default" translate>' +
'                    GENERAL.CANCEL' +
'                </button>' +
'            </form>' +
'            <div class="buttons" ng-show="!updateForm.$visible && !installForm.$visible">' +
'                <div class="row" style="margin-bottom: 5px;">' +
'                    <button type="button"' +
'                            style="margin-right: 5px;"' +
'                            class="btn btn-primary btn-xs"' +
'                            ng-click="installForm.$show()"' +
'                            ng-disabled="$ctrl.isOperationInProgress"' +
'                            translate>NODE_MANAGEMENT.INSTALL' +
'                    </button>' +
'                </div>' +
'                <div class="row" style="margin-bottom: 5px;">' +
'                    <button type="button"' +
'                            style="margin-right: 5px;"' +
'                            class="btn btn-success btn-xs"' +
'                            ng-click="$ctrl.startNode(item)"' +
'                            ng-disabled="!$ctrl.canStart(item) || $ctrl.isOperationInProgress"' +
'                            translate>GENERAL.START' +
'                    </button>' +
'                    <button type="button"' +
'                            class="btn btn-danger btn-xs"' +
'                            ng-click="$ctrl.stopNode(item)"' +
'                            ng-disabled="!$ctrl.canStop(item) || $ctrl.isOperationInProgress"' +
'                            translate>GENERAL.STOP' +
'                    </button>' +
'                </div>' +
'                <div class="row">' +
'                    <button type="button"' +
'                            style="margin-right: 5px;"' +
'                            class="btn btn-default btn-xs"' +
'                            ng-click="updateForm.$show()"' +
'                            ng-disabled="$ctrl.isOperationInProgress"' +
'                            translate>GENERAL.EDIT' +
'                    </button>' +
'                    <button type="button"' +
'                            class="btn btn-default btn-xs"' +
'                            ng-click="$ctrl.uninstallAgent(item)"' +
'                            ng-disabled="$ctrl.isOperationInProgress"' +
'                            translate>NODE_MANAGEMENT.UNINSTALL' +
'                    </button>' +
'                </div>' +
'            </div>' +
'        </td>' +
'    </tr>' +
'    </tbody>' +
'    <tfoot>' +
'    <tr>' +
'        <td colspan="5" class="text-center">' +
'            <div st-pagination="" st-items-by-page="10" st-displayed-pages="7"></div>' +
'        </td>' +
'    </tr>' +
'    </tfoot>' +
'</table>' +
'';


// 3rd party
var async = require('async');

/** @enum {string} */
var STATES = {
    NOT_INSTALLED: 'AGENTS.NOT_INSTALLED',
    RUNNING: 'AGENTS.RUNNING',
    DOWN: 'AGENTS.DOWN',
    PENDING: 'AGENTS.PENDING'
};

angular.module('bitcraft-node.list').
    component('bitcraftNodeList', {
        template: template,
        controller: [
            'Notification',
            function NodeListController(Notification) {
                var i;
                var self = this;

                self.distributionList = [];
                self.selectedDistribution = {};
                self.aceSettings =
                {
                    mode: 'javascript',
                    showGutter: false
                };

                function init() {
                    async.forEach(self.nodeList,
                        function (item, next) {
                            self.checkAgentInstall(item, function (err) {
                                self.checkState(item);
                                item.customDataAsString = JSON.stringify(item.customData, null, '\t');
                                next(err);
                            });
                        },
                        /** @param {string} err */
                        function (err) {
                            if (err) {
                                Notification.error({
                                    message: 'Failed to initialize agents (' + err + ')'
                                });
                            }
                        });
                }

                function getAgentByUuid(id) {
                    for (i = 0; i < self.nodeList.length; i += 1) {
                        if (self.nodeList[i]._id === id) {
                            return self.nodeList[i];
                        }
                    }
                }

                self.checkState = function (item) {
                    if (item.agentInstalled) {
                        item.state = STATES.PENDING;
                        self.onCheckAgentState({nodeInfo: item}).then(
                            /** @param {{data: {running: boolean}}} */
                            function (resp) {
                                item.state = resp.data.running ? STATES.RUNNING : STATES.DOWN;
                            },
                            /** @param {{data: {error: string}}} */
                            function (resp) {
                                var errorMessage = resp.statusText +
                                    (resp.data && resp.data.error && (': ' + resp.data.error));

                                Notification.error({
                                    message: 'Failed to check state (' + errorMessage + ')'
                                });
                            }
                        );
                    }
                };

                self.$onChanges = function (changesObj) {
                    if (changesObj.nodeList) {
                        self.nodeList = angular.copy(changesObj.nodeList.currentValue);
                        if (self.nodes !== undefined || !changesObj.nodeList.isFirstChange()) {
                            init();
                        }
                    }
                    if (changesObj.distributionList) {
                        self.distributionList = angular.copy(
                            changesObj.distributionList.currentValue
                        );
                        if (self.distributionList && self.distributionList.length > 0) {
                            self.selectedDistribution = self.distributionList[0];
                        }
                    }
                };

                /**
                 * @param {{
                 *      name: string,
                 *      udpPort: string,
                 *      tcpPort: string,
                 *      adminHost: string,
                 *      adminPort: string,
                 *      customData: string}} data
                 * @param {NodeInfo} node
                 */
                self.updateNode = function (data, node) {
                    /** @type {JSON} */
                    var customDataAsJson;

                    try {
                        customDataAsJson = JSON.parse(data.customData);
                    } catch (e) {
                        Notification.error({
                            message: 'Invalid JSON format in custom data (' + e + ')'
                        });
                        return;
                    }

                    console.log('customData: ' + JSON.stringify(customDataAsJson));

                    self.onUpdateNode({
                        nodeId: node._id,
                        name: data.name,
                        udpPort: parseInt(data.udpPort, 10),
                        tcpPort: parseInt(data.tcpPort, 10),
                        adminHost: data.adminHost,
                        adminPort: parseInt(data.adminPort, 10),
                        customData: customDataAsJson
                    });
                };

                /**
                 * @param {NodeInfo} item
                 * @param {function(string=)} callback
                 */
                self.checkAgentInstall = function (item, callback) {
                    item.agentInstalled = false;
                    item.state = STATES.NOT_INSTALLED;
                    self.onCheckAgentInstall({nodeInfo: item}).then(
                        function (data) {
                            item.agentInstalled = data.installed;
                            callback();
                        },
                        function (resp) {
                            Notification.error({
                                message: 'Failed to check agent state (' +
                                    resp.statusText + ': ' + resp.data + ')'
                            });
                            callback(resp.data);
                        }
                    );
                };

                /**
                 * @param {DistributionInfo} data
                 * @param {NodeInfo} node
                 */
                self.installAgent = function (data, node) {
                    self.isOperationInProgress = true;
                    self.onInstallAgent({
                        nodeId: node._id,
                        distributionId: data._id,
                        callback: function () {
                            self.isOperationInProgress = false;
                            self.checkAgentInstall(node, function () {
                                node.state = STATES.DOWN;
                            });
                        }
                    });
                };

                /** @param {NodeInfo} node */
                self.uninstallAgent = function (node) {
                    self.isOperationInProgress = true;
                    self.onUninstallNode({
                        nodeInfo: node,
                        callback: function () {
                            self.isOperationInProgress = false;
                        }
                    });
                };

                /** @param {NodeInfo} nodeInfo */
                self.startNode = function (nodeInfo) {
                    self.onStartNode({
                        nodeInfo: nodeInfo,
                        callback: function () {
                            self.checkState(nodeInfo);
                        }
                    });
                };

                /** @param {NodeInfo} nodeInfo */
                self.stopNode = function (nodeInfo) {
                    self.onStopNode({
                        nodeInfo: nodeInfo,
                        callback: function () {
                            self.checkState(nodeInfo);
                        }
                    });
                };

                /**
                 * Returns true if the node can be started
                 * @param {NodeInfo} data
                 * @returns {boolean}
                 */
                self.canStart = function (data) {
                    var agent = getAgentByUuid(data._id);
                    if (!agent) {
                        return false;
                    }

                    return agent.agentInstalled && agent.state === STATES.DOWN;
                };

                /**
                 * Returns true if the node can be stopped
                 * @param {NodeInfo} data
                 * @returns {boolean}
                 */
                self.canStop = function (data) {
                    var agent = getAgentByUuid(data._id);
                    if (!agent) {
                        return false;
                    }

                    return agent.agentInstalled && agent.state === STATES.RUNNING;
                };
            }],
        bindings: {
            nodeList: '<',
            isOperationInProgress: '=',
            distributionList: '<',
            onCheckAgentState: '&',
            onUninstallNode: '&',
            onUpdateNode: '&',
            onInstallAgent: '&',
            onCheckName: '&',
            onStartNode: '&',
            onStopNode: '&',
            onCheckAgentInstall: '&'
        }
    });

},{"async":5}],28:[function(require,module,exports){
/*global angular*/
'use strict';

angular.module('bitcraft-node.list', []);

},{}],29:[function(require,module,exports){
/*global angular*/
'use strict';

var template = '' +
'<div>' +
'    <div>' +
'        <label translate>NODE_MANAGEMENT.HOST_CATEGORY</label>' +
'        <div class="btn-group level-button-group">' +
'            <button type="button"' +
'                    class="btn btn-default"' +
'                    ng-class="{active: $ctrl.hostCategory===\'agent\'}"' +
'                    ng-click="$ctrl.updateHostCategory(\'agent\')"' +
'                    translate>HOST_CATEGORY.AGENT' +
'            </button>' +
'            <button type="button"' +
'                    class="btn btn-default"' +
'                    ng-class="{active: $ctrl.hostCategory===\'http\'}"' +
'                    ng-click="$ctrl.updateHostCategory(\'http\')"' +
'                    translate>HOST_CATEGORY.HTTP' +
'            </button>' +
'            <button type="button"' +
'                    class="btn btn-default"' +
'                    ng-class="{active: $ctrl.hostCategory===\'router\'}"' +
'                    ng-click="$ctrl.updateHostCategory(\'router\')"' +
'                    translate>HOST_CATEGORY.ROUTER' +
'            </button>' +
'        </div>' +
'    </div>' +
'        <div>' +
'            <div class="row" style="margin-top:10px; margin-bottom:10px;">' +
'                <div class="col-md-12">' +
'                    <h4 translate>NODE_MANAGEMENT.HOST_LIST</h4>' +
'                </div>' +
'            </div>' +
'            <bitcraft-host-list host-list="$ctrl.hostList"' +
'                       is-operation-in-progress="$ctrl.isOperationInProgress"' +
'                       distribution-list="$ctrl.distributions"' +
'                       host-category="$ctrl.hostCategory"' +
'                       on-host-clicked="$ctrl.onHostClicked(hostInfo)"' +
'                       on-check-http-state="$ctrl.checkHttpState(hostInfo)"' +
'                       on-start-http="$ctrl.onStartHttp(hostInfo, callback)"' +
'                       on-stop-http="$ctrl.onStopHttp(hostInfo, callback)"' +
'                       on-get-node-count="$ctrl.onGetNodeCount(hostInfo)"' +
'                       on-uninstall-host="$ctrl.onUninstallHost(hostInfo)"' +
'                       on-reinstall-sentinel="$ctrl.onReinstallSentinel(hostInfo, callback)"' +
'                       on-install-http="$ctrl.onInstallHttp(distributionId, hostId, callback)"' +
'                       on-check-http-install="$ctrl.onCheckHttpInstall(hostInfo)"' +
'            ></bitcraft-host-list>' +
'        </div>' +
'    </div>' +
'    <div ng-show="$ctrl.hostCategory===\'agent\'"' +
'         class="col-md-12">' +
'        <div class="row" style="margin-top:10px; margin-bottom:10px;">' +
'            <div class="col-md-12">' +
'                <h4 translate>NODE_MANAGEMENT.NODE_LIST</h4>' +
'            </div>' +
'        </div>' +
'        <!-- add new node form -->' +
'        <div ng-show="$ctrl.selectedHost"' +
'             style="margin-top: 10px; margin-bottom: 10px">' +
'            <uib-accordion>' +
'                <div uib-accordion-group class="panel-default"' +
'                     heading="{{\'NODE_MANAGEMENT.CREATE_NODE\'|translate}}" is-open="false">' +
'                    <form class="form-inline">' +
'                        <div class="form-group">' +
'                            <div class="row" style="margin-bottom: 10px">' +
'                                <div class="col-md-2">' +
'                                    <label class="control-label" for="input-name" translate>NODE_MANAGEMENT.NAME</label>' +
'                                </div>' +
'                                <div class="col-md-4">' +
'                                    <input' +
'                                            ng-model="$ctrl.newNodeName"' +
'                                            type="text"' +
'                                            class="form-control"' +
'                                            id="input-name"' +
'                                    >' +
'                                </div>' +
'                            </div>' +
'                            <div class="row" style="margin-bottom: 10px">' +
'                                <div class="col-md-2">' +
'                                    <label class="control-label" for="input-tcpPort" translate>NODE_MANAGEMENT.TCP_PORT</label>' +
'                                </div>' +
'                                <div class="col-md-4">' +
'                                    <input' +
'                                            ng-model="$ctrl.newTcpPort"' +
'                                            type="text"' +
'                                            class="form-control"' +
'                                            id="input-tcpPort"' +
'                                    >' +
'                                </div>' +
'                                <div class="col-md-2">' +
'                                    <label class="control-label" for="input-udpPort" translate>NODE_MANAGEMENT.UDP_PORT</label>' +
'                                </div>' +
'                                <div class="col-md-4">' +
'                                    <input' +
'                                            ng-model="$ctrl.newUdpPort"' +
'                                            type="text"' +
'                                            class="form-control"' +
'                                            id="input-udpPort"' +
'                                    >' +
'                                </div>' +
'                            </div>' +
'                            <div class="row" style="margin-bottom: 10px">' +
'                                <div class="col-md-2">' +
'                                    <label class="control-label" for="input-adminAccessHost"' +
'                                           translate>NODE_MANAGEMENT.ADMIN_ACCESS_HOST</label>' +
'                                </div>' +
'                                <div class="col-md-4">' +
'                                    <input' +
'                                            ng-model="$ctrl.adminAccessHost"' +
'                                            type="text"' +
'                                            ng-disabled="true"' +
'                                            class="form-control"' +
'                                            id="input-adminAccessHost"' +
'                                    >' +
'                                </div>' +
'                                <div class="col-md-2">' +
'                                    <label class="control-label" for="input-adminAccessPort"' +
'                                           translate>NODE_MANAGEMENT.ADMIN_ACCESS_PORT</label>' +
'                                </div>' +
'                                <div class="col-md-4">' +
'                                    <input' +
'                                            ng-model="$ctrl.adminAccessPort"' +
'                                            type="text"' +
'                                            class="form-control"' +
'                                            id="input-adminAccessPort"' +
'                                    >' +
'                                </div>' +
'                            </div>' +
'                            <div class="row" style="margin-bottom: 10px">' +
'                                <div class="col-md-2">' +
'                                    <label class="control-label" for="input-customData" translate>NODE_MANAGEMENT.CUSTOM_DATA</label>' +
'                                </div>' +
'                                <div class="col-md-10">' +
'                                    <div ui-ace="$ctrl.aceSettings"' +
'                                         id="input-customData"' +
'                                         ng-model="$ctrl.customData"' +
'                                         style="height: 200px; background: whitesmoke;"' +
'                                    />' +
'                                </div>' +
'                            </div>' +
'                            <a class="btn btn-primary"' +
'                               ng-click="$ctrl.onAddNode()"' +
'                               translate>' +
'                                GENERAL.ADD' +
'                            </a>' +
'                        </div>' +
'                    </form>' +
'                </div>' +
'            </uib-accordion>' +
'        </div>' +
'        <div ng-if="$ctrl.selectedHost" class="row" style="margin-top:20px; margin-bottom:20px;">' +
'            <label class="control-label col-md-2" translate>NODE_MANAGEMENT.BULK_OPERATIONS</label>' +
'            <!-- install distribution form -->' +
'            <form editable-form name="installForm"' +
'                  onbeforesave="$ctrl.installAllNodes($ctrl.selectedHost, $ctrl.selectedDistribution)"' +
'                  ng-show="installForm.$visible" class="form-buttons form-inline">' +
'                <select' +
'                        ng-model="$ctrl.selectedDistribution"' +
'                        class="form-control"' +
'                        id="distribution-list"' +
'                        ng-options="dist as (dist.name + \' - \' + dist.version)' +
'                            for dist in $ctrl.distributions' +
'                            track by dist._id"' +
'                >' +
'                </select>' +
'                <button type="submit" ng-disabled="installForm.$waiting"' +
'                        class="btn btn-primary" translate>' +
'                    DIALOGS_OK' +
'                </button>' +
'                <button type="button" ng-disabled="installForm.$waiting"' +
'                        ng-click="installForm.$cancel()" class="btn btn-default" translate>' +
'                    GENERAL.CANCEL' +
'                </button>' +
'            </form>' +
'            <div ng-show="!installForm.$visible">' +
'                <button type="button"' +
'                        class="btn btn-primary"' +
'                        ng-click="installForm.$show()"' +
'                        ng-disabled="$ctrl.isOperationInProgress || !$ctrl.nodeList.length"' +
'                        translate>NODE_MANAGEMENT.INSTALL' +
'                </button>' +
'                <button type="button"' +
'                        class="btn btn-success"' +
'                        ng-click="$ctrl.startAllNodes($ctrl.selectedHost)"' +
'                        ng-disabled="$ctrl.isOperationInProgress || !$ctrl.nodeList.length"' +
'                        translate>GENERAL.START' +
'                </button>' +
'                <button type="button"' +
'                        class="btn btn-danger"' +
'                        ng-click="$ctrl.stopAllNodes($ctrl.selectedHost)"' +
'                        ng-disabled="$ctrl.isOperationInProgress || !$ctrl.nodeList.length"' +
'                        translate>GENERAL.STOP' +
'                </button>' +
'                <button type="button"' +
'                        class="btn btn-default"' +
'                        ng-click="$ctrl.uninstallAllNodes($ctrl.selectedHost)"' +
'                        ng-disabled="$ctrl.isOperationInProgress || !$ctrl.nodeList.length"' +
'                        translate>NODE_MANAGEMENT.UNINSTALL' +
'                </button>' +
'            </div>' +
'        </div>' +
'        <bitcraft-node-list node-list="$ctrl.nodeList"' +
'                   is-operation-in-progress="$ctrl.isOperationInProgress"' +
'                   distribution-list="$ctrl.distributions"' +
'                   on-check-agent-state="$ctrl.onCheckAgentState(nodeInfo)"' +
'                   on-uninstall-node="$ctrl.onUninstallNode(nodeInfo, callback)"' +
'                   on-update-node="$ctrl.onUpdateNode(nodeId, name, udpPort, tcpPort, adminHost, adminPort, customData)"' +
'                   on-install-agent="$ctrl.onInstallAgent(nodeId, distributionId, callback)"' +
'                   on-check-name="$ctrl.onCheckName(name)"' +
'                   on-start-node="$ctrl.onStartNode(nodeInfo, callback)"' +
'                   on-stop-node="$ctrl.onStopNode(nodeInfo, callback)"' +
'                   on-check-agent-install="$ctrl.onCheckAgentInstall(nodeInfo)"' +
'        ></bitcraft-node-list>' +
'    </div>' +
'</div>' +
'';


//noinspection JSUnusedGlobalSymbols
angular.module('bitcraft-node-management').
    component('bitcraftNodeManagement', {
        template: template,
        controller: [
            '$log', 'dialogs', 'Notification', 'DeploymentManagement', 'ServerConfig',
            function nodeManagementController(
                $log,
                dialogs,
                Notification,
                DeploymentManagement,
                ServerConfig
            ) {
                var self = this;

                /** @type {boolean} */
                self.isOperationInProgress = false;

                self.selectedDistribution = {};
                self.hostList = [];
                self.nodeList = [];
                self.newNodeName = '';
                self.newUdpPort = '';
                self.newTcpPort = '';
                /** @type {{host: string, port: string}} */
                self.adminAccessHost = '0.0.0.0';
                self.adminAccessPort = '';
                self.hostCategory = 'agent';
                self.selectedHost = undefined;
                //TODO load custom data
                self.customData = '';
                /** @type {SystemConfig} */
                self.config = {};
                self.aceSettings =
                {
                    mode: 'javascript'
                };

                ServerConfig.getCurrentConfig().then(function (data) {
                    self.config = data;

                    self.newUdpPort = self.config.nodeManagement.udpPort;
                    self.newTcpPort = self.config.nodeManagement.tcpPort;
                    self.adminAccessHost = self.config.nodeManagement.adminHost;
                    self.adminAccessPort = self.config.nodeManagement.adminPort;
                    self.customData = JSON.stringify(self.config.nodeManagement.customData, null, '\t');
                }, function (resp) {
                    $log.debug('failed to retrieve config from server. (' + resp.data + ')');
                });

                function refreshNodes() {
                    if (self.selectedHost) {
                        DeploymentManagement.listNodes(self.selectedHost).then(
                            function (data) {
                                self.nodeList = data;
                            },
                            function (resp) {
                                Notification.error({
                                    message: 'Failed to retrieve nodes (' + resp.statusText + ')'
                                });
                            }
                        );
                    }
                }

                function refreshHosts() {
                    DeploymentManagement.listHosts().then(function (data) {
                        self.hostList = data;
                    }, function (resp) {
                        Notification.error({
                            message: 'Failed to retrieve hosts (' + resp.statusText + ')'
                        });
                    });
                }

                /**
                 * @param {Array.<DistributionInfo>} distributionList
                 * @returns {Array.<DistributionInfo>}
                 */
                function sortFilterDistributions(distributionList) {
                    if (distributionList !== undefined) {
                        return distributionList.filter(function (item) {
                            return item.category === self.hostCategory;
                        }).sort(function (a, b) {
                            return (a.uploadTimestamp > a.uploadTimestamp ? -1 : 1);
                        });
                    }
                }

                /** @param {string} category */
                self.updateHostCategory = function (category) {
                    self.hostCategory = category;
                    self.refreshDistributions();
                };

                self.refreshDistributions = function () {
                    DeploymentManagement.listDistributions().then(function (data) {
                        self.distributions = sortFilterDistributions(data);
                        if (self.distributions && self.distributions.length > 0) {
                            self.selectedDistribution = self.distributions[0];
                        }
                    }, function (resp) {
                        Notification.error({
                            message: 'Failed to retrieve distributions (' + resp.statusText + ')'
                        });
                    });
                };

                /** @param {HostInfo} hostInfo */
                self.onHostClicked = function (hostInfo) {
                    if (self.selectedHost === hostInfo) {
                        return;
                    }

                    //TODO highlight selected host
                    self.selectedHost = hostInfo;
                    refreshNodes();
                };

                self.onAddNode = function () {
                    /** @type {JSON} */
                    var customDataAsJson;

                    try {
                        customDataAsJson = JSON.parse(self.customData);
                    } catch (e) {
                        Notification.error({
                            message: 'Invalid JSON format in custom data (' + e + ')'
                        });
                        return;
                    }

                    DeploymentManagement.addNode(
                        self.selectedHost,
                        self.newNodeName,
                        parseInt(self.newUdpPort, 10),
                        parseInt(self.newTcpPort, 10),
                        self.adminAccessHost,
                        parseInt(self.adminAccessPort, 10),
                        customDataAsJson
                    ).then(function (data) {
                        self.newNodeName = '';
                        self.newUdpPort = self.config.nodeManagement.udpPort;
                        self.newTcpPort = self.config.nodeManagement.tcpPort;
                        self.adminAccessHost = self.config.nodeManagement.adminHost;
                        self.adminAccessPort = self.config.nodeManagement.adminPort;
                        refreshNodes();

                        Notification.success({
                            message: 'Node successfully added'
                        });
                    }, function (resp) {
                        Notification.error({
                            message: 'Failed to add node (' + resp.statusText + ')'
                        });
                    });
                };

                /**
                 * @param {NodeInfo} nodeInfo
                 * @param {function()} callback
                 */
                self.onUninstallNode = function (nodeInfo, callback) {
                    var dlg = dialogs.confirm(
                        'Confirmation required',
                        'Do you want to uninstall the node?',
                        {size: 'sm'}
                    );
                    dlg.result.then(function (btn) {
                        Notification.info({
                            message: 'Uninstalling node...'
                        });
                        DeploymentManagement.uninstallAgent(
                            nodeInfo
                        ).then(
                            function (resp) {
                                Notification.success({
                                    message: 'Distribution successfully uninstalled'
                                });
                                refreshNodes(self.selectedHost);
                                callback();
                            },
                            /** @param {{data: {error: string}}} */
                            function (resp) {
                                var errorMessage = resp.statusText +
                                    (resp.data && resp.data.error && (': ' + resp.data.error));
                                Notification.error({
                                    message: 'Failed to uninstall distribution (' + errorMessage + ')'
                                });
                                callback();
                            }
                        );
                    },
                        function () {
                            callback();
                        });
                };

                /** @param {HostInfo} hostInfo */
                self.onUninstallHost = function (hostInfo) {
                    DeploymentManagement.getNodeCount(hostInfo).then(
                        function (data) {
                            if (data.nodeCount > 0) {
                                dialogs.error(
                                    'Host contains nodes',
                                    'Please make sure to uninstall all nodes before uninstalling the host.',
                                    {size: 'sm'}
                                );
                                return;
                            }

                            var dlg = dialogs.confirm(
                                'Confirmation required',
                                'Do you want to uninstall the host?',
                                {size: 'sm'}
                            );
                            dlg.result.then(function (btn) {
                                Notification.info({
                                    message: 'Uninstalling host...'
                                });
                                DeploymentManagement.uninstallHost(
                                    hostInfo
                                ).then(function (data) {
                                    if (self.selectedHost === hostInfo) {
                                        self.selectedHost = null;
                                    }
                                    refreshHosts();
                                    Notification.success({
                                        message: 'Host successfully uninstalled'
                                    });
                                }, function (resp) {
                                    Notification.error({
                                        message: 'Failed to uninstall host (' + resp.statusText + ')'
                                    });
                                });
                            });
                        },
                        function (resp) {
                            Notification.error({
                                message: 'Failed to check node count (' +
                                    resp.statusText + ': ' + resp.data + ')'
                            });
                        }
                    );
                };

                /**
                 * @param {HostInfo} hostInfo
                 * @param {function()} callback
                 */
                self.onReinstallSentinel = function (hostInfo, callback) {
                    var dlg = dialogs.confirm(
                        'Confirmation required',
                        'Do you want to reinstall the sentinel?',
                        {size: 'sm'}
                    );
                    dlg.result.then(function (btn) {
                        Notification.info({
                            message: 'Reinstalling sentinel...'
                        });
                        DeploymentManagement.reinstallSentinel(
                            hostInfo
                        ).then(function (data) {
                            if (self.selectedHost === hostInfo) {
                                self.selectedHost = null;
                            }
                            refreshHosts();
                            self.nodeList = [];
                            callback();
                            Notification.success({
                                message: 'Sentinel successfully reinstalled'
                            });
                        }, function (resp) {
                            callback();
                            Notification.error({
                                message: 'Failed to reinstall sentinel (' + resp.statusText + ')'
                            });
                        });
                    },
                        function () {
                            callback();
                        });
                };

                /**
                 * @param {string} nodeId
                 * @param {string} name
                 * @param {number} udpPort
                 * @param {number} tcpPort
                 * @param {string} adminHost
                 * @param {number} adminPort
                 * @param {JSON} customData
                 */
                self.onUpdateNode = function (nodeId, name, udpPort, tcpPort, adminHost, adminPort, customData) {
                    DeploymentManagement.updateNode(
                        nodeId,
                        name,
                        udpPort,
                        tcpPort,
                        adminHost,
                        adminPort,
                        customData
                    ).then(function (data) {
                        Notification.success({
                            message: 'Node successfully updated'
                        });
                    }, function (resp) {
                        Notification.error({
                            message: 'Failed to updated node (' + resp.statusText + ')'
                        });
                    });
                };

                /**
                 * @param {string} nodeId
                 * @param {string} distributionId
                 * @param {function()} callback
                 */
                self.onInstallAgent = function (nodeId, distributionId, callback) {
                    Notification.info({
                        message: 'Installing agent...'
                    });
                    DeploymentManagement.installAgent(
                        nodeId,
                        distributionId
                    ).then(
                        function (resp) {
                            Notification.success({
                                message: 'Distribution successfully installed'
                            });
                            callback();
                        },
                        /** @param {{data: {error: string}}} */
                        function (resp) {
                            var errorMessage = resp.statusText +
                                (resp.data && resp.data.error && (': ' + resp.data.error));
                            Notification.error({
                                message: 'Failed to install distribution (' + errorMessage + ')'
                            });
                            callback();
                        }
                    );
                };

                /**
                 * @param {string} distributionId
                 * @param {string} hostId
                 * @param {function()} callback
                 */
                self.onInstallHttp = function (distributionId, hostId, callback) {
                    Notification.info({
                        message: 'Installing http...'
                    });
                    DeploymentManagement.installHttp(
                        distributionId,
                        hostId
                    ).then(function (data) {
                        Notification.success({
                            message: 'Http successfully installed'
                        });
                        callback();
                    }, function (resp) {
                        Notification.error({
                            message: 'Failed to install http (' + resp.statusText + ')'
                        });
                        callback();
                    });
                };

                /**
                 * @param {HostInfo} hostInfo
                 * @param {function()} callback
                 */
                self.onStartHttp = function (hostInfo, callback) {
                    DeploymentManagement.startHttp(
                        hostInfo._id
                    ).then(function (data) {
                        Notification.success({
                            message: 'Http successfully started'
                        });
                        callback();
                    }, function (resp) {
                        Notification.error({
                            message: 'Failed to start http (' + resp.statusText + ')'
                        });
                        callback();
                    });
                };

                /**
                 * @param {HostInfo} hostInfo
                 * @param {function()} callback
                 */
                self.onStopHttp = function (hostInfo, callback) {
                    DeploymentManagement.stopHttp(
                        hostInfo._id
                    ).then(function (data) {
                        Notification.success({
                            message: 'Http successfully stopped'
                        });
                        callback();
                    }, function (resp) {
                        Notification.error({
                            message: 'Failed to stop http (' + resp.statusText + ')'
                        });
                        callback();
                    });
                };

                /**
                 * @param {NodeInfo} nodeInfo
                 * @param {function()} callback
                 */
                self.onStartNode = function (nodeInfo, callback) {
                    DeploymentManagement.startNode(
                        nodeInfo._id
                    ).then(
                        /** @param {{data: {running: boolean}}} resp */
                        function (resp) {
                            if (!resp.data.running) {
                                Notification.error({
                                    message: 'Agent could not be started'
                                });
                            } else {
                                Notification.success({
                                    message: 'Agent successfully started'
                                });
                            }
                            callback();
                        },
                        /** @param {{data: {error: string}}} */
                        function (resp) {
                            var errorMessage = resp.statusText +
                                (resp.data && resp.data.error && (': ' + resp.data.error));
                            Notification.error({
                                message: 'Failed to start agent (' + errorMessage + ')'
                            });
                            callback();
                        }
                    );
                };

                /**
                 * @param {NodeInfo} nodeInfo
                 * @param {function()} callback
                 */
                self.onStopNode = function (nodeInfo, callback) {
                    DeploymentManagement.stopNode(
                        nodeInfo._id
                    ).then(
                        /** @param {{data: {running: boolean}}} resp */
                        function (resp) {
                            if (resp.data.running) {
                                Notification.error({
                                    message: 'Agent could not be stopped'
                                });
                            } else {
                                Notification.success({
                                    message: 'Agent successfully stopped'
                                });
                            }
                            callback();
                        },
                        /** @param {{data: {error: string}}} */
                        function (resp) {
                            var errorMessage = resp.statusText +
                                (resp.data && resp.data.error && (': ' + resp.data.error));
                            Notification.error({
                                message: 'Failed to stop agent (' + errorMessage + ')'
                            });
                            callback();
                        }
                    );
                };

                /** @param {HostInfo} hostInfo */
                self.stopAllNodes = function (hostInfo) {
                    self.isOperationInProgress = true;
                    DeploymentManagement.stopAllNodes(
                        hostInfo._id
                    ).then(
                        /** @param {{data: {allAgentsStopped: boolean}}} resp */
                        function (resp) {
                            if (!resp.data.allAgentsStopped) {
                                Notification.error({
                                    message: 'Not all agents could be stopped'
                                });
                            } else {
                                Notification.success({
                                    message: 'All agents were successfully stopped'
                                });
                            }
                            refreshNodes(self.selectedHost);
                            self.isOperationInProgress = false;
                        },
                        /** @param {{data: string}} */
                        function (resp) {
                            var errorMessage = resp.statusText + (resp.data && (': ' + resp.data));
                            Notification.error({
                                message: 'Failed to stop all agents (' + errorMessage + ')'
                            });
                            refreshNodes(self.selectedHost);
                            self.isOperationInProgress = false;
                        }
                    );
                };

                /** @param {HostInfo} hostInfo */
                self.startAllNodes = function (hostInfo) {
                    self.isOperationInProgress = true;
                    DeploymentManagement.startAllNodes(
                        hostInfo._id
                    ).then(
                        /** @param {{data: {allAgentsStarted: boolean}}} resp */
                        function (resp) {
                            if (!resp.data.allAgentsStarted) {
                                Notification.error({
                                    message: 'Not all agents could be started'
                                });
                            } else {
                                Notification.success({
                                    message: 'All agents were successfully started'
                                });
                            }
                            refreshNodes(self.selectedHost);
                            self.isOperationInProgress = false;
                        },
                        /** @param {{data: string}} */
                        function (resp) {
                            var errorMessage = resp.statusText + (resp.data && (': ' + resp.data));
                            Notification.error({
                                message: 'Failed to start all agents (' + errorMessage + ')'
                            });
                            refreshNodes(self.selectedHost);
                            self.isOperationInProgress = false;
                        }
                    );
                };

                /**
                 * @param {HostInfo} hostInfo
                 * @param {DistributionInfo} distributionInfo
                 */
                self.installAllNodes = function (hostInfo, distributionInfo) {
                    self.isOperationInProgress = true;
                    Notification.info({
                        message: 'Installing all nodes...'
                    });
                    DeploymentManagement.installAllNodes(
                        hostInfo._id,
                        distributionInfo._id
                    ).then(
                        function (resp) {
                            Notification.success({
                                message: 'All agents were successfully installed'
                            });
                            refreshNodes(self.selectedHost);
                            self.isOperationInProgress = false;
                        },
                        /** @param {{data: {error: string}}} */
                        function (resp) {
                            var errorMessage = resp.statusText +
                                (resp.data && resp.data.error && (': ' + resp.data.error));

                            Notification.error({
                                message: 'Failed to install all agents (' + errorMessage + ')'
                            });
                            refreshNodes(self.selectedHost);
                            self.isOperationInProgress = false;
                        }
                    );
                };

                /** @param {HostInfo} hostInfo */
                self.uninstallAllNodes = function (hostInfo) {
                    var dlg = dialogs.confirm(
                        'Confirmation required',
                        'Do you want to uninstall all nodes?',
                        {size: 'sm'}
                    );
                    dlg.result.then(function (btn) {
                        self.isOperationInProgress = true;
                        Notification.info({
                            message: 'Uninstalling all nodes...'
                        });
                        DeploymentManagement.uninstallAllNodes(
                            hostInfo._id
                        ).then(
                            function (resp) {
                                Notification.success({
                                    message: 'All nodes were successfully uninstalled'
                                });
                                refreshNodes(self.selectedHost);
                                self.isOperationInProgress = false;
                            },
                            /** @param {{data: {error: string}}} */
                            function (resp) {
                                var errorMessage = resp.statusText +
                                    (resp.data && resp.data.error && (': ' + resp.data.error));

                                Notification.error({
                                    message: 'Failed to uninstall all nodes (' + errorMessage + ')'
                                });
                                refreshNodes(self.selectedHost);
                                self.isOperationInProgress = false;
                            }
                        );
                    });
                };

                self.onCheckName = DeploymentManagement.checkAgentName;
                self.onCheckAgentState = DeploymentManagement.checkAgentState;
                self.checkHttpState = DeploymentManagement.checkHttpState;
                self.onCheckHttpInstall = DeploymentManagement.onCheckHttpInstall;
                self.onCheckAgentInstall = DeploymentManagement.onCheckAgentInstall;
                self.onGetNodeCount = DeploymentManagement.getNodeCount;

                refreshHosts();
                self.refreshDistributions();
            }
        ]
    });


/**
 * @typedef {Object} AdminAccess
 * @property {string} host
 * @property {number} port
 */

/**
 * @typedef {Object} NodeInfo
 * @property {ObjectID|string} _id
 * @property {ObjectID|string} hostId
 * @property {string} uuid
 * @property {string} name
 * @property {number} udpPort
 * @property {number} tcpPort
 * @property {AdminAccess} adminAccess
 * @property {number} bootTime
 * @property {string} distributionId
 * @property {string} distributionVersion
 * @property {number} distributionTimestamp
 * @property {number} installTimestamp
 * @property {Object} customData
 */

/**
 * @typedef {Object} DistributionInfo
 * @property {ObjectID} _id
 * @property {string} category
 * @property {string} name
 * @property {string} version
 * @property {number} uploadTimestamp
 * @property {Buffer} data
 */

},{}],30:[function(require,module,exports){
/*global angular*/
'use strict';

angular.module('bitcraft-node-management', ['bitcraft-host.list', 'bitcraft-node.list']);

},{}],31:[function(require,module,exports){
/*global angular*/
angular.module('bitcraft-deployment-management', ['ngFileUpload']);

},{}],32:[function(require,module,exports){
/*global angular, window*/
'use strict';

angular.
    module('bitcraft-deployment-management').
    factory('DeploymentManagement', [
        '$http', '$log', '$q', 'Upload',
        function ($http, $log, $q, Upload) {

            /**
             * Upload a new distribution
             * @param {string} hostCategory
             * @param {number} version
             * @param {File} file
             */
            function uploadDistribution(hostCategory, version, file) {
                var fields = {
                    category: hostCategory,
                    name: file.name,
                    version: version,
                    file: file
                };

                return Upload.upload({
                    url: './rest/uploadDistributionArchive',
                    fields: fields
                });
            }

            /**
             * Upload a new sentinel archive
             * @param {File} file
             */
            function uploadSentinel(file) {
                var fields = {
                    file: file
                };

                return Upload.upload({
                    url: './rest/uploadSentinelArchive',
                    fields: fields
                });
            }

            /**
             * Install the sentinel for a new host
             * @param {HostInfo} hostInfo
             * @param {File} [sentinelFile]
             */
            function installHost(hostInfo, sentinelFile) {
                var fields = {
                    category: hostInfo.category,
                    sentinelPort: hostInfo.sentinelPort,
                    localAddress: hostInfo.localAddress,
                    id: hostInfo._id,
                    dns: hostInfo.dns
                };
                if (sentinelFile) {
                    fields.file = sentinelFile;
                }

                return Upload.upload({
                    url: './rest/installHost',
                    fields: fields
                });
            }

            /**
             * Get all hosts in database
             */
            function listHosts() {
                var deferred = $q.defer();

                $http.get('./rest/listHosts').then(function successCallback(res) {
                    deferred.resolve(res.data);
                }, function errorCallback(resp) {
                    $log.error('Failed to retrieve the agent list (' + resp.statusText + ')');
                    deferred.reject(resp);
                });

                return deferred.promise;
            }

            /**
             * Get all nodes of a specific host
             * @param {HostInfo} hostInfo
             */
            function listNodes(hostInfo) {
                var deferred = $q.defer();

                $http.get('./rest/listNodes', {
                    params: {
                        hostId: hostInfo._id
                    }
                }).then(function successCallback(res) {
                    deferred.resolve(res.data);
                }, function errorCallback(resp) {
                    $log.error('Failed to retrieve the node list (' + resp.statusText + ')');
                    deferred.reject(resp);
                });

                return deferred.promise;
            }

            /** Get all distributions */
            function listDistributions(hostInfo) {
                var deferred = $q.defer();

                $http.get('./rest/listDistributions').then(function successCallback(res) {
                    deferred.resolve(res.data);
                }, function errorCallback(resp) {
                    $log.error(
                        'Failed to retrieve the distribution list (' + resp.statusText + ')'
                    );
                    deferred.reject(resp);
                });

                return deferred.promise;
            }

            /**
             * Add a new node to a host
             * @param {HostInfo} hostInfo
             * @param {string} name
             * @param {number} udpPort
             * @param {number} tcpPort
             * @param {string} adminHost
             * @param {number} adminPort
             * @param {JSON} customData
             */
            function addNode(hostInfo, name, udpPort,
                             tcpPort, adminHost, adminPort, customData) {
                var req = {
                    method: 'POST',
                    url: './rest/addNode',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    data: {
                        host: hostInfo,
                        name: name,
                        udpPort: udpPort,
                        tcpPort: tcpPort,
                        adminHost: adminHost,
                        adminPort: adminPort,
                        customData: customData
                    }
                };

                return $http(req);
            }

            /**
             * Remove a node from the database
             * @param {NodeInfo} nodeInfo
             */
            function uninstallAgent(nodeInfo) {
                var req = {
                    method: 'POST',
                    url: './rest/uninstallAgent',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    data: {nodeId: nodeInfo._id}
                };

                return $http(req);
            }

            /**
             * Remove a distribution from the database
             * @param {DistributionInfo} distributionInfo
             */
            function removeDistribution(distributionInfo) {
                var req = {
                    method: 'POST',
                    url: './rest/removeDistribution',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    data: {distributionId: distributionInfo._id}
                };

                return $http(req);
            }

            /**
             * Downlaod a distribution from the database
             * @param {DistributionInfo} distributionInfo
             */
            function downloadDistribution(distributionInfo) {
                var url = './rest/downloadDistribution?distributionId=' + distributionInfo._id;
                window.open(url, '_blank');
            }

            /**
             * Remove a host from the database
             * @param {HostInfo} hostInfo
             */
            function uninstallHost(hostInfo) {
                var req = {
                    method: 'POST',
                    url: './rest/uninstallHost',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    data: {hostId: hostInfo._id}
                };

                return $http(req);
            }

            /**
             * Reinstall the latest sentinel on the host
             * @param {HostInfo} hostInfo
             */
            function reinstallSentinel(hostInfo) {
                var req = {
                    method: 'POST',
                    url: './rest/reinstallSentinel',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    data: hostInfo
                };

                return $http(req);
            }

            /**
             * Update a node in the database
             * @param {string} id
             * @param {string} name
             * @param {number} udpPort
             * @param {number} tcpPort
             * @param {string} adminHost
             * @param {number} adminPort
             * @param {JSON} customData
             */
            function updateNode(id, name, udpPort, tcpPort, adminHost, adminPort, customData) {
                var req = {
                    method: 'POST',
                    url: './rest/updateNode',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    data: {
                        id: id,
                        name: name,
                        udpPort: udpPort,
                        tcpPort: tcpPort,
                        adminHost: adminHost,
                        adminPort: adminPort,
                        customData: customData
                    }
                };

                return $http(req);
            }

            /**
             * Update a distribution in the database
             * @param {string} id
             * @param {string} version
             */
            function updateDistribution(id, version) {
                var req = {
                    method: 'POST',
                    url: './rest/updateDistribution',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    data: {id: id, version: version}
                };

                return $http(req);
            }

            /**
             * Install a distribution for a node
             * @param {string} nodeId
             * @param {string} distributionId
             */
            function installAgent(nodeId, distributionId) {
                var req = {
                    method: 'POST',
                    url: './rest/installAgent',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    data: {nodeId: nodeId, distributionId: distributionId}
                };

                return $http(req);
            }

            /**
             * Install a http
             * @param {string} distributionId
             * @param {string} hostId
             */
            function installHttp(distributionId, hostId) {
                var req = {
                    method: 'POST',
                    url: './rest/installHttp',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    data: {distributionId: distributionId, hostId: hostId}
                };

                return $http(req);
            }

            /** @param {string} hostId */
            function startHttp(hostId) {
                var req = {
                    method: 'POST',
                    url: './rest/startHttp',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    data: {hostId: hostId}
                };

                return $http(req);
            }

            /**
             * Start the agent of a node
             * @param {string} nodeId
             */
            function startNode(nodeId) {
                var req = {
                    method: 'POST',
                    url: './rest/startNode',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    data: {nodeId: nodeId}
                };

                return $http(req);
            }

            /**
             * Stop the node
             * @param {string} nodeId
             */
            function stopNode(nodeId) {
                var req = {
                    method: 'POST',
                    url: './rest/stopNode',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    data: {nodeId: nodeId}
                };

                return $http(req);
            }

            /**
             * Stop all nodes
             * @param {string} hostId
             */
            function stopAllNodes(hostId) {
                var req = {
                    method: 'POST',
                    url: './rest/stopAllNodes',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    data: {hostId: hostId}
                };

                return $http(req);
            }

            /**
             * Start all nodes
             * @param {string} hostId
             */
            function startAllNodes(hostId) {
                var req = {
                    method: 'POST',
                    url: './rest/startAllNodes',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    data: {hostId: hostId}
                };

                return $http(req);
            }

            /**
             * Install all nodes
             * @param {string} hostId
             * @param {string} distributionId
             */
            function installAllNodes(hostId, distributionId) {
                var req = {
                    method: 'POST',
                    url: './rest/installAllNodes',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    data: {hostId: hostId, distributionId: distributionId}
                };

                return $http(req);
            }

            /**
             * Uninstall all nodes
             * @param {string} hostId
             */
            function uninstallAllNodes(hostId) {
                var req = {
                    method: 'POST',
                    url: './rest/uninstallAllNodes',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    data: {hostId: hostId}
                };

                return $http(req);
            }

            /** @param {string} hostId */
            function stopHttp(hostId) {
                var req = {
                    method: 'POST',
                    url: './rest/stopHttp',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    data: {hostId: hostId}
                };

                return $http(req);
            }

            /**
             * Checks the state of the http
             * @param {HostInfo} hostInfo
             */
            function checkHttpState(hostInfo) {
                var deferred = $q.defer();

                $http.get('./rest/checkHttpState', {
                    params: {
                        hostId: hostInfo._id
                    }
                }).then(function successCallback(res) {
                    deferred.resolve(res.data);
                }, function errorCallback(resp) {
                    $log.error('Failed to retrieve the host status (' + resp.statusText + ')');
                    deferred.reject(resp);
                });

                return deferred.promise;
            }

            /**
             * Checks the state of the http
             * @param {HostInfo} hostInfo
             */
            function onCheckHttpInstall(hostInfo) {
                var deferred = $q.defer();
                $http.get('./rest/checkHttpInstall', {
                    params: {
                        hostId: hostInfo._id
                    }
                }).then(function successCallback(res) {
                    deferred.resolve(res.data);
                }, function errorCallback(resp) {
                    $log.error('Failed to retrieve the http status (' + resp.statusText + ')');
                    deferred.reject(resp);
                });

                return deferred.promise;
            }

            /**
             * Checks the state of the agent
             * @param {NodeInfo} nodeInfo
             */
            function onCheckAgentInstall(nodeInfo) {
                var deferred = $q.defer();
                $http.get('./rest/checkAgentInstall', {
                    params: {
                        nodeId: nodeInfo._id
                    }
                }).then(function successCallback(res) {
                    deferred.resolve(res.data);
                }, function errorCallback(resp) {
                    $log.error('Failed to retrieve the agent status (' + resp.statusText + ')');
                    deferred.reject(resp);
                });

                return deferred.promise;
            }

            /**
             * Checks the state of the agent
             * @param {NodeInfo} nodeInfo
             */
            function checkAgentState(nodeInfo) {
                var deferred = $q.defer();

                $http.get('./rest/checkAgentState', {
                    params: {
                        nodeId: nodeInfo._id
                    }
                }).then(function successCallback(res) {
                    deferred.resolve(res.data);
                }, function errorCallback(resp) {
                    $log.error('Failed to retrieve the agent status (' + resp.statusText + ')');
                    deferred.reject(resp);
                });

                return deferred.promise;
            }

            /**
             * Get the node count of a host
             * @param {HostInfo} hostInfo
             */
            function getNodeCount(hostInfo) {
                var deferred = $q.defer();

                $http.get('./rest/getNodeCount', {
                    params: {
                        hostId: hostInfo._id
                    }
                }).then(function successCallback(res) {
                    deferred.resolve(res.data);
                }, function errorCallback(resp) {
                    $log.error('Failed to retrieve the node count of host "' +
                        hostInfo._id + '" (' + resp.statusText + ')');
                    deferred.reject(resp);
                });

                return deferred.promise;
            }

            /**
             * Checks if the agent name is valid. Returns a message in case of error,
             * otherwise returns undefined.
             * @param {string|undefined} data
             * @returns {string|undefined}
             */
            function checkAgentName(data) {
                if (typeof data !== 'string' || data.length === 0) {
                    return 'Agent name cannot be empty';
                }
            }

            return {
                uploadDistribution: uploadDistribution,
                uploadSentinel: uploadSentinel,
                installHost: installHost,
                listHosts: listHosts,
                addNode: addNode,
                listNodes: listNodes,
                stopAllNodes: stopAllNodes,
                startAllNodes: startAllNodes,
                installAllNodes: installAllNodes,
                uninstallAllNodes: uninstallAllNodes,
                listDistributions: listDistributions,
                removeDistribution: removeDistribution,
                downloadDistribution: downloadDistribution,
                uninstallAgent: uninstallAgent,
                uninstallHost: uninstallHost,
                reinstallSentinel: reinstallSentinel,
                updateNode: updateNode,
                updateDistribution: updateDistribution,
                installAgent: installAgent,
                installHttp: installHttp,
                onCheckHttpInstall: onCheckHttpInstall,
                onCheckAgentInstall: onCheckAgentInstall,
                checkHttpState: checkHttpState,
                checkAgentState: checkAgentState,
                checkAgentName: checkAgentName,
                startHttp: startHttp,
                stopHttp: stopHttp,
                startNode: startNode,
                stopNode: stopNode,
                getNodeCount: getNodeCount
            };
        }
    ]);

},{}],33:[function(require,module,exports){
'use strict';
require('./deployment-management.module');
require('./deployment-management.service');
require('./server-config.module');
require('./server-config.service');

},{"./deployment-management.module":31,"./deployment-management.service":32,"./server-config.module":34,"./server-config.service":35}],34:[function(require,module,exports){
/*global angular*/
angular.module('bitcraft-server-config', []);

},{}],35:[function(require,module,exports){
/*global angular*/
'use strict';

angular.
    module('bitcraft-server-config').
    factory('ServerConfig', [
        '$http', '$log', '$q',
        function ($http, $log, $q) {
            /** @returns {Promise} */
            function getCurrentConfig() {
                var deferred = $q.defer();
                $http.get('./rest/getCurrentConfig').then(function successCallback(res) {
                    deferred.resolve(res.data);
                }, function errorCallback(resp) {
                    $log.error('Failed to retrieve the current config (' + resp.statusText + ')');
                    deferred.reject(resp);
                });
                return deferred.promise;
            }

            return {
                getCurrentConfig: getCurrentConfig
            };
        }
    ]);

},{}]},{},[15]);
